#!/bin/bash
# -*- ENCODING: UTF-8 -*-
echo "Id Compañia:"
read company
bundle exec rake update_employees[$company]
sleep 15s
bundle exec rake update_labors[$company]
sleep 15s
bundle exec rake update_vacations[$company]
sleep 15s
bundle exec rake update_employees_balances[$company]
sleep 15s
bundle exec rake update_info_type[$company]
sleep 15s
bundle exec rake update_employee_info[$company]
sleep 15s
bundle exec rake update_master_data[$company]
sleep 15s
bundle exec rake update_pdfs[$company]
sleep 15s
bundle exec rake update_boss_employees[$company]
sleep 15s
bundle exec rake update_timeline_positions[$company]
sleep 15s
bundle exec rake update_pdf_types[$company]
sleep 15s
bundle exec rake update_indebtedness_levels[$company]
sleep 15s
bundle exec rake update_embargoes[$company]
sleep 15s
bundle exec rake update_loan_records[$company]
sleep 15s
bundle exec rake update_permissions[$company]
sleep 15s
bundle exec rake update_incapacity[$company]
sleep 15s
bundle exec rake update_vacation_records[$company]
sleep 15s
bundle exec rake update_extra_hour_records[$company]
sleep 15s
bundle exec rake update_compensatory_vacations_records[$company]
sleep 15s
bundle exec rake update_vacation_balance_records[$company]
sleep 15s
bundle exec rake update_payments_deductions[$company]
sleep 15s
bundle exec rake update_absences[$company]
sleep 15s
bundle exec rake update_variance_analyses[$company]
sleep 15s
bundle exec rake update_historical_positions[$company]
sleep 15s
bundle exec rake update_staff_turnover[$company]
sleep 15s
exit
