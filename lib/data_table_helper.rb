class DataTableHelper
  #@value valor del campo
  #@type tipo de valor : 'date' || 'money' || 'int' || 'string' || 'percentage'
  #@sortable bool si el campo es de ordenacion y filtro
  def self.info_field value, type, sortable
    return {
      :value    => value,
      :type     => type,
      :sortable => sortable
    }
  end

  #@param DATE date_db
  def self.format_date date_db
    if date_db.year == 9999
      return 'Actualidad'
    else
      return "#{self.put_zero date_db.day}/#{self.put_zero date_db.month}/#{date_db.year}"
    end
  end

  def self.diff_dates date_1, date_2
    #dates       = date_2.split('-')
    dates       = date_2
    date1 = date_1 #var date1 		  = new Date(date_1);
    #var date2 		  = dates[0] == 9999 ? new Date() : new Date(date_2);
    #date2 		  = dates[0] == 9999 ? DateTime.new : date_2
    date2 		  = dates.year == 9999 ? DateTime.now : date_2
    months      = (date2.year.to_i * 12 + date2.month.to_i) - (date1.year.to_i * 12 + date1.month.to_i).to_i
    years       = (months / 12).to_i
    restMonths  = (months - (years * 12))
    time_string = ''

    if years != 0
      time_string += years.to_s
      time_string += years > 1 ? ' años' : ' año'
    end
    if restMonths != 0
      time_string += time_string != '' ? ' y '+restMonths.to_s : restMonths.to_s
      time_string += restMonths > 1 ? ' meses' : ' mes'
    end
    return time_string
  end

  def self.put_zero value
    return value < 10 ? "0#{value}" : value
  end
end