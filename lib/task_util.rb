class TaskUtil
  def self.company_selected(company_id)
    company = Company.find_by_id_and_active company_id ? company_id : 1, true
    puts 'Company: ' + company.name.to_s
    company
  end

  def self.rest_connection(url, company )
    sap = RestClient::Request.execute method: :get, url: url.to_s, user: company.user_sap , password: company.password_sap, :accept => :json
    #sap = RestClient.get url.to_s, {:accept => :json}
    JSON.parse(sap.to_s)
  end

  def self.rest_connection_binary_image(url, company)
    sap = RestClient::Request.execute method: :get, url: url.to_s, user: company.user_sap , password: company.password_sap, :accept => :json
    #sap = RestClient.get url.to_s, {:accept => :json}
    sap.to_s
  end

  def self.url_rest(company, operation, employee_identification=nil)
    if employee_identification.present?
      url = company.mainurl + "OPERATION=#{operation}&PERNR=#{employee_identification}"
    else
      url = company.mainurl + "OPERATION=#{operation}"
    end
    url
  end

  #Eliminación de lo que no traiga el servicio
  def self.delete_registers_not_found(model,array_task,company_id=nil,specific_user=nil,specific_type_pdf=nil)
    begin
      conditions = ""
      userId = nil
      if(specific_user != nil)
        user = Employee.find_by_pernr_and_company_id(specific_user.to_i,company_id)
        userId = user.id
      end

      if(specific_type_pdf != nil)
        conditions << "op = :type_pdf AND "
      end
      case model.to_s
        when 'Employee', 'Vacation', 'Saldo', 'EmployeeInfo','VacationBalanceRecord','User'
          if userId != nil
            conditions << "company_id = :company_id AND  id = :id_user AND "
          else
            conditions << "company_id = :company_id AND "
          end
        when 'Labor','TypesRequestsRecord','PdfType','IndebtednessLevel','Permission','Incapacity','EmployeeTurnover',
            'HistoricalPosition','VarianceAnalysis','Absence','PaymentsDeduction','Embargo','LoanRecord',
            'VacationRecord','ExtraHourRecord','CompensatoryVacationRecord','TimelinePosition','TypesRequestForGroup',
            'EmployeeCalendarBreak'
          conditions << "(company_id = :company_id OR company_id IS NULL) AND "
        when 'FileEmployee'
          if specific_type_pdf != nil
            if userId != nil
              conditions << "company_id = :company_id AND  employee_id = :id_user AND op = :type_pdf AND "
            else
              conditions << "company_id = :company_id AND op = :type_pdf AND "
            end
          else
            if userId != nil
              conditions << "company_id = :company_id AND  employee_id = :id_user AND "
            else
              conditions << "company_id = :company_id AND "
            end
          end

      end
      if array_task.length >0
        conditions << "id NOT IN(:ids_no_delete)"
      else
        conditions = conditions[0..-5]
      end
      offset_query = 0
      #Corre paquetes cada 50 registros
      begin
        registersToDelete = model.where(conditions,{:id_user => userId, :type_pdf => specific_type_pdf, :ids_no_delete => array_task, :company_id => company_id
        })
        registersToDelete.each do |register|
          puts "Eliminado el id:" + register.id.to_s
          if model.to_s == 'Employee'
            register.requests_employees.update_all(:is_active => false)
            User.where('employee_id = ?',register.id.to_s).destroy_all
          end
          register.destroy
        end
      rescue => e
        puts e.inspect
      end
    rescue => e
      puts e.inspect
    end
  end

  def self.data_rest(url, key_data, company)
    puts 'url: ' + url.to_s
    sap   = nil
    error = 0

    begin
      sap = RestClient::Request.execute method: :get, url: url.to_s, user: company.user_sap, password: company.password_sap, :accept => :json
      #sap = RestClient.get url.to_s, {:accept => :json}
      parse_sap = JSON.parse(sap.to_s)
      url = ''
      if !parse_sap['Data'].is_a? Array
        url = parse_sap['Data'].has_key?('next') ? parse_sap['Data']['next'] : ''
        data = parse_sap['Data'].has_key?(key_data) ? parse_sap['Data'][key_data.to_s] : parse_sap['Data']
      else
        data = parse_sap['Data']
      end
      puts 'Service Row: ' + data.length.to_s
    rescue => e
      error += 1
      puts "Error #{error} SAP"
      puts e.inspect
      puts '60 seconds try again...'
      sleep(60.0)
    end while sap == nil && error <= 10
    #if error >= 10 #para proceso por acumulacion de errores
    #  puts 'task stops'
    #  return false
    #end
    { url: url, data: data, errors: error}
  end

  def self.delete_rows_by_months(limit_date, model)
    date = Time.zone.now - 4.months
    puts limit_date.to_s+" <= "+date.to_s
    if(limit_date <= date)
      puts 'destroy: '+limit_date.to_s
      model.destroy
      return true
    end
    return false
  end

  def self.verify_integers_negative(value)
    begin
      if value.is_a?(Array)
        val_return = []
        value.each_with_index do |val,key|
          #puts 'val: '+val.to_s
          val_return.push(verify_integers_negative(val))
        end
      elsif(value.is_a?(Hash))
        val_return = Hash.new
        tempArray = Array(value)
        tempArray.each do |keyHash,valHash|
          val_return[keyHash] = verify_integers_negative(valHash)
        end
      else
        if (value[-1,1] == '-')
          num = value.to_f
          val_return = num * (-1)
        else
          val_return = value
        end
      end
    rescue => e
      puts e.backtrace.inspect
    end
    return val_return
  end

  def self.send_pendings_sap(url,company,array_send)
    sap = RestClient::Request.execute(
        method: :post,
        url: url,
        user: company.user_sap ,
        password: company.password_sap,
        payload:array_send.to_json,
        headers: {:content_type => :json,
                  :accept => :json}
    )

    parse_sap = JSON.parse(sap.to_s)
    #Save response
    response = parse_sap.as_json

    if response["Data"].present?
      response["Data"].each do |reply|
        request = RequestsEmployee.find_by_id reply['ticked_id'].to_i
        puts "#{reply['ticked_id']} #{reply['messg']}"
        if reply['messg'].present?
          request.synch_message_sap = reply['messg']
          request.save
        end
      end
    end

  end
end