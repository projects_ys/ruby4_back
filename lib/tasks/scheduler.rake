# task :seed, [:amount] => :environment do |task, args|
#   puts task
#   if  args.amount
#     puts "con"
#   else
#     puts "sin"
#   end
# end

task :update_pdf_types,[:company_id] => :environment do |task, args|
  msg = ''
  company = TaskUtil.company_selected(args.company_id)
  if company.mainurl
    url   = company.mainurl + 'OPERATION=GETRDF'
    datos = TaskUtil.data_rest(url, false, company)
    types = datos[:data]
    types_pdf_ids = []
    types.each_with_index do |type, i|
      pdf_type = PdfType.find_by_activ_and_ident_and_company_id(type['activ'], type['ident'], args.company_id)

      if pdf_type.present?
        if pdf_type.company_id.to_s == company.id.to_s
          pdf_type.activ = type['activ']
          pdf_type.ident = type['ident']
          pdf_type.nomid = type['nomid']
          pdf_type.save
          types_pdf_ids.push(pdf_type.id)
        else
          msg.push "[#{company.name.to_s}] - PdfType already exists in another company, with nomid: #{type['nomid']}"
        end
      else
        if type['ident'].present?
          pdf_type = PdfType.new(:activ => type['activ'], :ident => type['ident'], :nomid => type['nomid'], :company_id => args.company_id)
          pdf_type.save
          types_pdf_ids.push(pdf_type.id)
        end
      end
    end
    #elimina lo que no encuentre en base de datos
    TaskUtil.delete_registers_not_found(PdfType,types_pdf_ids,company.id)
  end
  puts "FIN DEL PROCESO:"
  puts msg.inspect
end

task :update_timeline_positions,[:company_id] => :environment do |task, args|
  msg = ''
  company = TaskUtil.company_selected(args.company_id)
  begin
    if company.mainurl
      url   = company.mainurl + 'OPERATION=HISPOS'
      positions_ids = []
      while url.present? && url != 'NULL'
        datos = TaskUtil.data_rest(url, 'hist_pos', company)
        positions = datos[:data]
        countRecords = 1
        url = datos[:url]
        positions.each do |pos|
          dataTimelinePosition = {:position_id => pos['objid'],:begda => pos['begda'],:endda => pos['endda'],:name_position => pos['stext'],:company_id => company.id}
          idTimelinePos = TimelinePosition.where({:position_id => pos['objid'],:begda => pos['begda'],:endda => pos['endda'],:company_id => company.id}).first_or_create
          TimelinePosition.where({:position_id => pos['objid'],:begda => pos['begda'],:endda => pos['endda'],:company_id => company.id}).update_all(dataTimelinePosition)
          positions_ids.push(idTimelinePos.id)
          puts "#{countRecords}/#{positions.length}"
          countRecords +=1
        end
      end
    end
  rescue => e
    ActiveRecord::Rollback
    puts 'ERROR INESPERADO'
    puts 'Company: ' + company.name
    puts e.inspect
  end
  #Elimina lo que se encuentre en la base de datos y no en el servicio
  TaskUtil.delete_registers_not_found(TimelinePosition,positions_ids,company.id,args.identification)
  puts "FIN DEL PROCESO:"
  puts msg.inspect
end

task :update_boss_employees,[:company_id] => :environment  do |task, args|
  errors        = 0
  saves         = 0
  not_employees = 0
  length        = 0
  company = TaskUtil.company_selected(args.company_id)
  begin
    Employee.transaction do
      if company.mainurl
        url = company.mainurl + 'OPERATION=ORGAN'
        while url.present? && url != 'NULL'
          datos         = TaskUtil.data_rest(url, 'organ', company)
          relationships = datos[:data]
          url = datos[:url]
          length        = relationships.length
          relationships.each_with_index do | relationship, i |
            employee = Employee.find_by_pernr_and_company_id relationship['pernr'].to_i, company.id
            if employee
              employee.boss = relationship['pboss'].to_i
              if employee.save
                puts "updates #{saves}/#{length}" if saves % 100 == 0
                saves = saves + 1
              else
                puts 'Employee - ' + employee.id
                puts employee.errors.full_messages
                errors = errors + 1
              end
            else
              not_employees = not_employees + 1
            end
            dataPosition = {:name_position => relationship['stext'],:id_position => relationship['plans'], :company_id => company.id, :position_boss => relationship['pboss']}
            Position.where({id_position: relationship['plans'].to_i, company_id: company.id.to_i}).first_or_create
            Position.where({id_position: relationship['plans'].to_i, company_id: company.id.to_i}).update_all(dataPosition)
          end
        end
        puts 'LOG ' +  company.name
        puts 'LENGTH REALATIONSHIPS: '  + length.to_s
        puts 'ERRORS: ' + errors.to_s
        puts 'SAVES: ' + saves.to_s
        puts 'NOT EMPLOYEES: ' + not_employees.to_s
      end
    end
  rescue => e
    ActiveRecord::Rollback
    puts 'ERROR INESPERADO'
    puts 'Company: ' + company.name
    puts e.inspect
  end
end

task :update_pdfs,[:company_id,:identification,:type_pdf] => :environment do |task, args|
  TYPES   = { volpg: [:volantes_p, :VPAG], inret: [:ingyret, :CIYR], vctns: [:vacations, :VCTN], clabr: [:cartas_lab, :CLAB] }
  puts "Options document available's: volpg(Volante Pago),inret(Ingresos y retenciones),vctns(Vacaciones),clabr(Carta Laboral)"
  #TYPES   = { clabr: :cartas_lab}
  company = TaskUtil.company_selected(args.company_id)
  if company.mainurl
    specific_user = args.identification
    if(specific_user == "all")
      specific_user = nil
    end
    pdfs_id = [] #Almacenara los id's de pdf's para eliminación de los que no esten en la actualización del servicio
    TYPES.each do |key,value|
      #Condiciona el recorrido del array de los tipos de documento en caso que se envie el tercer parametro
      type_param = args.type_pdf
      if type_param.present?
        if type_param != key.to_s
          next
        else
          @specific_type_pdf = value[1].to_s
        end
      end

      puts 'Initializes: ' + value[0].to_s
      #En caso de que el segundo parametro sea igual a 'all' genera nil en la variable para que lo haga para todos los usuarios

      url = TaskUtil.url_rest(company,key.to_s,specific_user)
      while url != 'NULL'
        datos = TaskUtil.data_rest(url, value[0].to_s, company )
        pdfs  = datos[:data]
        url   = datos[:url]

        case value[0].to_s
          when 'vacations', 'volantes_p' #cuando son varios archivos
            length = pdfs.length
            pdfs.each_with_index do |pdf, i|
              pdf['binar'].each do |data|
                if data['pdf'].present?
                  begin
                    intern_directory = "public/#{value[0].to_s}-#{company.id}/#{pdf['pernr'].to_i.to_s}"
                    FileUtils.mkdir_p intern_directory
                    user_file = Employee.find_by_pernr_and_company_id(pdf['pernr'].to_i,company.id)
                    s3 = FileEmployee.find_by_name_iden_and_employee_id_and_op_and_archivo_and_pdf_type_ident(data['endda'],user_file.id,value[1].to_s,value[0].to_s, data['ident']) || FileEmployee.new(employee_id: user_file.id, op: value[1].to_s, archivo: value[0].to_s, name_iden: data['endda'], pdf_type_ident: data['ident'])
                    if s3.present?
                      s3.op              = value[1].to_s
                      s3.archivo         = value[0].to_s
                      s3.pdf_type_ident  = data['ident']
                      s3.begda           = data['begda']
                      s3.endda           = data['endda']
                      s3.company_id      = company.id
                    else
                      s3 = FileEmployee.new(employee_id: user_file.id, op: value[1].to_s, archivo: value[0].to_s, pdf_type_ident: data['ident'], begda: data['begda'], endda: data['endda'], company_id: company.id)
                    end

                    File.open(File.join(intern_directory, "#{value[0].to_s}_#{data['begda']}_#{data['endda']}_#{pdf['pernr'].to_i}_#{company.id}.pdf"), 'wb') { |output|
                      output << data['pdf'].unpack('m').first
                      s3.file = output
                    }
                    s3.save
                    pdfs_id.push(s3.id)
                  rescue => e
                    puts 'Error save'
                    puts e.inspect
                    puts e.message
                  end
                end
              end
              puts "pdfs #{i}/#{length}" if i % 10 == 0
            end
          when 'cartas_lab'
            directory = "public/#{value[0].to_s}-#{company.id}"
            FileUtils.mkdir_p directory
            length = pdfs.length

            pdfs.each_with_index do |pdf, i|
              if pdf['pdf'].present?
                begin
                  user_file = Employee.find_by_pernr_and_company_id(pdf['pernr'].to_i,company.id)
                  s3 =  FileEmployee.find_by_employee_id_and_op_and_archivo_and_pdf_type_ident(user_file.id,value[1].to_s,value[0].to_s, pdf['ident'])

                  if s3.present?
                    s3.op              = value[1].to_s
                    s3.archivo         = value[0].to_s
                    s3.pdf_type_ident  = pdf['ident']
                    s3.begda           = pdf['fecha']
                    s3.company_id      = company.id
                  else
                    s3 = FileEmployee.new( employee_id: user_file.id, op: value[1].to_s, archivo: value[0].to_s, pdf_type_ident: pdf['ident'], begda: pdf['fecha'],company_id:company.id)
                  end

                  File.open(File.join(directory, "#{value[0].to_s}_#{pdf['pernr'].to_i}_#{company.id}_#{pdf['ident']}.pdf"), 'wb') { |output|
                    output << pdf['pdf'].unpack('m').first
                    s3.file = output
                  }
                  s3.save
                  pdfs_id.push(s3.id)
                rescue => e
                  puts 'Error save'
                  puts e.inspect
                end
              end
              puts "pdfs #{i}/#{length}" if i % 10 == 0
            end

          when 'ingyret' # cuando es un solo archivo
            directory = "public/#{value[0].to_s}-#{company.id}"
            FileUtils.mkdir_p directory
            length = pdfs.length
            pdfs.each_with_index do |pdf, i|
              if pdf['pdf'].present?
                begin
                  user_file = Employee.find_by_pernr_and_company_id(pdf['pernr'].to_i,company.id)
                  s3 =  FileEmployee.find_by_employee_id_and_op_and_archivo_and_pdf_type_ident(user_file.id,value[1].to_s,value[0].to_s,pdf['ident']) || FileEmployee.new( employee_id: user_file.id, op: value[1].to_s, archivo: value[0].to_s,company_id:company.id )
                  s3.pdf_type_ident  = pdf['ident']
                  File.open(File.join(directory, "#{value[0].to_s}_#{pdf['pernr'].to_i}_#{company.id}_#{pdf['ident']}.pdf"), 'wb') { |output|
                    output << pdf['pdf'].unpack('m').first
                    s3.file = output
                  }
                  s3.company_id      = company.id
                  s3.save
                  pdfs_id.push(s3.id)
                rescue => e
                  puts 'Error save'
                  puts e.inspect
                end
              end
              puts "pdfs #{i}/#{length}" if i % 10 == 0
            end
          else
            puts "no support " + value[1].to_s
        end
      end
    end
    #Elimina lo que no encuentre en base de datos
    puts 'pernr: '+specific_user.to_s
    TaskUtil.delete_registers_not_found(FileEmployee,pdfs_id,company.id,specific_user,@specific_type_pdf)
  end
end

task :update_master_data,[:company_id] => :environment  do |task, args|
  msg = ''
  company = TaskUtil.company_selected(args.company_id)
  begin
    if company.mainurl
      url   = company.mainurl + '&LANG=S&LAND=CO&OPERATION=BSCTBL'
      datos = TaskUtil.data_rest(url, false, company)
      data_master = datos[:data]
      data_master.delete("bukrs")

      if(data_master['paises'].present?)
        dataMaster = {:countries => data_master['paises'],:state => data_master['estados'],:family => data_master['familiares'],
                      :institutes => data_master['institutos'],:measurement_units => data_master['unidades_med'],:marital_status => data_master['estado_civil'],
                      :formations => data_master['formaciones'],:titles => data_master['titulos'],:specialties => data_master['especialidades'],
                      :labor_relations => data_master['rel_laborales'],:banks => data_master['bancos'],:company_id => company.id}
        data_master = dataMaster
      end

      MasterDatum.where(:company_id => company.id).first_or_create
      MasterDatum.where(:company_id => company.id).update_all(data_master)
    end
  rescue => e
    ActiveRecord::Rollback
    puts 'ERROR INESPERADO'
    puts 'Company: ' + company.name
    puts e.inspect
  end
  puts "FIN DEL PROCESO:"
  puts msg.inspect
end
task :update_employee_info,[:company_id,:identification]  => :environment  do |task, args|
  puts 'UPDATE EMPLOYEES INFO'
  company = TaskUtil.company_selected(args.company_id)
  if company.mainurl
    url = TaskUtil.url_rest(company,"DMEMP",args.identification)
    employee_info_ids = []
    @errorsService = 0
    while url != 'NULL'
      datos = TaskUtil.data_rest(url, 'datos_maestros', company )
      data  = datos[:data]
      url   = datos[:url]

      data.each do |v|
        begin
          v.each do |key,value|
              if !value.present?
                v.delete(key)
              end
          end

          if v["01"].present?
              v[:datos_personales] = v["01"]
              v.delete("01")
          else
            v[:datos_personales] = nil
          end

          if v["02"].present?
              v[:datos_contacto] = v["02"]
              v.delete("02")
          else
            v[:datos_contacto] = nil
          end

          if v["03"].present?
              v[:datos_familiares] = v["03"]
              v.delete("03")
          else
            v[:datos_familiares] = nil
          end

          if v["04"].present?
              v[:datos_beneficiarios] = v["04"]
              v.delete("04")
          else
            v[:datos_beneficiarios] = nil
          end

          if v["05"].present?
              v[:datos_estudios] = v["05"]
              v.delete("05")
          else
            v[:datos_estudios] = nil
          end

          if v["06"].present?
              v[:datos_empresariales] = v["06"]
              v.delete("06")
          else
            v[:datos_empresariales] = nil
          end

          if v["07"].present?
              v[:datos_bancarios] = v["07"]
              v.delete("07")
          else
            v[:datos_bancarios] = nil
          end

          if v["08"].present?
              v[:datos_seguridadsocial] = v["08"]
              v.delete("08")
          else
            v[:datos_seguridadsocial] = nil
          end

          if v["09"].present?
              v[:datos_impuestos] = v["09"]
              v.delete("09")
          else
            v[:datos_impuestos] = nil
          end

          if v["10"].present?
            v[:datos_retefuente] = v["10"]
            v.delete("10")
          else
            v[:datos_retefuente] = nil
          end

          employee = Employee.find_by_pernr_and_company_id v['pernr'].to_i, company.id
          if employee
            v[:employee_id] = employee.id
          else
            puts 'Not Employee identification(pernr): ' + v['pernr'].to_s
          end

          employee_info = EmployeeInfo.where({pernr: v['pernr'].to_i, company_id: company.id }).first_or_create
          employee_info_ids.push(employee_info.id)
          EmployeeInfo.where({pernr: v['pernr'].to_i, company_id: company.id }).update(employee_info.id,:employee_id => employee.id)
          EmployeeInfo.where({pernr: v['pernr'].to_i, company_id: company.id }).update_all(v)
        rescue => e
          puts e.inspect
          @errorsService = @errorsService + 1
        end
      end
    end
    if @errorsService == 0
      #Elimina lo que se encuentre en la base de datos y no en el servicio
      TaskUtil.delete_registers_not_found(EmployeeInfo,employee_info_ids,company.id,args.identification)
    end

  end
end

task :update_type_requests_records,[:company_id] => :environment do |task, args|
  puts 'UPDATE TYPE REQUESTS AND RECORDS'
  company = TaskUtil.company_selected(args.company_id)
  if company.mainurl
    url   = company.mainurl + 'OPERATION=GETACT'
    datos = TaskUtil.data_rest(url, false, company )
    data  = datos[:data]
    info  = {}
    types_id = []
    groups_per_request_ids = []
    data.each do |t|
      info = {
          :id_activity => t['idactv'],
          :subtype => t['subty'],
          :description => t['descr'],
          :moabw => t['moabw'],
          :type_request => t['idnsr'],
          :form => t['forms'],
          :company_id => company.id,
          :contingent => t['cotng'].to_i
      }
      info_type = TypesRequestsRecord.where( { company_id: company.id, id_activity:t['idactv'], subtype: t['subty']} ).first_or_create
      types_id.push(info_type.id)
      info_type.update(info)
      #grupos
      group = GroupEmployeesRequest.where({company_id: company.id,identifier_group: t['moabw']}).first_or_create
      if group.present? && info_type.present?
        group_per_request = TypesRequestForGroup.where({identifier_group: group.identifier_group,type_request: info_type.form,company_id:company.id}).first_or_create
        groups_per_request_ids.push(group_per_request.id)
      end
    end

    #Elimina lo que se encuentre en la base de datos y no en el servicio
    TaskUtil.delete_registers_not_found(TypesRequestsRecord,types_id,company.id)
    TaskUtil.delete_registers_not_found(TypesRequestForGroup,groups_per_request_ids,company.id)
  end
end

task :update_employees_balances,[:company_id,:identification]  => :environment do |task, args|
  puts 'UPDATE EMPLOYEES BALANCES'
  company = TaskUtil.company_selected(args.company_id)
  if company.mainurl
    url = TaskUtil.url_rest(company,"SALDS",args.identification)
    employee_balances_id = []
    @errorsService = 0
    while url != 'NULL'
      datos = TaskUtil.data_rest(url, 'saldos', company )
      url   = datos[:url]
      data  = datos[:data]
      data.each do |v|
        v['saldo'] = TaskUtil.verify_integers_negative(v['saldo'])
        v['vcausado'] = TaskUtil.verify_integers_negative(v['vcausado'])
        v['intsaldo'] = TaskUtil.verify_integers_negative(v['intsaldo'])
        v['intvcausado'] = TaskUtil.verify_integers_negative(v['intvcausado'])
        v['totdevengos'] = TaskUtil.verify_integers_negative(v['totdevengos'])
        v['totdeducciones'] = TaskUtil.verify_integers_negative(v['totdeducciones'])
        v['t_cesantias'] = TaskUtil.verify_integers_negative(v['t_cesantias'])
        v['t_intcesantias'] = TaskUtil.verify_integers_negative(v['t_intcesantias'])
        v['t_endeudamiento'] = TaskUtil.verify_integers_negative(v['t_endeudamiento'])
        begin
          v[:company_id] = company.id
          e = Employee.find_by pernr: v['pernr'].to_i, company_id: company.id
          if e
            v[:employee_id] = e.id
          end
          saldo_employee = Saldo.where({pernr: v['pernr'].to_i, company_id: company.id}).first_or_create
          employee_balances_id.push(saldo_employee.id)
          Saldo.where({pernr: v['pernr'].to_i, company_id: company.id}).update_all(v)
        rescue => e
          ActiveRecord::Rollback
          @errorsService = @errorsService + 1
          puts "Error Inesperado"
          puts e.inspect
        end
      end
    end
    #Elimina lo que se encuentre en la base de datos y no en el servicio
    if @errorsService == 0
      TaskUtil.delete_registers_not_found(Saldo,employee_balances_id,company.id,args.identification)
    end

  end
end

task :update_vacations,[:company_id,:identification] => :environment do |task, args|
  puts 'UPDATE VACATIONS'
  company = TaskUtil.company_selected(args.company_id)
  if company.mainurl
    url = TaskUtil.url_rest(company,"vacat",args.identification)
    vacations_id = []
    @errorsService = 0
    while url != 'NULL'
      datos = TaskUtil.data_rest(url, 'vacations', company)
      url   = datos[:url]
      data  = datos[:data]

      data.each do |v|
        begin
          v.each do |key,value|
            if key == 'bukrs'
              v.delete(key)
            end
            # quitar todo lo que esta vacio
            if value.empty?
              v.delete(key)
            end
          end

          # Convertirlo en un array de un solo campo
          if v['resumen'].present?
            v[:resumen] = v['resumen'].values
            v.delete('resumen')
          end

          # Joder casi no para convertir toda esta linea del hash al array para almacenarlo
          if v['detalle'].present?
            (0..(v['detalle'].count-1)).each do |j|
              v['detalle'][j] = v['detalle'][j].values
            end
          end

          e = Employee.find_by pernr: v['pernr'].to_i, company_id: company.id
          if e
            v[:employee_id] = e.id
            vacation = Vacation.find_by  pernr: v["pernr"].to_i, company_id: company.id
            if vacation
              vacation.employee_id = e.id
            else
              vacation = Vacation.create  pernr: v["pernr"].to_i, company_id: company.id, employee_id: e.id
            end
            vacations_id.push(vacation.id)
            v.delete('pernr')
            vacation.update(v)
            vacation.save
          end
        rescue => e
          ActiveRecord::Rollback
          @errorsService = @errorsService+1
          "Error inesperado"
          puts e.inspect
        end
      end
    end
    if @errorsService == 0
      #Elimina lo que se encuentre en la base de datos y no en el servicio
      TaskUtil.delete_registers_not_found(Vacation,vacations_id,company.id,args.identification)
    end
  end
end

task :update_labors,[:company_id] => :environment do |task, args|
  puts 'UPDATE LABORS'
  company = TaskUtil.company_selected(args.company_id)
  if company.mainurl
    url = company.mainurl + 'OPERATION=GETRL'
    labors_id = []
    datos = TaskUtil.data_rest(url, false, company)
    data  = datos[:data]

    data.each do |labor|
      labor['company_id'] = company.id
      #labor[company_id] = company.id
      laborFound = Labor.where({ansvh: labor['ansvh'],company_id: labor['company_id']}).first_or_create
      labors_id.push(laborFound.id)
      Labor.where({ansvh: labor['ansvh'],company_id: labor['company_id']}).update_all(labor)
    end
    #Elimina lo que se encuentre en la base de datos y no en el servicio
    TaskUtil.delete_registers_not_found(Labor,labors_id,company.id)
  end
end

task :update_calendar_breaks,[:company_id] => :environment do |task, args|
  puts 'UPDATE CALENDAR BREAKS'
  company = TaskUtil.company_selected(args.company_id)
  if company.mainurl
    ids_breaks = []
    url = TaskUtil.url_rest(company,"GETCLD")
    @errorsService = 0
    while url.present? && url != 'NULL'
      datos = TaskUtil.data_rest(url, false, company)
      data  = datos[:data]
      url   = datos[:url]

      data.each do |breakk|
        date =  breakk['fecha'].to_date
        date_break = EmployeeCalendarBreak.where({company_id:company.id,grouper:breakk['motpr'],horary:breakk['zmodn'],date_break:date}).first_or_create
        ids_breaks.push(date_break.id)
      end
    end
    TaskUtil.delete_registers_not_found(EmployeeCalendarBreak,ids_breaks,company.id)
  end
end

task :update_employees,[:company_id,:identification] => :environment do |task, args|
  include RequestsConcern
  puts 'UPDATE EMPLOYEES'
  msg = Array.new
  company = TaskUtil.company_selected(args.company_id)
  if company.mainurl
    ids_employees = []
    ids_users = []
    url = TaskUtil.url_rest(company,"EMPLE",args.identification)
    @errorsService = 0
    while url != 'NULL'
      datos = TaskUtil.data_rest(url, 'employees', company)
      url   = datos[:url]
      data  = datos[:data]
      data.each do |v|
        begin
          v.each do |key,value|
            # Quitar todo lo que no necesito o tengo en la base de datos la imagen todavia no va
            if key == "mandt" || key == "seqnr" || key == "boss"
              v.delete(key)
            end
          end
          if args.identification.present?
            puts "Usuario especifico: #{v['name']} #{v['second_name']} #{v['lastname']}  #{v['second_lastname']}"
          end
          # poner la imagen formateada
          email = v['email'].downcase.gsub(/\s+/, '')
          employee = Employee.find_by_pernr_and_company_id v['identification'].to_i, company.id
          unless employee
            employee = Employee.new  pernr: v['identification'].to_i, company_id: company.id, email: email
            employee.save
          end

          if v['imagen'].present? && v['imagen'] != 'images/generic_user_image.jpg'
            mtype = v['mimetype'].split('/')
            directory = "public/image_employees-#{company.id}"
            file = "#{v['identification'].to_i}.#{mtype[1]}"
          else
            v['imagen'] = "#{company.front_domain}/images/generic_user_image.jpg"
            directory = "public/image_employees-#{company.id}"
            file = "generic_user_image.jpg"
          end

          FileUtils.mkdir_p directory
          conect = TaskUtil.rest_connection_binary_image v['imagen'], company
          open(File.join(directory, file), 'wb') do |file|
            file << conect
            employee.image = file
          end

          usrLDAP = v['usrid']
          v.delete('imagen')
          v.delete('mimetype')
          v.delete('usrid')
          user = User.find_by_employee_id employee.id

          if user.present?
              user.email = email
              user.employee_id = employee.id
              user.nickname = usrLDAP
              user.company_id = company.id
              user.save
              ids_users.push(user.id)
              employee.user_id = user.id
          else
            if v['email'].present?
              arrCharacters = [('a'..'z'), ('A'..'Z'), (0..9)].map { |i| i.to_a }.flatten
              if Rails.env.production?
                password = (1...10).map { arrCharacters[rand(arrCharacters.length)] }.join
                #password = '123456789'
              else
                password = '123456789'
              end

              findUserH = User.find_by_email (email.downcase)

              if findUserH.present?
                findUserH.email = email
                findUserH.employee_id = employee.id
                findUserH.nickname = usrLDAP
                findUserH.company_id = company.id
                findUserH.save
                ids_users.push(findUserH.id)
              else
                user = User.new(:email => email, :password => password, :password_confirmation => password, :company_id => company.id, :employee_id => employee.id,:nickname => usrLDAP)
                user.save
                ids_users.push(user.id)
                employee.user_id = user.id
              end

            end
          end
          v.delete('identification')
          v.delete('short_name')
          v.keys.each do |k|
            if k[-10..-1] == 'approver_2'
              v["second_#{k[0..-3]}"] = v[k]
              v.delete(k)
            end
          end
          v['name_complete'] = "#{v['name']} #{v['second_name']} #{v['lastname']}  #{v['second_lastname']}"
          v['group_request'] = v['moabw']
          v['type_document'] = v['ictxt']
          v['number_document'] = v['icnum']
          v['grouper_calendar'] = v['motpr']
          v['horary_calendar'] = v['zmodn']
          v.delete('moabw')
          v.delete('ictxt')
          v.delete('icnum')
          v.delete('motpr')
          v.delete('zmodn')
          #Determinar si es aprobador
          if v['is_approver1'] == 'X'
            v['is_first_approver'] = true
          else
            v['is_first_approver'] = false
          end
          if v['is_approver2'] == 'X'
            v['is_second_approver'] = true
          else
            v['is_second_approver'] = false
          end
          v.delete('is_approver1')
          v.delete('is_approver2')
          employee.update(v)
          employee.save
          ids_employees.push(employee.id)
          #Verificar solicitudes y mi aprobador
          requests = employee.requests_employees.where(:status_id => ['ReqEmpStaPenAproJef','ReqEmpStaPenRH'])
          if requests.present?
            requests.each do |req|
              case req.status_id
                when 'ReqEmpStaPenAproJef'
                  approver_assigned = approver_asigned(req.id_activity,:first,employee)
                  unless approver_assigned.nil?
                    req.first_approver_assigned = approver_assigned.pernr
                  else
                    req.first_approver_assigned = 0
                  end
                when 'ReqEmpStaPenRH'
                  approver_assigned = approver_asigned(req.id_activity,:second,employee)
                  unless approver_assigned.nil?
                    if approver_assigned.pernr != req.second_approver_assigned
                      req.second_approver_assigned = approver_assigned.pernr
                    end
                  else
                    req.second_approver_assigned = 0
                  end
              end
              unless req.save
                puts req.errors.messages
              end
              req.save
            end
          end
        rescue => e
          ActiveRecord::Rollback
          @errorsService = @errorsService + 1
          puts e.inspect
          puts e.backtrace.inspect
        end
      end
    end
    #Elimina lo que se encuentre en la base de datos y no en el servicio

    if @errorsService == 0
      TaskUtil.delete_registers_not_found(Employee,ids_employees,company.id,args.identification)
      TaskUtil.delete_registers_not_found(User,ids_users,company.id,args.identification)
    end

  end
  puts "FIN DEL PROCESO:"
  #puts error.inspect
  puts msg.inspect
end
