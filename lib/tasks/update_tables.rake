task :update_type_inhabilities => :environment  do
  errors = 0
  saves = 0
  not_inhabilities = 0
  length = 0
  companies = Company.where(active: true)
  companies.each do |company|
    begin
      Employee.transaction do
        if company.mainurl
          sap = RestClient.get company.mainurl, {:params => {:OPERATION => 'GETTE', :'sap-client' => 100}, :accept => :json, :timeout => 360, :open_timeout => 360}
          inhabilities = JSON.parse(sap)['Data']
          length = inhabilities.length
          inhabilities.each_with_index do | inhability, i |
            type_inhability = TypeInhability.find_or_initialize_by descd: inhability["descd"]
            type_inhability.descr = inhability["descr"]
            if type_inhability.save
              puts "updates #{saves}/#{length}" if saves % 100 == 0
              saves += 1
            else
              puts type_inhability.inspect
              not_inhabilities += 1
            end
          end
          puts 'LOG ' +  company.name
          puts 'TYPE INHABILITIES: '  + length.to_s
          puts 'SAVES: ' + saves.to_s
          puts 'NOT SAVES: ' + not_inhabilities.to_s
        end
      end
    rescue => e
      ActiveRecord::Rollback
      puts 'ERROR GENERAL'
      puts 'Company: ' + company.name
      puts e.inspect
    end
  end
end