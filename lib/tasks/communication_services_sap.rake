task :send_first_approvals_to_sap,[:company_id] => :environment do |task, args|
  puts 'SEND TO SAP APPROVALS 1'
  if args.company_id.present?
    companies = Company.where(:id => args.company_id)
  else
    companies = Company.where(:make_approvals => true)
  end

  companies.each do |company|
    company = TaskUtil.company_selected(company.id)
    if company.mainurl
      url_requests = TaskUtil.url_rest(company,"PRCSOL")
      url_records = TaskUtil.url_rest(company,"PRCREG")
      request_pending_sap = {:data => []}
      records_pending_sap = {:data => []}
      requests = RequestsEmployee.where(:status_id => 'ReqEmpStaPenRH', :company_id => company.id, :synch_message_sap => nil)
      requests.each do |request|
        puts request.inspect
        if request.types_requests.type_request.downcase == 's'
          request_pending_sap[:data] << {
              ticked_id: request.id,
              pernr: request.pernr,
              begda: (request.date_begin.present?) ? request.date_begin.to_date.to_formatted_s(:number):nil,
              endda: (request.date_end.present?) ? request.date_end.to_date.to_formatted_s(:number): nil,
              plans: request.second_approver.id_posicion,
              type: request.types_requests.id_activity,
              subty: request.types_requests.subtype
          }
        end

        if request.types_requests.type_request.downcase == 'r'
          records_pending_sap[:data] << {
              ticked_id: request.id,
              pernr: request.pernr,
              begda: (request.date_begin.present?) ? request.date_begin.to_date.to_formatted_s(:number):nil,
              endda: (request.date_end.present?) ? request.date_end.to_date.to_formatted_s(:number): nil,
              plans: request.second_approver.id_posicion,
              type: request.types_requests.id_activity,
              subty: request.types_requests.subtype
          }
        end

      end

      #requests
      if request_pending_sap[:data].length > 0
        TaskUtil.send_pendings_sap(url_requests,company,request_pending_sap)
      end

      #records
      if records_pending_sap[:data].length > 0
        TaskUtil.send_pendings_sap(url_records,company,records_pending_sap)
      end

    end
  end
end