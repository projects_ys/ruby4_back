task :update_staff_turnover,[:company_id] => :environment do |task, args|
  company = TaskUtil.company_selected(args.company_id)
  if company.mainurl
    url = TaskUtil.url_rest(company,"RGRTPR")
    rotationsService = []
    while url.present? && url != 'NULL'
      data = TaskUtil.data_rest(url, 'rotper', company)
      rotations = data[:data]
      url = data[:url]
      msg = ""
      rotations.each do |rotation|
        employee = Employee.find_by_pernr_and_company_id(rotation['pernr'],company.id)
        begin
          if employee.present?
            dataNewRotation = {:type_rotation => rotation['tiprp'],:denomination_class => rotation['mntxt'],:denomination_motive => rotation['mgtxt'],:begda => rotation['begda'].to_date,:employee_id => employee.id,:company_id => company.id,:society => rotation['bukrs'].to_i,:position_id => rotation['plans'].to_i}
            rotation_record = EmployeeTurnover.where({:begda => rotation['begda'].to_date, :employee_id => employee.id,:company_id => company.id,:type_rotation => rotation['tiprp']}).first_or_create
            EmployeeTurnover.where({:begda => rotation['begda'].to_date, :employee_id => employee.id,:company_id => company.id,:type_rotation => rotation['tiprp']}).update_all(dataNewRotation)
            rotationsService.push(rotation_record.id)
          else
            msg << "Registro NO almacenado, es necesario correr el servicio de empleados para PERNR = #{rotation['pernr']} en #{company.name}\n"
          end
        rescue => e
          ActiveRecord::Rollback
          puts 'ERROR INESPERADO'
          puts 'Company: ' + company.name
          puts e.inspect
        end
      end
    end
    puts msg
    TaskUtil.delete_registers_not_found(EmployeeTurnover,rotationsService,company.id)
    puts "FIN PROCESO"
  end
end
task :update_historical_positions,[:company_id] => :environment do |task,args|
  company = TaskUtil.company_selected(args.company_id)
  if company.mainurl
    url = TaskUtil.url_rest(company,"RGHICR")
    historicalServices = []
    while url.present? && url != 'NULL'
      data = TaskUtil.data_rest(url,'hist_carg',company)
      url = data[:url]
      #array para almacenar los id's que quedan en base de datos de las posiciones y eliminar el resto
      historical_positions = data[:data]
      msg = ""
      iHistorical = 1;
      historical_positions.each do |historical_pos|
        employee = Employee.find_by_pernr_and_company_id(historical_pos['pernr'],company.id)

        begin
          if employee.present?
            dataNewHistorical = {:begda=>historical_pos['begda'].to_date,:endda=>historical_pos['endda'].to_date,:position=>historical_pos['plans'].to_i,:position_boss=>historical_pos['plans_boss'].to_i,:amount_accrued=>historical_pos['betrg'].to_f,:currency=>historical_pos['waers'],:employee_id=>employee.id,:society=>historical_pos['bukrs'],:company_id => company.id}
            historical = HistoricalPosition.where({:begda => historical_pos['begda'].to_date,:employee_id => employee.id}).first_or_create
            HistoricalPosition.where({:begda => historical_pos['begda'].to_date,:employee_id => employee.id}).update_all(dataNewHistorical)
            historicalServices.push(historical.id)
            iHistorical +=1
            puts "#{iHistorical}/#{historical_positions.length}"
          else
            msg << "Registro NO almacenado, es necesario correr el servicio de empleados para PERNR = #{historical_pos['pernr']} en #{company.name}\n"
          end
        rescue => e
          ActiveRecord::Rollback
          puts 'ERROR INESPERADO'
          puts 'Company: ' + company.name
          puts e.inspect
        end
      end
    end
    puts msg
    TaskUtil.delete_registers_not_found(HistoricalPosition,historicalServices,company.id)
    puts "FIN PROCESO"

  end
end
task :update_variance_analyses,[:company_id] => :environment do |task,args|
  company = TaskUtil.company_selected(args.company_id)
  if company.mainurl
    url = TaskUtil.url_rest(company,"RGANVA")
    variaces_analyses_save = []
    while url.present? && url != 'NULL'
      data = TaskUtil.data_rest(url,'ana_var',company)
      url = data[:url]
      #array para almacenar los id's que quedan en base de datos de las posiciones y eliminar el resto
      variaces_analyses = data[:data]
      msg = ""
      iVariances = 0
      variaces_analyses.each do |variance|
        employee = Employee.find_by_pernr_and_company_id(variance['pernr'],company.id)

        begin
          if employee.present?
            dataNewVariance = {:position => variance['plans'].to_i,:position_boss => variance['plans_boss'].to_i, :id_cc_payroll => variance['lgart'], :description_payroll =>variance['lgtxt'], :type_accumulation => variance['tacum'], :previous_amount => variance['impan'].to_f, :current_amount =>  variance['impac'].to_f, :difference_amount =>  variance['difim'].to_f, :percentage_difference =>  variance['porim'].to_i, :currency => variance['waers'], :society => variance['bukrs'], :employee_id => employee.id, :company_id => company.id}
            varianceSave = VarianceAnalysis.where(:employee_id => employee.id, :id_cc_payroll => variance['lgart']).first_or_create
            VarianceAnalysis.where(:employee_id => employee.id, :id_cc_payroll => variance['lgart']).update_all(dataNewVariance)
            variaces_analyses_save.push(varianceSave.id)
            iVariances +=1
            puts "#{iVariances}/#{variaces_analyses.length}"
          else
            msg << "Registro NO almacenado, es necesario correr el servicio de empleados para PERNR = #{variance['pernr']} en #{company.name}\n"
          end
        rescue => e
          ActiveRecord::Rollback
          puts 'ERROR INESPERADO'
          puts 'Company: ' + company.name
          puts e.inspect
        end
      end
    end
    puts msg
    TaskUtil.delete_registers_not_found(VarianceAnalysis,variaces_analyses_save,company.id)
    puts "FIN PROCESO"

  end
end
task :update_absences,[:company_id] => :environment do |task,args|
  company = TaskUtil.company_selected(args.company_id)
  if company.mainurl
    url = TaskUtil.url_rest(company,"RGABSN")
    absences_save = []
    while url.present? && url != 'NULL'
      data = TaskUtil.data_rest(url,'absentismos',company)
      url = data[:url]
      #array para almacenar los id's que quedan en base de datos de las posiciones y eliminar el resto
      absences = data[:data]
      msg = ""
      iAbsence = 0
      absences.each do |absence|
        employee = Employee.find_by_pernr_and_company_id(absence['pernr'],company.id)

        begin
          if employee.present?
            dataNewAbsence = {:type_absence => absence['tyabs'], :class_absence => absence['awart'], :description_absence => absence['atext'], :start_absence => absence['begda'].to_date, :end_absence => absence['endda'].to_date, :days_payroll => absence['abrtg'].to_i, :days_natural => absence['kaltg'].to_i, :code_disease => absence['umskd'], :description_disease => absence['umsch'], :society => absence['bukrs'], :employee_id => employee.id, :company_id => company.id }
            absenceSave = Absence.where(:employee_id => employee.id, :type_absence => absence['tyabs'], :start_absence => absence['begda'].to_date).first_or_create
            Absence.where(:employee_id => employee.id, :type_absence => absence['tyabs'], :start_absence => absence['begda'].to_date).update_all(dataNewAbsence)
            absences_save.push(absenceSave.id)
            iAbsence +=1
            puts "#{iAbsence}/#{absences.length}"
          else
            msg << "Registro NO almacenado, es necesario correr el servicio de empleados para PERNR = #{absence['pernr']} en #{company.name}\n"
          end
        rescue => e
          ActiveRecord::Rollback
          puts 'ERROR INESPERADO'
          puts 'Company: ' + company.name
          puts e.inspect
        end
      end
    end
    puts msg
    TaskUtil.delete_registers_not_found(Absence,absences_save,company.id)
    puts "FIN PROCESO"

  end
end
task :update_payments_deductions,[:company_id] => :environment do |task,args|
  company = TaskUtil.company_selected(args.company_id)
  if company.mainurl
    url = TaskUtil.url_rest(company,"RGGCIR")
    pay_deduc_save = []
    while url.present? && url != 'NULL'
      data = TaskUtil.data_rest(url,'cert_inret',company)
      url = data[:url]
      #array para almacenar los id's que quedan en base de datos de las posiciones y eliminar el resto
      pay_deduc = data[:data]
      msg = ""
      iPay_Deduc = 0
      pay_deduc.each do |pd|
        employee = Employee.find_by_pernr_and_company_id(pd['pernr'],company.id)

        begin
          if employee.present?
            dataNewPayDeduc = {:year_report => pd['perid'],:compression_concept => pd['aklas'],:number_form => pd['cirnu'], :type_identification => pd['ictyp'], :number_identification => pd['icnum'], :text_cumulative_concept => pd['cumtx'], :amount => pd['betrg'].to_f, :currency => pd['waers'], :employee_id => employee.id, :society => pd['bukrs'], :company_id => company.id, :identification_payroll_concept => pd['lgart'],:description_payroll_concept => pd['lgtxt']}
            payDeducSave = PaymentsDeduction.where(:employee_id => employee.id, :year_report => pd['perid'], :compression_concept => pd['aklas'], :identification_payroll_concept => pd['lgart']).first_or_create
            PaymentsDeduction.where(:employee_id => employee.id, :year_report => pd['perid'], :compression_concept => pd['aklas'], :identification_payroll_concept => pd['lgart']).update_all(dataNewPayDeduc)
            pay_deduc_save.push(payDeducSave.id)
            iPay_Deduc +=1
            puts "#{iPay_Deduc}/#{pay_deduc.length}"
          else
            msg << "Registro NO almacenado, es necesario correr el servicio de empleados para PERNR = #{absence['pernr']} en #{company.name}\n"
          end
        rescue => e
          ActiveRecord::Rollback
          puts 'ERROR INESPERADO'
          puts 'Company: ' + company.name
          puts e.inspect
        end
      end
    end
    puts msg
    TaskUtil.delete_registers_not_found(PaymentsDeduction,pay_deduc_save,company.id)
    puts "FIN PROCESO"

  end
end
