task :update_vacation_balance_records, [:company_id, :employee_identification] => :environment do |task, args|
  company = TaskUtil.company_selected(args.company_id)
  if company.mainurl
    puts 'Initializes: Vacation Balance Records'
    url       = TaskUtil.url_rest(company, 'CCNTVC', args.employee_identification)
    vacations_balances_ids = []
    while url.present? && url != 'NULL'
      datos     = TaskUtil.data_rest(url, 'cont_vcs', company )
      vacations = datos[:data]
      url = datos[:url]
      vacations.each do |vacation|
        begin
          employee = Employee.find_by_pernr_and_company_id vacation['pernr'].to_i, args.company_id
          if employee.present?
            s_vacation = VacationBalanceRecord.find_by_employee_id_and_company_id_and_begda_and_endda_and_subty vacation['pernr'], company.id, vacation['begda'], vacation['endda'], vacation['subty']
            vacation['begda']       = Date.parse vacation['begda']
            vacation['endda']       = Date.parse vacation['endda']
            vacation['employee_id'] = vacation['pernr']
            vacation['company_id'] = company.id
            vacation.delete('pernr')
            vacation.delete('pendientes')
            if s_vacation.present?
              puts 'update: '+s_vacation.id.to_s
              s_vacation.update vacation
            else
              puts 'new VacationBalanceRecord'
              s_vacation = VacationBalanceRecord.new vacation
            end
          else
            puts "Employee pernr: #{vacation['pernr']}, Not Exists"
          end
          s_vacation.save
          vacations_balances_ids.push(s_vacation.id)
        rescue => e
          puts 'Error save'
          puts e.inspect
        end
      end
    end
    TaskUtil.delete_registers_not_found(VacationBalanceRecord,vacations_balances_ids,company.id,args.employee_identification)
  end
end

task :update_compensatory_vacations_records, [:company_id, :employee_identification] => :environment do |task, args|
  company = TaskUtil.company_selected(args.company_id)
  if company.mainurl
    puts 'Initializes: Compensatory Vacation Record'
    url       = TaskUtil.url_rest(company, 'CVCCMP', args.employee_identification)
    vacations_compensatory_ids = []
    while url.present? && url != 'NULL'
      datos     = TaskUtil.data_rest(url, 'vac_comp', company )
      url = datos[:url]
      vacations = datos[:data]
      vacations.each do |vacation|
        begin
          employee = Employee.find_by_pernr_and_company_id vacation['pernr'].to_i, args.company_id
          if employee.present?
            s_vacation = CompensatoryVacationRecord.find_by_employee_id_and_begda_and_endda employee.id, vacation['begda'], vacation['endda']
            vacation['employee_id'] = employee.id
            vacation['company_id'] = company.id
            vacation.delete('pernr')
            vacation.delete('beguz')
            vacation.delete('enduz')
            vacation.delete('awart')
            vacation.delete('atext')
            vacation.delete('abwtg')
            vacation.delete('kaltg')
            if s_vacation.present?

              puts 'update: '+s_vacation.id.to_s
              s_vacation.update vacation
            else
              puts 'new CompensatoryVacationRecord'
              s_vacation = CompensatoryVacationRecord.new vacation
            end
          else
            puts "Employee pernr: #{vacation['pernr']}, Not Exists"
          end
          s_vacation.save
          vacations_compensatory_ids.push(s_vacation.id)
        rescue => e
          puts 'Error save'
          puts e.inspect
        end
      end
    end
    TaskUtil.delete_registers_not_found(CompensatoryVacationRecord,vacations_compensatory_ids,company.id)
  end
end

task :update_extra_hour_records, [:company_id, :employee_identification] => :environment do |task, args|
  company = TaskUtil.company_selected(args.company_id)
  if company.mainurl
    puts 'Initializes: Extra Hour Records'
    url           = TaskUtil.url_rest(company, 'CHOEXT', args.employee_identification)
    extra_hours_ids = []
    while url.present? && url != 'NULL'
      datos         = TaskUtil.data_rest(url, 'hors_ext', company )
      extra_hours   = datos[:data]
      url = datos[:url]
      extra_hours.each do |extra|
        begin
          employee = Employee.find_by_pernr_and_company_id extra['pernr'].to_i, args.company_id
          if employee.present?
            s_extra = ExtraHourRecord.find_by_employee_id_and_fecha_and_lgart_and_anzhl employee.id, extra['fecha'], extra['lgart'], extra['anzhl']
            extra['employee_id'] = employee.id
            extra['company_id'] = company.id
            extra.delete('pernr')
            extra.delete('mandt')
            if s_extra.present?
              puts 'update: '+s_extra.id.to_s
              s_extra.update extra
            else
              puts 'new ExtraHourRecord'
              s_extra = ExtraHourRecord.new extra
            end
          else
            puts "Employee pernr: #{extra['pernr']}, Not Exists"
          end
          s_extra.save
          extra_hours_ids.push(s_extra.id)
        rescue => e
          puts 'Error save'
          puts e.inspect
        end
      end
    end
    TaskUtil.delete_registers_not_found(ExtraHourRecord,extra_hours_ids,company.id)
  end
end

task :update_vacation_records, [:company_id, :employee_identification] => :environment do |task, args|
  company = TaskUtil.company_selected(args.company_id)
  if company.mainurl
    puts 'Initializes: Loan Records'
    url       = TaskUtil.url_rest(company, 'CVACAS', args.employee_identification)
    vacations_records_ids = []
    while url.present? && url != 'NULL'
      datos     = TaskUtil.data_rest(url, 'vacat', company )
      vacations = datos[:data]
      url = datos[:url]
      vacations.each do |vacation|
        begin
          employee = Employee.find_by_pernr_and_company_id vacation['pernr'].to_i, args.company_id
          if employee.present?
            s_vacation = VacationRecord.find_by_employee_id_and_begda_and_endda employee.id, vacation['begda'], vacation['endda']
            vacation['begda']       = Date.parse vacation['begda']
            vacation['endda']       = Date.parse vacation['endda']
            vacation['employee_id'] = employee.id
            vacation['company_id'] = company.id
            vacation.delete('beguz')
            vacation.delete('enduz')
            vacation.delete('pernr')
            if s_vacation.present?
              puts 'update: '+s_vacation.id.to_s
              s_vacation.update vacation
            else
              puts 'new VacationRecord'
              s_vacation = VacationRecord.new vacation
            end
          else
            puts "Employee pernr: #{vacation['pernr']}, Not Exists"
          end
          s_vacation.save
          vacations_records_ids.push(s_vacation.id)
        rescue => e
          puts 'Error save'
          puts e.inspect
        end
      end
    end

    TaskUtil.delete_registers_not_found(VacationRecord,vacations_records_ids,company.id)
  end
end

task :update_incapacity, [:company_id, :identification] => :environment do |task,args|
  company = TaskUtil.company_selected(args.company_id)
  if company.mainurl
    puts 'Initializes: incapacities'
    url       = TaskUtil.url_rest(company, 'CINCAP', args.identification)
    incapacities_service = []
    while url.present? && url != 'NULL'
      datos     = TaskUtil.data_rest(url, 'incapa', company)
      url = datos[:url]
      incapacities = datos[:data]
      incapacities.each do |incapacity|
        begin
          employee = Employee.find_by_pernr_and_company_id(incapacity['pernr'].to_i,company.id )
          if employee.present?
            dataIncapacity = {:begda => incapacity['begda'].to_date,:endda => incapacity['endda'].to_date, :id_incapacity => incapacity['awart'].to_i, :text_incapacity => incapacity['atext'], :days_working_incapacity => incapacity['abwtg'].to_i, :days_taken_incapacity => incapacity['kaltg'].to_i, :employee_id => employee.id, :company_id => company.id}
            incapacity_serv = Incapacity.where({:begda => incapacity['begda'].to_date,:endda => incapacity['endda'].to_date, :id_incapacity => incapacity['awart'].to_i,:employee_id => employee.id}).first_or_create
            Incapacity.where({:begda => incapacity['begda'].to_date,:endda => incapacity['endda'].to_date, :id_incapacity => incapacity['awart'].to_i,:employee_id => employee.id}).update_all(dataIncapacity)
            incapacities_service.push(incapacity_serv.id)
          else
            puts "Employee # #{incapacity['pernr']} Not exists to database"
          end
        rescue => e
          ActiveRecord::Rollback
          puts 'ERROR INESPERADO'
          puts 'Company: ' + company.name
          puts e.inspect
        end
      end
    end
    TaskUtil.delete_registers_not_found(Incapacity,incapacities_service,company.id)
    puts "FIN PROCESO"
  end
end

task :update_permissions, [:company_id, :identification] => :environment do |task,args|
  company = TaskUtil.company_selected(args.company_id)
  if company.mainurl
    puts 'Initializes: Permissions'
    url       = TaskUtil.url_rest(company, 'CPERMS', args.identification)
    permissions_service = []
    while url.present? && url != 'NULL'
      datos     = TaskUtil.data_rest(url, 'permisos', company)
      permissions = datos[:data]
      url = datos[:url]
      permissions.each do |permission|
        begin
          employee = Employee.find_by_pernr_and_company_id(permission['pernr'].to_i,company.id )
          if employee.present?
            dataPermission = {:begda => permission['begda'].to_date,:endda => permission['endda'].to_date, :id_permission => permission['awart'].to_i, :text_permission => permission['atext'], :days_working => permission['abwtg'].to_i, :days_taken => permission['kaltg'].to_i, :employee_id => employee.id, :company_id => company.id}
            permission_serv = Permission.where({:begda => permission['begda'].to_date,:endda => permission['endda'].to_date, :id_permission => permission['awart'].to_i,:employee_id => employee.id}).first_or_create
            Permission.where({:begda => permission['begda'].to_date,:endda => permission['endda'].to_date, :id_permission => permission['awart'].to_i,:employee_id => employee.id}).update_all(dataPermission)
            permissions_service.push(permission_serv.id)
          else
            puts "Employee # #{permission['pernr']} Not exists to database"
          end
        rescue => e
          ActiveRecord::Rollback
          puts 'ERROR INESPERADO'
          puts 'Company: ' + company.name
          puts e.inspect
        end
      end
    end
    TaskUtil.delete_registers_not_found(Permission,permissions_service,company.id)
    puts "FIN PROCESO"
  end
end

task :update_loan_records, [:company_id, :employee_identification] => :environment do |task, args|
  company = TaskUtil.company_selected(args.company_id)
  if company.mainurl
    puts 'Initializes: Loan Records'
    url       = TaskUtil.url_rest(company, 'CPREST', args.employee_identification)
    loan_records_ids = []
    while url.present? && url != 'NULL'
      datos     = TaskUtil.data_rest(url, 'prestamos', company )
      url = datos[:url]
      loans     = datos[:data]
      loans.each do |loan|
        begin
          employee = Employee.find_by_pernr_and_company_id loan['pernr'].to_i, args.company_id
          if employee.present?
            s_loan   = LoanRecord.find_by_employee_id_and_dlart_and_objps_and_fpper employee.id, loan['dlart'], loan['objps'].to_i, loan['fpper']
            fpbeg = Date.parse loan['fpbeg']
            fpend = Date.parse loan['fpend']
            if s_loan.present?

              puts 'update: '+s_loan.id.to_s
              s_loan.update("dlart" => loan['dlart'], "stext" => loan['stext'],
                            "objps" => loan['objps'].to_i, "fpper" => loan['fpper'],
                            "fpbeg" => fpbeg, "fpend" => fpend, "zsoll" => loan['zsoll'].to_i,
                            "nrdys" => loan['nrdys'].to_i, "lolci" => loan['lolci'].to_d,
                            "lolrp" => loan['lolrp'].to_d, "lolim" => loan['lolim'].to_d,
                            "totcu" => loan['totcu'].to_d, "lollb" => loan['lollb'].to_d,
                            "totsa" => loan['totsa'].to_d, "anrte" => loan['anrte'].to_d,
                            "darbt" => loan['darbt'].to_d,"company_id" => company.id)
            else
              puts 'new LoanRecord'
              s_loan = LoanRecord.new("dlart" => loan['dlart'], "stext" => loan['stext'],
                                      "objps" => loan['objps'].to_i, "fpper" => loan['fpper'],
                                      "fpbeg" => fpbeg, "fpend" => fpend, "zsoll" => loan['zsoll'].to_i,
                                      "nrdys" => loan['nrdys'].to_i, "lolci" => loan['lolci'].to_d,
                                      "lolrp" => loan['lolrp'].to_d, "lolim" => loan['lolim'].to_d,
                                      "totcu" => loan['totcu'].to_d, "lollb" => loan['lollb'].to_d,
                                      "totsa" => loan['totsa'].to_d, "anrte" => loan['anrte'].to_d,
                                      "darbt" => loan['darbt'].to_d, "employee_id" => employee.id,
                                      "company_id" => company.id)
            end
            s_loan.save
            loan_records_ids.push(s_loan.id)
          else
            puts "Employee pernr: #{loan['pernr']}, Not Exists"
          end
        rescue => e
          puts 'Error save'
          puts e.inspect
        end
      end
    end
    #elimina lo que no encuentre en base de datos
    TaskUtil.delete_registers_not_found(LoanRecord,loan_records_ids,company.id)
  end
end

task :update_embargoes, [:company_id, :employee_identification] => :environment do |task, args|
  company = TaskUtil.company_selected(args.company_id)
  if company.mainurl
    puts 'Initializes: Embargoes of Employees'
    url       = TaskUtil.url_rest(company, 'CEMBRG', args.employee_identification)
    embargoes_ids = []
    while url.present? && url != 'NULL'
      datos     = TaskUtil.data_rest(url, 'embargos', company )
      url = datos[:url]
      embargoes = datos[:data]
      embargoes.each do |embargo|
        #payroll_period = Date.parse(embargo['fpper'])
        payroll_period = embargo['fpper']
        begin
          employee  = Employee.find_by_pernr_and_company_id embargo['pernr'].to_i, args.company_id
          #embargo.delete('pernr')
          s_embargo = Embargo.find_by_employee_id_and_fpper_and_gcase_and_seqnr employee.id, payroll_period, embargo['gcase'], embargo['seqnr']
          #s_embargo = Embargo.find_by_employee_id_and_gcase employee.id, embargo['gcase']
          if s_embargo.present?
            date = Time.zone.now.beginning_of_day - 3.months
            puts 'update: '+s_embargo.id.to_s
            s_embargo.update("fpper"=>payroll_period, "gcase"=>embargo["gcase"], "edate"=>Date.parse(embargo['edate']),
                             "desga"=>embargo["desga"], "gproc"=>embargo["gproc"].to_i, "value"=>embargo["value"].to_i,
                             "gardv"=>embargo["gardv"].to_i, "balan"=>embargo["balan"].to_i, "perde"=>embargo["perde"].to_i,
                             "gorna"=>embargo["gorna"], "pnach"=>embargo["pnach"], "cedor"=>embargo["cedor"],
                             "acnum"=>embargo["acnum"],"company_id" => company.id,"orcod" => embargo["orcod"],
                             "desor" => embargo["desor"],"seqnr" => embargo["seqnr"])
          else
            puts 'new Embargo'
            s_embargo = Embargo.new("fpper"=>payroll_period, "gcase"=>embargo["gcase"], "edate"=>Date.parse(embargo['edate']),
                                    "desga"=>embargo["desga"], "gproc"=>embargo["gproc"].to_i, "value"=>embargo["value"].to_i,
                                    "gardv"=>embargo["gardv"].to_i, "balan"=>embargo["balan"].to_i, "perde"=>embargo["perde"].to_i,
                                    "gorna"=>embargo["gorna"], "pnach"=>embargo["pnach"], "cedor"=>embargo["cedor"],
                                    "acnum"=>embargo["acnum"], "employee_id"=>employee.id,"company_id" => company.id,"orcod" => embargo["orcod"],
                                    "desor" => embargo["desor"],"seqnr" => embargo["seqnr"])
          end
          s_embargo.save
          embargoes_ids.push(s_embargo.id)
        rescue => e
          puts 'Error save'
          puts e.inspect
        end
      end
    end
    #elimina lo que no encuentre en base de datos
    TaskUtil.delete_registers_not_found(Embargo,embargoes_ids,company.id)
  end
end

task :update_indebtedness_levels, [:company_id] => :environment do |task, args|
  company = TaskUtil.company_selected(args.company_id)
  if company.mainurl
    puts 'Initializes: Nivel Endeudamiento'
    url       = TaskUtil.url_rest(company, 'CNIVEN')
    indebtednees_ids = []
    while url.present? && url != 'NULL'
      datos  = TaskUtil.data_rest(url, 'niv_end', company )
      levels = datos[:data]
      length = levels.length
      url = datos[:url]
      levels.each_with_index do |level, key|
        p_date        = Date.parse(level['fecha'])
        employe_id    = level['pernr'].to_i
        pconcept_code = level['lgart']
        pconcept_txt  = level['lgtxt']
        deven         = level['deven'].to_f
        deduc         = level['deduc'].to_f


        indebtedness_level = IndebtednessLevel.find_by_employee_identification_and_company_id_and_payroll_concept_code_and_payroll_date(employe_id, args.company_id.to_i, pconcept_code, p_date)
        begin
          if indebtedness_level.present?
            date = Time.zone.now.beginning_of_day - 3.months
            puts indebtedness_level.updated_at.to_s+" <= "+date.to_s
            if(indebtedness_level.payroll_date <= date)
              puts 'destroy: '+indebtedness_level.updated_at.to_s
              indebtedness_level.destroy
            else
              puts 'update employee: '+employe_id.to_s
              indebtedness_level.payroll_date            = p_date
              indebtedness_level.payroll_concept_code    = pconcept_code
              indebtedness_level.payroll_concept_txt     = pconcept_txt
              indebtedness_level.deven                   = deven
              indebtedness_level.deduc                   = deduc
            end
          else
            employee = Employee.find_by_pernr_and_company_id(employe_id, args.company_id.to_i)
            if employee.present?
              puts 'new employee : '+employe_id.to_s
              indebtedness_level = IndebtednessLevel.new(:payroll_date => p_date, :employee_id => employee.id, :employee_identification => employe_id, :company_id => args.company_id, :payroll_concept_code => pconcept_code, :payroll_concept_txt => pconcept_txt, :deven => deven, :deduc => deduc)
            else
              puts 'not employee : '+employe_id.to_s
            end
          end
          indebtedness_level.save
          indebtednees_ids.push(indebtedness_level.id)
        rescue => e
          puts 'Error save'
          puts e.inspect
        end
        puts "Indebtedness Levels #{key}/#{length}" if key % 10 == 0
      end
    end
    #elimina lo que no encuentre en base de datos
    TaskUtil.delete_registers_not_found(IndebtednessLevel,indebtednees_ids,company.id)
  end
end