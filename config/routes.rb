Rails.application.routes.draw do

  devise_for :users, skip: [:registrations ]

  namespace :api do
    namespace :v2 do
      mount_devise_token_auth_for 'User', at: 'auth', controllers: {
          passwords:          'api/v2/passwords', #'devise_token_auth/passwords',
          omniauth_callbacks: 'devise_token_auth/omniauth_callbacks',
          sessions:           'api/v2/sessions',
          token_validations:  'api/v2/token_validations'
      }
      resources :companies, only: [] do
        collection do
          get 'info', action: :info_company
        end
      end

      resources :employees do
        collection do
          get 'all_by_fecha_de_ingreso'
          get 'all_by_cumpleano_mes'
          get 'all_by_current_month_entry'
          get 'income_and_deductions'
          get 'vacations_enjoy'
          get 'severance_last_months'
          get 'interests_severance_last_months'
          get 'autoservice_files'
          get 'profile'
          get 'basic_info_employees'
          get 'subordinates'
        end
      end

      resources :organization_chart, only:[:index] do
        collection do
          get 'search'
        end
      end

      #consultations
      resources :embargo do
        collection do
          get 'by_employee/:employee_id', action: :by_employee
        end
      end
      resources :incapacities do
        collection do
          get 'by_employee/:employee_id', action: :by_employee
        end
      end
      resources :payments_deductions do
        collection do
          get 'by_employee/:employee_id', action: :by_employee
        end
      end
      resources :indebtedness_level do
        collection do
          get 'by_employee/:employee_id', action: :by_employee
        end
      end
      resources :permissions do
        collection do
          get 'by_employee/:employee_id', action: :by_employee
        end
      end
      resources :loan_records do
        collection do
          get 'by_employee/:employee_id', action: :by_employee
        end
      end
      resources :vacation_balance_records do
        collection do
          get 'by_employee/:employee_id', action: :by_employee
        end
      end
      resources :compensatory_vacation_records do
        collection do
          get 'by_employee/:employee_id', action: :by_employee
        end
      end
      resources :vacation_record do
        collection do
          get 'by_employee/:employee_id', action: :by_employee
        end
      end

      #Manager Report
      resources :absences, only: [:show]
      resources :variance_analysis, only: [:show]
      resources :historical_positions, only: [:show]
      resources :historical_salaries, only: [:show]
      resources :staff_turnover , only: [:show]

      resources :articles do
        collection do
          get 'categories',action: :categories_articles
        end
        member do
          put 'viewed'
        end
      end

      resources :comments_articles, only:[:index,:create]

      resources :favorite_employees, only: [:index,:create,:destroy] do
        collection do
          get 'nofavorites/:record/:numrecords',action: :employees_remaining
          get 'favorites/:record/:numrecords',action: :favorites
          post 'search', action: :search
        end
      end

      #Solicitudes y registros

      get 'types_request/:type', to: 'types_request#index'

      resources :requests_employees, only:[:index,:create,:show,:destroy] do
        collection do
          get 'request_states'
          get 'my_requests'
          get 'requests_managed/:record/:numrecords',action: :requests_managed
        end

      end

      resources :approvals, only:[:index,:show,:update]

    end
  end

  root to: "pages#welcome.html"

end

