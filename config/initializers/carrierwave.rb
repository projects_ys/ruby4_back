if Rails.env.production? || Rails.env.staging? || Rails.env.devcloud?
  CarrierWave.configure do |config|
    config.cache_dir = "#{Rails.root}/tmp/"
    config.storage = :fog
    config.fog_credentials = {
        :provider               => 'AWS',
        :aws_access_key_id      => ENV['AWS_ACCESS_KEY_ID'],
        :aws_secret_access_key  => ENV['AWS_SECRET_ACCESS_KEY'],
        :region                 => 'us-east-1'
    }
    config.fog_directory  = ENV['AWS_BUCKET_NAME']
    config.asset_host     = ENV['AWS_BUCKET_URL']
    config.fog_public     = false
    config.fog_authenticated_url_expiration = 900 #Seconds
    config.fog_attributes = { 'Cache-Control' => 'max-age=315576000'}
  end
elsif Rails.env.development?
  CarrierWave.configure do |config|
    config.storage = :file
    config.asset_host = ActionController::Base.asset_host
  end
end