if Rails.env.production? || Rails.env.staging?
  Rails.logger = Le.new(ENV['LOGENTRIES_TOKEN'])
end