# The following commands run under the vagrant user.
# We need to do this since we execute rails under that
# user

cd /vagrant

# Make sure we're using rvm, with the proper config
source /usr/local/rvm/scripts/rvm

# We're changing ruby versions
rvm install 2.1.2
rvm --default use 2.1.2

# Install all the dependencies for the rails project
bundle install

# Create the database
rake db:create

# Add some data to the database
rake db:migrate

#run basic config tasks
# rake update_employees
# rake update_labors
# rake update_vacations
# rake update_employees_balances
# rake update_info_type
# rake update_employee_info
# rake update_boss_employees