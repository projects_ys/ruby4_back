class InhabilityRequirementsController < AuthorizeController

  def index
    inhabilities = current_user.employee.inhability_requirements.where(status: InhabilityRequirement::STATUS_ID[:pendiente]).order(:created_at)
    render json: inhabilities.as_json
  end

  def approves
    inhabilities = current_user.company.inhability_requirements.where(status: InhabilityRequirement::STATUS_ID[:pendiente], approver_id_posicion: current_user.employee.id_posicion ).order(:created_at)
    render json: inhabilities.as_json(
        include: { employee: { only: [:short_name] }}
    )
  end

  def create
    inhability = current_user.employee.inhability_requirements.new(
        status: params[:status],
        motivo: params[:motivo],
        start_date: params[:start_date],
        end_date: params[:end_date],
        attachment: params[:file],
        approver_id_posicion: current_user.employee.inca_approver
    )
    inhability.status = InhabilityRequirement::STATUS_ID[:pendiente]
    inhability.company_id = current_user.employee.company_id
    inhability.save
    respond_with(inhability)
  end

  def destroy
    inhability = current_user.employee.inhability_requirements.find_by_status_and_id(InhabilityRequirement::STATUS_ID[:pendiente].to_s, params[:id].to_i )
    inhability.destroy
    respond_with(inhability)
  end

  def approve
    inhability = current_user.company.inhability_requirements.find_by_status_and_id_and_approver_id_posicion(InhabilityRequirement::STATUS_ID[:pendiente].to_s, params[:id].to_i, current_user.employee.id_posicion)
    inhability.status = InhabilityRequirement::STATUS_ID[:aprobado].to_s
    inhability.approved_by_identification = current_user.employee.pernr
    inhability.save
    respond_with(inhability)
  end

  def denied
    inhability = current_user.company.inhability_requirements.find_by_status_and_id_and_approver_id_posicion(InhabilityRequirement::STATUS_ID[:pendiente].to_s, params[:id].to_i, current_user.employee.id_posicion)
    inhability.status = InhabilityRequirement::STATUS_ID[:denegado].to_s
    inhability.approved_by_identification = current_user.employee.pernr
    inhability.save
    respond_with(inhability)
  end



end
