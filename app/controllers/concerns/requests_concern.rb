module RequestsConcern
  extend ActiveSupport::Concern

  def available_contingent(employee,type_request_user)
    case type_request_user.id_activity
      when 'VACA','VCCP'
        contingents = employee.vacation_balance_records.where(:subty => type_request_user.contingent)
        available = contingents.sum(:anzhl)
        used = contingents.sum(:kverb)
        contingent_available = available - used
        #solicitudes activas con contingentes
        requests_open = employee.requests_employees.where(:status_id => ['ReqEmpStaPenAproJef','ReqEmpStaPenRH'],:id_activity => type_request_user.id_activity).sum(:days_request)
        total_data_available = contingent_available.to_f - requests_open.to_f
      else
        total_data_available = nil
    end
    total_data_available
  end

=begin
  def info_detail request,type_user
    current_status = request.status
    current_applicant = request.employee
    current_type = request.types_requests
    if current_status.identifier == 'ReqEmpStaPenAproJef' || current_status.identifier == 'ReqEmpStaCanJef'
      current_approver = approver_asigned(current_type.id_activity,:first,current_applicant)
    elsif current_status.identifier == 'ReqEmpStaPenRH' || current_status.identifier == 'ReqEmpStaCanRH' || current_status.identifier == 'ReqEmpStaAprRH'
      current_approver =approver_asigned(current_type.id_activity,:second,current_applicant)
    else
      current_approver = nil
    end
    data_request ={
        id: request.id,
        date_begin: request.date_begin,
        date_end: request.date_end,
        created_at: request.created_at,
        company_id: request.company_id,
        observation_request:request.observation_request,
        comment_first_approver:request.comment_first_approver,
        date_first_approval:request.date_first_approval,
        comment_second_approver:request.comment_second_approver,
        date_second_approval:request.date_second_approval,
        file_support:request.file_support.url,
        days_request:request.days_request,
        type_request: {
            identifier: current_type.form,
            value: current_type.description
        },
        status: {
            identifier: current_status.identifier,
            value: current_status.value
        }
    }
    if type_user == 'applicant'
      data_request[:current_approver] = {
          short_name: current_approver.short_name,
          position: current_approver.posicion
      }
    elsif  type_user == 'approver'
      data_request[:current_applicant] = {
                short_name:current_applicant.short_name,
                position:current_applicant.posicion,
                image:current_applicant.image
              }

      if request.id_activity == 'VACA'
        data_request[:applicants_crossed] = current_approver.first_approver_requests_received
                                                .select(:id,:pernr,:date_begin,:date_end,:company_id,:status_id)
                                                .where(:types_requests_record_id => request.types_requests_record_id)
                                                .where.not(:id => request.id)
                                                .crossed(request.date_begin,request.date_end)
                                                .order(date_begin: :asc)
                                                .as_json(include:{employee:{only:[:id,:name]}})
      end

    end
    data_request
  end
=end

  def preview_request requests,type_user
    my_requests = []
    requests.each do |request|
      current_status = request.status
      current_type = request.types_requests
      current_applicant = request.employee
      if type_user == 'approver'
        my_requests << {
            id: request.id,
            date_request: request.created_at,
            applicant:{
                name: current_applicant.short_name,
                image: current_applicant.image
            },
            type_request: {
                identifier: current_type.form,
                value: current_type.description
            },
            status: {
                identifier: current_status.identifier,
                value: current_status.value
            }
        }
      elsif type_user == 'applicant'
        if current_status.identifier == 'ReqEmpStaPenAproJef' || current_status.identifier == 'ReqEmpStaCanJef'
          current_approver = approver_asigned(current_type.id_activity,:first,current_applicant)
        elsif current_status.identifier == 'ReqEmpStaPenRH' || current_status.identifier == 'ReqEmpStaCanRH' || current_status.identifier == 'ReqEmpStaAprRH'
          current_approver =approver_asigned(current_type.id_activity,:second,current_applicant)
        else
          current_approver = nil
        end

        my_requests << {
            id: request.id,
            date_request: request.created_at,
            type_request: {
                identifier: current_type.form,
                value: current_type.description
            },
            status: {
                identifier: current_status.identifier,
                value: current_status.value
            },
            current_approver: {
                short_name: current_approver.short_name,
                position: current_approver.posicion
            }
        }
      end
    end
    my_requests
  end

  def approver_asigned(type, number_approver,employee)
    if number_approver == :first
      case type
        when 'CESA'
          position_approver = employee.cesa_approver
        when 'DAMS'
          position_approver = employee.dams_approver
        when 'HOEX'
          position_approver = employee.hoex_approver
        when 'INCA'
          position_approver = employee.inca_approver
        when 'PERM'
          position_approver = employee.perm_approver
        when 'PRES'
          position_approver = employee.pres_approver
        when 'VACA'
          position_approver = employee.vaca_approver
        when 'VCCP'
          position_approver = employee.vccp_approver
      end
    elsif number_approver == :second
      case type
        when 'CESA'
          position_approver = employee.second_cesa_approver
        when 'DAMS'
          position_approver = employee.second_dams_approver
        when 'HOEX'
          position_approver = employee.second_hoex_approver
        when 'INCA'
          position_approver = employee.second_inca_approver
        when 'PERM'
          position_approver = employee.second_perm_approver
        when 'PRES'
          position_approver = employee.second_pres_approver
        when 'VACA'
          position_approver = employee.second_vaca_approver
        when 'VCCP'
          position_approver = employee.second_vccp_approver
      end
    else
      position_approver = nil
    end
    approver_is = Employee.find_by_id_posicion position_approver
    approver_is
  rescue => e
    puts e.inspect
  end

end