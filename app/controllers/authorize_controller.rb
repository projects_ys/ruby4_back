class AuthorizeController < ApplicationController
  #The problem using a custom class.
  #
  #CanCan tries to singularize the controller class name to
  #match the model class name and that
  #sometimes isn't what we expect:
  #:class => false
  #load_and_authorize_resource :class => false
  before_filter :authenticate_user!

  load_and_authorize_resource

  rescue_from CanCan::AccessDenied do |exception|
    result =  {
      :permission    => false,
      :alert         => exception.message,
      :action        => exception.action
    }
    unless Rails.env.development?
      Airbrake.notify(exception)
    end
    @reply = result
    @number_status = 401

  end

end
