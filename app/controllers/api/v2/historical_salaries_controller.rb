class Api::V2::HistoricalSalariesController < AuthorizeController
  skip_load_and_authorize_resource :only => [:show]

  def show
    authorize! :read, HistoricalPosition
    employee_id = params[:id].to_i == 0 ? current_user.employee.id : params[:id].to_i
    company_id  = current_user.company_id
    employee    = Employee.find_by_id_and_company_id(employee_id, company_id)

    labels = {
      :field_0 => DataTableHelper.info_field("Cargo", "string", true),
      :field_1 => DataTableHelper.info_field("Fecha Inicial", "date", true),
      :field_2 => DataTableHelper.info_field("Fecha Final", "date", false),
      :field_3 => DataTableHelper.info_field("Tiempo", "string", false),
      :field_4 => DataTableHelper.info_field("Salario", "money", false)
    }
    historical_salaries = historical_salaries_employee(employee)
    @reply = {
      "labels" => labels,
      "list"   => historical_salaries
    }.as_json
    @number_status = 200
  rescue => e
    unless rescue_with_handler(e)
      if Rails.env.development?
        @reply = {status: e.message, details: e.backtrace.inspect }
        @number_status = 202
      else
        Airbrake.notify(e)
        @reply = {}
        @number_status = 500
      end
    end
  ensure
    render json: @reply,status: @number_status
  end

  protected
  def historical_salaries_employee(employee)
    historical_employee = employee.historical_positions.select(:position,:begda,:endda,:amount_accrued).order('endda asc')
    historical = []
    historical_employee.each do |h|
      if h.position_history != nil
        name_position = h.position_history.name_position
      else
        timelinepos = h.timeline_positions
        if timelinepos.length > 0
          name_position = timelinepos[0].name_position
        else
          name_position = 'DESCONOCIDO / INEXISTENTE'
        end
      end

      historical << {
        "field_0" => name_position,
        "field_1" => DataTableHelper.format_date(h['begda']),
        "field_2" => DataTableHelper.format_date(h['endda']),
        "field_3" => DataTableHelper.diff_dates(h['begda'], h['endda']),
        "field_4" => h['amount_accrued'].to_f,
      }
    end

    return historical
  end
end
