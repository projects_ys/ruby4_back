class Api::V2::HistoricalPositionsController < AuthorizeController
  skip_load_and_authorize_resource :only => [:show]
  def show
    authorize! :read, HistoricalPosition
    employee_id = params[:id].to_i == 0 ? current_user.employee.id : params[:id].to_i
    company_id  = current_user.company_id
    employee    = Employee.find_by_id_and_company_id(employee_id, company_id)

    labels = {
      :field_0 => DataTableHelper.info_field("Cargo", "string", true),
      :field_1 => DataTableHelper.info_field("Fecha Inicial", "date", true),
      :field_2 => DataTableHelper.info_field("Fecha Final", "date", false),
      :field_3 => DataTableHelper.info_field("Tiempo", "string", false)
    }

    historical_positions = historical_employee(employee)
    @reply = {
      "labels" => labels,
      "list"   => historical_positions
    }.as_json
    @number_status = 200
  rescue => e
    unless rescue_with_handler(e)
      if Rails.env.development?
        @reply = {status: e.message, details: e.backtrace.inspect }
        @number_status = 202
      else
        Airbrake.notify(e)
        @reply = {}
        @number_status = 500
      end
    end
  ensure
    render json: @reply,status: @number_status
  end

  protected
  def historical_employee(employee)
    historical_employee = HistoricalPosition.select('position,MIN(begda) as begda,MAX(endda) as endda,MAX(amount_accrued) as amount_accrued,MAX(currency) as currency').where('employee_id = ?' ,employee.id).group('position')
    historical = []
    historical_employee.each do |h|
      timelines = h.timeline_positions
      if timelines.length > 0
        historical_change_name = HistoricalPosition.select('position,MIN(begda) AS begda,MAX(endda) AS endda,MAX(amount_accrued) AS amount_accrued,MAX(currency) AS currency').where('employee_id = ? AND position = ?',employee.id,h.position).group('position')
        historical_change_name.each do |hn|
          positions_in_time = TimelinePosition.where('position_id = ? AND endda > ? AND begda <= ?',hn['position'],hn['begda'],hn['endda'])
          positions_in_time.each do |pt|
            begda = hn['begda'] > pt['begda'] ?  hn['begda'] : pt['begda']
            endda = hn['endda'] >= pt['endda'] ?  pt['endda'] : hn['endda']
            historical << {
              "field_0" => pt['name_position'],
              "field_1" => DataTableHelper.format_date(begda),
              "field_2" => DataTableHelper.format_date(endda),
              "field_3" => DataTableHelper.diff_dates(begda, endda)
            }
          end
        end
      else
        begda = h['begda']
        endda = h['endda']
        historical << {
          "field_0" => h.position_history != nil ? h.position_history.name_position : 'DESCONOCIDO / INEXISTENTE',
          "field_1" => DataTableHelper.format_date(begda),
          "field_2" => DataTableHelper.format_date(endda),
          "field_3" => DataTableHelper.diff_dates(begda, endda)
        }
      end
    end
    return historical
  end
end
