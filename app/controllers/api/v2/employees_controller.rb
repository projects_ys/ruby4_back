class Api::V2::EmployeesController < AuthorizeController

  def subordinates
    authorize! :read, Position
    subordinates = current_user
                       .employee
                       .subordinates
                       .select(:id,:name,:lastname,:id_posicion,:posicion,:image)
                       .order(name: :asc,lastname: :asc)

    subordinates_vacancies = current_user
                                 .employee.my_position
                                 .subordinates_all
                                 .where.not(:id_position => subordinates.pluck(:id_posicion))
                                 .order(name_position: :asc)
    subordinates =  subordinates.as_json(methods:[:amount,:short_name,:type])
    if subordinates_vacancies.present?
      subordinates_vacancies.each do |vacancy|
        subordinates = subordinates.push(organize_vacancies(vacancy))
      end
    end
    @reply = subordinates
    @number_status = 200
  rescue => e
    unless rescue_with_handler(e)
      if Rails.env.development?
        @reply = {status: e.message, details: e.backtrace.inspect }
        @number_status = 202
      else
        Airbrake.notify(e)
        @reply = {}
        @number_status = 500
      end
    end
  ensure
    render json: @reply,status:@number_status
  end


  #Perfil, datos maestros y opciones de llenado
  def profile
    profile_data_basic = (Employee.select([:id,:posicion,:unidad_org,:area,:division_per,:subdivision_per,:name,:lastname]).find_by_id current_user.employee.id).as_json({:methods => :short_name})
    profile = {
        basic_data: profile_data_basic,
        data_master_info: current_user.company.master_datum,
        employee_info: current_user.employee_info
    }
    @reply = profile.as_json
  rescue => e
    rescue_with_handler(e)
  ensure
    render json: @reply,status:@number_status
  end

  #Dias de vacaciones dashboard
  def vacations_enjoy
    begin
      vac_availa = current_user.employee.vacation
      vacation_resume = {
          :value => vac_availa.resumen[2].to_i.to_s
      }
      @reply = vacation_resume.as_json
      @number_status = 200
    rescue => e
      unless rescue_with_handler(e)
        if Rails.env.development?
          @reply = {status: e.message, details: e.backtrace.inspect }
          @number_status = 202
        else
          Airbrake.notify(e)
          @reply = {}
          @number_status = 500
        end
      end
    ensure
      render json: @reply, status: @number_status
    end
  end
  #Mis cesantias dashboard
  def severance_last_months
    begin
      in_sev = current_user.employee.saldo
      saldos_decrypt = {
          :data_graph => eval(in_sev.t_cesantias.to_s),
          :value => in_sev.saldo,
      }
      @number_status =  200
      @reply = saldos_decrypt.as_json
    rescue => e
      unless rescue_with_handler(e)
        if Rails.env.development?
          @reply = {status: e.message, details: e.backtrace.inspect }
          @number_status = 500
        else
          Airbrake.notify(e)
          @reply = {}
          @number_status = 500
        end
      end
    ensure
      render json: @reply, status: @number_status
    end
  end
  #Mis interes de cesantias dashboard
  def interests_severance_last_months
    begin
      in_sev = current_user.employee.saldo
      interests_decrypt = {
          :data_graph => eval(in_sev.t_intcesantias.to_s),
          :value => in_sev.intsaldo,
      }
      @reply = interests_decrypt.as_json
      @number_status = 200
    rescue => e
      unless rescue_with_handler(e)
        if Rails.env.development?
          @reply = {status: e.message, details: e.backtrace.inspect }
          @number_status = 202
        else
          Airbrake.notify(e)
          @reply = {}
          @number_status = 500
        end
      end
    ensure
      render json: @reply, status: @number_status
    end

  end

  #Grafica de ingresos y deducciones
  def income_and_deductions
    begin
      in_ded = current_user.employee.saldo
      saldos_decrypt = {
          :data_graph => eval(in_ded.t_endeudamiento.to_s),
          :total_one => in_ded.totdevengos,
          :total_two => in_ded.totdeducciones,
      }
      @reply = saldos_decrypt.as_json
      @number_status = 200
    rescue => e
      unless rescue_with_handler(e)
        if Rails.env.development?
          @reply = {status: e.message, details: e.backtrace.inspect }
          @number_status = 202
        else
          Airbrake.notify(e)
          @reply = {}
          @number_status = 500
        end
      end
    ensure
      render json: @reply, status: @number_status
    end

  end
  #Ingresos de los ultimos 3 meses
  def all_by_current_month_entry
    t = Time.zone.now
    t2 = t - 3.months

    employees = current_user.company.employees
                    .select('id,name,lastname,image,posicion,fecha_ingreso as date_show')
                    .where('fecha_ingreso <= ?',  t )
                    .where('fecha_ingreso >= ?',  t2 )
                    .order('fecha_ingreso DESC')
    @reply = employees.as_json({:methods => :short_name})
    @number_status = 200

  rescue => e
    rescue_with_handler(e)
  ensure
    render json: @reply,status:@number_status
  end
  # Aniversario de empleados segun su fecha de ingreso un mes por delante
  def all_by_fecha_de_ingreso
    tinitial = Time.zone.now
    tfinal = tinitial+1.months
    employees = current_user.company.employees
                    .select('id,name,lastname,image,posicion,fecha_ingreso as date_show')
                    .where.not(fecha_ingreso:nil)
                    .where( "TO_DATE(concat_ws('-',EXTRACT(YEAR from now()::date+'1 month'::interval),TO_CHAR(fecha_ingreso,'MM-DD')),'YYYY-MM-DD') >= ?", tinitial )
                    .where( "TO_DATE(concat_ws('-',EXTRACT(YEAR from now()::date+'1 month'::interval),TO_CHAR(fecha_ingreso,'MM-DD')),'YYYY-MM-DD') <= ?", tfinal )
                    .order( "TO_DATE(concat_ws('-',EXTRACT(YEAR from NOW()),TO_CHAR(fecha_ingreso,'MM-DD')),'YYYY-MM-DD') ASC" )
    @reply = employees.as_json({:methods => :short_name})
    @number_status = 200
  rescue => e
    rescue_with_handler(e)
  ensure
    render json: @reply,status:@number_status
  end
  # Cumpleaños de empleados del proximo mes al dia de consulta
  def all_by_cumpleano_mes
    tinitial = Date.today
    tfinal = tinitial+1.months

    employees = current_user.company.employees
                    .select('id,name,lastname,image,posicion,fecha_nac as date_show')
                    .where.not(fecha_nac:nil)
                    .where( "TO_DATE(concat_ws('-',EXTRACT(YEAR from now()::date+'1 month'::interval),TO_CHAR(fecha_nac,'MM-DD')),'YYYY-MM-DD') >= ?", tinitial )
                    .where( "TO_DATE(concat_ws('-',EXTRACT(YEAR from now()::date+'1 month'::interval),TO_CHAR(fecha_nac,'MM-DD')),'YYYY-MM-DD') <= ?", tfinal )
                    .order( "TO_DATE(concat_ws('-',EXTRACT(YEAR from NOW()),TO_CHAR(fecha_nac,'MM-DD')),'YYYY-MM-DD') ASC" )
    @reply =  employees.as_json({:methods => :short_name})
    @number_status = 200
  rescue => e
    unless rescue_with_handler(e)
      if Rails.env.development?
        @reply = {status: e.message, details: e.backtrace.inspect }
        @number_status = 202
      else
        Airbrake.notify(e)
        @reply = {}
        @number_status = 500
      end
    end
  ensure
    render json: @reply,status:@number_status
  end

  def autoservice_files
    begin
      pdfs = {}
      pdf_types   = current_user.company.pdf_types
      employee_id = current_user.employee.id

      pdf_types.each do |type|
        files = FileEmployee.select("id, name_iden, file, op, pdf_type_ident, company_id")
                    .where('employee_id = ?', employee_id)
                    .where('op = ?', type.activ)
                    .order('name_iden DESC')

        pdfs[type.activ] = {
            'name' => type.nomid,
            'data' => self.create_files(files)
        }
      end
      @reply = pdfs.to_json
      @number_status = 200
    rescue => e
      unless rescue_with_handler(e)
        if Rails.env.development?
          @reply = {status: e.message, details: e.backtrace.inspect }
          @number_status = 202
        else
          Airbrake.notify(e)
          @reply = {}
          @number_status = 500
        end
      end
    ensure
      render json: @reply, status: @number_status
    end

  end

  protected
  def create_files files
    data = []
    files.each do |file|
      name = file.name_iden == nil ? file.pdf_type.nomid : file.name_iden
      data << {
          :id        => file.id,
          :name_iden => name,
          :file      => {
              :url => file.file.url
          }
      }
    end
    data
  end

  def create_basic_data data, subs
    hp = HistoricalPosition
             .select('position,MIN(begda) as begda,MAX(endda) as endda,MAX(amount_accrued) as amount_accrued,MAX(currency) as currency')
             .where('employee_id = ?' ,data.id)
             .where("endda = ?", "9999-12-31")
             .group('position')

    data = {
        :id => data.id,
        :first_name => data.name.capitalize,
        :last_name => data.lastname.capitalize,
        :name => data.short_name.split.map(&:capitalize).join(' '),
        :position => data.posicion.split.map(&:capitalize).join(' '),
        :area => data.area,
        :amount => hp[0].amount_accrued.to_f,
        :image => {
            :url => data.image.url
        }
    }
    if subs.present? && subs != nil
      data[:subordinates] = subs
    end
    data
  end

  def organize_vacancies(vacancy)
    {
        id: nil,
        name: nil,
        lastname: nil,
        image:nil,
        posicion:vacancy.name_position,
        id_posicion:vacancy.id_position,
        amount:0,
        short_name:t('.vacancy_msg'),
        type:t('.name_vacancy')
    }
  end
end

