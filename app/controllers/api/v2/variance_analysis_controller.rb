class Api::V2::VarianceAnalysisController < AuthorizeController
  skip_load_and_authorize_resource :only => [:show]
  def show
    authorize! :show, VarianceAnalysis
    employee_id = params[:id].to_i == 0 ? current_user.employee.id : params[:id].to_i
    company_id  = current_user.company_id
    employee    = Employee.find_by_id_and_company_id(employee_id, company_id)
    labels = {
      :field_0 => DataTableHelper.info_field("Tipo", "string", true),
      :field_1 => DataTableHelper.info_field("Nómina", "string", true),
      :field_2 => DataTableHelper.info_field("Importe Anterior", "money", true),
      :field_3 => DataTableHelper.info_field("Importe Actual", "money", true),
      :field_4 => DataTableHelper.info_field("Diferencia", "money", false),
      :field_5 => DataTableHelper.info_field("Porcentaje", "percentage", false)
    }
    variances = variance_analysis_employee(employee)
    @reply = {
      "labels" => labels,
      "list"   => variances
    }.as_json
    @number_status = 200
  rescue => e
    unless rescue_with_handler(e)
      if Rails.env.development?
        @reply = {status: e.message, details: e.backtrace.inspect }
        @number_status = 202
      else
        Airbrake.notify(e)
        @reply = {}
        @number_status = 500
      end
    end
  ensure
    render json: @reply,status: @number_status
  end

  protected
  def variance_analysis_employee(employee)
    if employee.variance_analysis.present?
      variances_employee = []
      employee.variance_analysis.each do |e|
        if e.type_accumulation == "V"
          typeAcumulation = "Devengo"
        else if e.type_accumulation == "D"
               typeAcumulation = "Deducción"
             end
        end
        variances_employee << {
            "field_0" => typeAcumulation,
            "field_1" => e.description_payroll,
            "field_2" => e.previous_amount.to_f,
            "field_3" => e.current_amount.to_f,
            "field_4" => e.difference_amount.to_f,
            "field_5" => e.percentage_difference
        }
      end
      return variances_employee
    else
      return nil
    end
  end
end
