class Api::V2::CommentsArticlesController < AuthorizeController
  skip_load_and_authorize_resource :only => [:index,:create,:destroy]

  def index
    @comments = CommentsArticle.all.where(:article_id=>params[:articleId]).order('created_at DESC').as_json(only:[],:methods=> [:data_complete])
    @reply = {:comments => @comments}
    @number_status = 200
  rescue => e
    rescue_with_handler(e)
  ensure
    render json: @reply,status:@number_status
  end

  def create
    @comment = CommentsArticle.new(comment_articles)
    @comment.employee_id = current_user.employee_id
    @comment.company_id = current_user.company_id
    if @comment.save
      @reply = {status: :created,comment_info:@comment}
      @number_status = 200
    else
      @reply = @comment.errors.full_messages
      @number_status = 400
    end
  rescue => e
    rescue_with_handler(e)
  ensure
    render json: @reply,status:@number_status
  end

  def destroy
    authorize! @comment
    if @comment.destroy
        @reply = {status: :deleted }
        @number_status = 200
    else
      @reply = @comment.errors.full_messages
      @number_status = 400
    end
  rescue => e
    rescue_with_handler(e)
  ensure
    render json: @reply,status:@number_status
  end

  private
  def set_comment
    @comment = CommentsArticle.find_by_id(params[:id])
  end

  def comment_articles
    params.permit(:comment_text,:article_id)
  end

end
