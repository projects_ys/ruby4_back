class Api::V2::StaffTurnoverController < AuthorizeController
  skip_load_and_authorize_resource :only => [:index,:show]

  def index
    authorize! :index, EmployeeTurnover
    rotations_personal = []
    employee = Employee.includes(:subordinates).find(current_user.employee_id)
    tempNew = rotation_employee(employee)
    if tempNew != nil
      rotations_personal << tempNew
    end
    employee.subordinates.each do |employeeSub|
      tempNew = rotation_employee(employeeSub)
      if tempNew != nil
        rotations_personal << tempNew
      end
      if employeeSub.subordinates.present?
        employeeSub.subordinates.each do |subordinate|
          tempNew = rotation_employee(subordinate)
          if tempNew != nil
            rotations_personal << tempNew
          end
        end
      end
     end

    @reply = {
            :staff_turnover => rotations_personal.as_json
        }
    @number_status = 200
  rescue => e
    unless rescue_with_handler(e)
      if Rails.env.development?
        @reply = {error: e.inspect, details:e.backtrace.inspect }
        @number_status = 500
      else
        Airbrake.notify(e)
        @reply = {}
        @number_status = 500
      end
    end
  ensure
    render json: @reply, status: @number_status
  end

  def show
    authorize! :show, EmployeeTurnover
    employee_id = params[:id].to_i == 0 ? current_user.employee.id : params[:id].to_i
    company_id  = current_user.company_id
    employee    = Employee.find_by_id_and_company_id(employee_id, company_id)
    labels = {
      :field_0 => DataTableHelper.info_field("Fecha", "date", true),
      :field_1 => DataTableHelper.info_field("Tipo rotación", "string", true),
      :field_2 => DataTableHelper.info_field("Clase de medida", "string", false),
      :field_3 => DataTableHelper.info_field("Motivo de medida", "string", false)
    }
    rotation = rotation_employee(employee)
    @reply = {
      "labels" => labels,
      "list"   => rotation
    }.as_json

    @number_status = 200
  rescue => e
    unless rescue_with_handler(e)
      if Rails.env.development?
        @reply = {status: e.message, details: e.backtrace.inspect }
        @number_status = 202
      else
        Airbrake.notify(e)
        @reply = {}
        @number_status = 500
      end
    end
  ensure
    render json: @reply, status: @number_status
  end

  protected
  def rotation_employee(employee)
    rotation = employee.employee_turnovers.select(:begda, :type_rotation, :denomination_class, :denomination_motive)
    rotations = []
    if rotation.length > 0
      rotation.each do |r|
        rotations << {
          "field_0"=> DataTableHelper.format_date(r.begda),
          "field_1"=> r.type_rotation,
          "field_2"=> r.denomination_class,
          "field_3"=> r.denomination_motive
        }
      end
    end
    return rotations
  end
end