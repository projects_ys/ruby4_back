class Api::V2::OrganizationChartController < AuthorizeController
  skip_load_and_authorize_resource :only => [:index,:search]
  def index
    authorize! :index,:parent_organizate
    company = current_user.company
    if params[:page].nil?
      params[:page] = 1
    end

    unless params[:pernr].present? || (params[:pernr] == current_user.employee.pernr)
      employee = current_user.employee
    else
      employee = Employee.find_by_pernr_and_company_id(params[:pernr],company.id)
    end
    boss = employee.employee_boss
    if boss.present?
      if current_user.employee.employee_boss == employee
        organizate_chart = {
            email:'',
            pernr:'',
            image:{url:company.logo_inside.url},
            posicion:'',
            short_name:company.name
        }
        organizate_chart[:relationship] = '001'
      else
        organizate_chart = boss.as_json(
            only:[:pernr,:email,:posicion,:image],
            methods:[:short_name]
        )
        organizate_chart[:relationship] = '101'
      end
      organizate_chart[:children] = [my_organizate(employee)]
    else
      organizate_chart = my_organizate(employee)
    end
    @reply = organizate_chart
  rescue => e
    unless rescue_with_handler(e)
      if Rails.env.development?
        @reply = {status: e.message, details: e.backtrace.inspect }
        @number_status = 202
      else
        Airbrake.notify(e)
        @reply = {}
        @number_status = 500
      end
    end
  ensure
    render json: @reply,status:@number_status
  end

  def search
    search = []
    if params[:search].present?
      #search = current_user.employee.search_all_subordinates(params[:search]).as_json(only:[:id,:image,:pernr],methods:[:short_name])
      search = current_user.company.employees.where(["lower(name_complete) LIKE :search",search: "%#{params[:search].downcase.gsub(' ',' % ')}%"]).as_json(only:[:id,:image,:pernr],methods:[:short_name])
    end
    @reply = search
  rescue => e
    unless rescue_with_handler(e)
      if Rails.env.development?
        @reply = {status: e.message, details: e.backtrace.inspect }
        @number_status = 202
      else
        Airbrake.notify(e)
        @reply = {}
        @number_status = 500
      end
    end
  ensure
    render json: @reply,status:@number_status
  end

  private
  def current_ability
    unless params[:pernr].present?
      employee = current_user.employee
    else
      employee = Employee.find_by_pernr_and_company_id params[:pernr],current_user.company_id
    end
    @current_ability ||= Ability.new(current_user,employee)
  end

  def my_organizate(employee)
    reply = employee.as_json(
        only:[:pernr,:email,:posicion,:image],
        methods:[:short_name,:total_children]
    )
    if employee.employee_boss.present?
      reply[:relationship] = '101'
    else
      reply[:relationship] = '001'
    end
    reply[:children] = employee.children(params[:page])
    reply
  end

end