class Api::V2::TokenValidationsController < DeviseTokenAuth::TokenValidationsController

  protected

  def render_validate_token_success
    render json: {
        success: true,
        data: @resource.as_json(
            include:  {employee:{include:[],methods:[:short_name,:has_articles]}},
            methods: [],
            except: [
                :tokens, :created_at, :updated_at
            ]
        )
    }
  end

end
