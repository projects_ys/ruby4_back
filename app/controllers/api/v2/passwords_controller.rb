class Api::V2::PasswordsController < DeviseTokenAuth::PasswordsController
  def update
    @resource = resource_class.reset_password_by_token({
                                                           reset_password_token: resource_params[:reset_password_token]
                                                       })
    # make sure user is authorized
    unless @resource
      return render_update_error_unauthorized
    end

    # make sure account doesn't use oauth2 provider
    unless @resource.provider == 'email'
      return render_update_error_password_not_required
    end

    # ensure that password params were sent
    unless password_resource_params[:password] and password_resource_params[:password_confirmation]
      return render_update_error_missing_password
    end

    if @resource.send(resource_update_method, password_resource_params)
      yield if block_given?
      return render_update_success
    else
      return render_update_error
    end
  end
end
