class Api::V2::EmbargoController < AuthorizeController
  skip_load_and_authorize_resource :only => [:by_employee]
  def by_employee
    employee_id = params[:employee_id].to_i == 0 ? current_user.employee.id : employee_id
    company_id  = current_user.company_id
    employee    = Employee.find_by_id_and_company_id(employee_id, company_id)
    labels = {
      :field_0 => DataTableHelper.info_field("Periodo", "date", true),
      :field_1 => DataTableHelper.info_field("Descripción", "string", true),
      :field_2 => DataTableHelper.info_field("Juzgado", "string", false),
      :field_3 => DataTableHelper.info_field("Demandante", "string", false),
      :field_4 => DataTableHelper.info_field("Monto", "money", false),
      :field_5 => DataTableHelper.info_field("Pagos", "money", false),
      :field_6 => DataTableHelper.info_field("Saldos", "money", false)
    }

    data = Embargo.select("edate as field_0, desga as field_1, desor as field_2, gorna as field_3, value as field_4, gardv as field_5, balan as field_6")
    .where("employee_id = ?", employee.id)
    .where("company_id = ?", company_id)
    .order(created_at: :desc)

    @reply = {
      "labels" => labels,
      "list"   => data
    }.as_json
  rescue => e
    unless rescue_with_handler(e)
      if Rails.env.development?
        @reply = {status: e.message, details: e.backtrace.inspect }
        @number_status = 202
      else
        Airbrake.notify(e)
        @reply = {}
        @number_status = 500
      end
    end
  ensure
    render json: @reply,status:@number_status
  end

  protected

  def get_by_employee employee_id, rows, is_distinct
    query = Embargo.select(rows).where("employee_id = ?", employee_id).order(edate: :desc)
    if is_distinct
      query = query.distinct()
    end
    query
  end
end
