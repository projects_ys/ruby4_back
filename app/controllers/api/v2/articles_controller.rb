class Api::V2::ArticlesController < AuthorizeController
  before_action :set_article, only: [:update, :destroy]
  skip_load_and_authorize_resource :only => [:viewed,:categories_articles,:create,:update,:destroy]

  def index
    authorize! :read, Article
    @articles = Article.where(:company_id=>current_user.company_id,:publicado=>'TRUE').order('created_at DESC').as_json(include_articles)
    @myArticles = Article.where(:company_id=>current_user.company_id,:publicado=>'FALSE',:employee_id => current_user.employee.pernr).order('created_at DESC').as_json(include_articles)
    @articles = @articles | @myArticles
    @reply = @articles
    @number_status = 200
  rescue => e
    rescue_with_handler(e)
  ensure
    render json: @reply,status:@number_status
  end

  def show
    authorize! :read, Article
    @article = Article.find_by_id_and_company_id_and_publicado(params[:id],current_user.company_id,'TRUE').as_json({only:[:id,:titulo,:ini,:cuerpo,:publicado,:created_at],include:{employee:{only:[:pernr,:image],methods:[:short_name]},my_category:{only:[:identifier,:value]}},:methods=> [:num_views,:num_comments]})
    unless @article.present?
      @article = Article.find_by_id_and_company_id_and_employee_id(params[:id],current_user.company_id,current_user.employee.pernr).as_json({only:[:id,:titulo,:ini,:cuerpo,:publicado,:created_at],include:{employee:{only:[:pernr,:image],methods:[:short_name]},my_category:{only:[:identifier,:value]}},:methods=> [:num_views,:num_comments]})
    end
    @reply = @article
    @number_status = 200
  rescue => e
    rescue_with_handler(e)
  ensure
    render json: @reply,status:@number_status
  end

  def create
    authorize! :create, Article
    @article = Article.new(article_params)
    @article.employee_id = current_user.employee.pernr
    @article.company_id = current_user.company_id
    if @article.save
      @reply = @article
      @number_status = 200
    else
      @reply = @article.errors.full_messages
      @number_status = 400
    end
  rescue => e
    rescue_with_handler(e)
  ensure
    render json: @reply,status:@number_status
  end

  def update
    authorize! :update, @article
    if @article.update(article_params)
      @reply = @article
      @number_status = 200
    else
      @reply = @article.errors.full_messages
      @number_status = 400
    end
  rescue => e
    rescue_with_handler(e)
  ensure
    render json: @reply,status:@number_status
  end

  def destroy
    authorize! :destroy, @article
    if @article.destroy
      @reply = {}
    end
  rescue => e
    unless rescue_with_handler(e)
      if Rails.env.development?
        @reply = {status: e.message, details: e.backtrace.inspect }
        @number_status = 202
      else
        Airbrake.notify(e)
        @reply = {}
        @number_status = 500
      end
    end
  ensure
    render json: @reply,status:@number_status
  end

  def viewed
    authorize! :create, ViewArticle
    @view_article = ViewArticle.new employee_id: current_user.employee.pernr, article_id: params[:id]
    if @view_article.save
      @reply = @view_article
      @number_status = 200
    else
      @reply = @view_article.errors.full_messages
      @number_status = 400
    end
  rescue => e
    rescue_with_handler(e)
  ensure
    render json: @reply,status:@number_status
  end

  def categories_articles
    authorize! :read, CustomList
    categories_articles = CustomList.where(:object_name => Article,:field => "categories",company_id: current_user.company_id,enabled: true).order(order_priority: :asc)
    @reply = {:categories_articles => categories_articles}
    @number_status = 200
  rescue => e
    rescue_with_handler(e)
  ensure
    render json: @reply,status:@number_status
  end

  private
  def set_article
    authorize! :read, Article
    @article = Article.find_by_id_and_company_id_and_employee_id(params[:id],current_user.company_id, current_user.employee.pernr)
  end

  def include_articles
    {only:[:id,:titulo,:ini,:publicado,:created_at],include:{employee:{only:[:pernr,:image],methods:[:short_name]},my_category:{only:[:identifier,:value]}},:methods=> [:num_views,:num_comments]}
  end

  def article_params
    params.permit(:titulo,:cuerpo,:ini,:publicado,:category)
  end
end
