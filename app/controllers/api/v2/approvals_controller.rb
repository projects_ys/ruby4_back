class Api::V2::ApprovalsController < AuthorizeController
  before_action :set_request, only: [:show,:update]
  skip_load_and_authorize_resource :only => [:index,:show,:update]
  include RequestsConcern

  def index
    authorize! :read, RequestsEmployee
    conditions = {}
    #Type request
    if params[:state].downcase == 'pending'
      state_to_search_first = 'ReqEmpStaPenAproJef'
      state_to_search_second = 'ReqEmpStaPenRH'
      order = :asc
    elsif params[:state].downcase == 'managed'
      state_to_search_first = ["ReqEmpStaCanJef","ReqEmpStaPenRH","ReqEmpStaCanRH","ReqEmpStaAprRH"]
      state_to_search_second = ["ReqEmpStaCanRH","ReqEmpStaAprRH"]
      order = :desc
    end

    #Pagination's
    unless params[:page].present?
      params[:page] = 1
    end


    #Search
    if params[:search].present?
      employees = current_user.company.employees
      search = params[:search].downcase
      search_matches_name = search.gsub(' ',' % ')
      conditions[:pernr] = employees
                        .where("lower(name_complete) LIKE ? OR number_document = ?","%#{search_matches_name}%",search).pluck(:pernr)
    end

    if params[:approver].downcase == 'first'
      conditions[:status_id] = state_to_search_first
      requests = current_user.employee
                     .first_approver_requests_received
                     .where(conditions)
                     .order(created_at: order)
                     .paginate(:page => params[:page],:per_page => 10)

    elsif params[:approver].downcase == 'second'
      conditions[:status_id] = state_to_search_second
      requests = current_user.employee
                     .second_approver_requests_received
                     .where(conditions)
                     .order(created_at: order)
                     .paginate(:page => params[:page],:per_page => 10)
    end

    @reply = requests.as_json(
        include:{
            types_requests:{
                only:[:form,:description]
            },
            status:{
                only:[:identifier,:value]
            },
            employee:{
                only:[:posicion,:image],
                methods:[:short_name]
            }
        }
    )
    @number_status = 200
  rescue => e
    rescue_with_handler(e)
  ensure
    render json: @reply,status:@number_status
  end

  def show
    authorize! :read, @request
    @reply = @request.as_json(
        only:[:id,:date_begin,:date_end,:created_at,:company_id,:observation_request,:comment_first_approver,:date_first_approval,:comment_first_approver,:date_second_approval,:comment_second_approver,:file_support,:days_request],
        include:{
            types_requests:{
                only:[:form,:description]
            },
            status:{
                only:[:identifier,:value]
            },
            employee:{
                only:[:image,:posicion],
                methods:[:short_name]
            },
            applicants_crossed:{
                include:{
                    employee:{
                        only:[],
                        methods:[:short_name]
                    }
                }
            }
        }
    )
    @number_status = 200
  rescue => e
    rescue_with_handler(e)
  ensure
    render json: @reply,status:@number_status
  end

  def update
    authorize! :update, @request
    RequestsEmployee.transaction do
      if @request.update(approval_params)
        second_approver = @request.second_approver
        employee_requesting = @request.employee
        @info_mail = {
            id: @request.id,
            status: @request.status.value,
            type: @request.types_requests.description,
            approval_to: @request.first_approval_name,
            date_approval: @request.date_first_approval,
            comment: @request.comment_first_approver,
            second_approver_name: second_approver.short_name,
            second_approver_email: second_approver.email,
            name_requesting: employee_requesting.short_name,
            email_requesting: employee_requesting.email.downcase,
            domain_approval: "#{employee_requesting.company.front_domain}/#/request/view/#{@request.id}",
            domain_requesting:  "#{employee_requesting.company.front_domain}/#/approved/pending_detail/#{@request.id}",
            name_company:@request.company.name
        }
=begin
        unless Rails.env.production?
          @info_mail[:email_requesting] = 'juan.contreras@hrsolutions-co.com'
          @info_mail[:second_approver_email] = 'andres.suarez@hrsolutions-co.com'
        end
=end
        EmployeeNotificationsApprovals.first_approval_update(@info_mail).deliver
        if @request.status_id == 'ReqEmpStaPenRH'
          SecondApproverNotifications.new_request_registered(@info_mail).deliver
        end
        @reply = @request
        @number_status =  200
      else
        @reply = @request.errors.full_messages
        @number_status =  400
      end
    end
  rescue => e
    unless rescue_with_handler(e)
      ActiveRecord::Rollback
      if Rails.env.development?
        @reply = {status: :error, details: e.message }
        @number_status =  500
      else
        Airbrake.notify(e)
        @reply = {}
        @number_status =  500
      end
    end

  ensure
    render json: @reply,status:@number_status
  end

  private

  def set_request
    @request = RequestsEmployee.find_by_id params[:id]
  end

  def approval_params
    params_approval = params.require(:approval).permit(:status_id,:comment_first_approver)
    employee = current_user.employee
    params_approval[:first_approval_name] = employee.short_name
    params_approval[:date_first_approval] = Time.now
    params_approval[:first_approval_pernr] = employee.pernr
    params_approval[:first_approval_position] = employee.id_posicion
    params_approval
  end
end