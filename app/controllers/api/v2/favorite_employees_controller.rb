class Api::V2::FavoriteEmployeesController < AuthorizeController
  skip_load_and_authorize_resource :only => [:index,:create,:destroy]

  def index
    if params[:search].present?
      query = current_user.company.employees.select(info_select(params[:type]))
                  .where(search_new)
                  .where.not(:pernr => current_user.employee.pernr)
                  .consult_employees_fav(params[:type],subquery_favorites)
                  .paginate(:page => params[:page],:per_page => 10)
                  .order(name: :asc,lastname: :asc)
    else
      query = current_user.company.employees.select(info_select(params[:type]))
                  .where.not(:pernr => current_user.employee.pernr)
                  .consult_employees_fav(params[:type],subquery_favorites)
                  .paginate(:page => params[:page],:per_page => 10)
                  .order(name: :asc,lastname: :asc)
    end
    @reply = query.as_json(methods:[:short_name])
    @number_status = 200
  rescue => e
    unless rescue_with_handler(e)
      if Rails.env.development?
        @reply = {status: e.message, details: e.backtrace.inspect }
        @number_status = 202
      else
        Airbrake.notify(e)
        @reply = {}
        @number_status = 500
      end
    end
  ensure
    render json: @reply,status:@number_status
  end

  def create
    @favorite = FavoriteEmployee.new(params_favorite)
    @favorite.employee_id = current_user.employee.pernr
    @favorite.company_id = current_user.company_id
      if @favorite.save
        @reply = @favorite
        @number_status = 200
      else
        @reply = @favorite.errors.full_messages
        @number_status = 400
      end
  rescue => e
    unless rescue_with_handler(e)
      if Rails.env.development?
        @reply = {status: e.message, details: e.backtrace.inspect }
        @number_status = 202
      else
        Airbrake.notify(e)
        @reply = {}
        @number_status = 500
      end
    end
  ensure
    render json: @reply,status:@number_status
  end

  def destroy
    @favorite = FavoriteEmployee.find_by_favorite_employee_and_employee_id_and_company_id(params[:id],current_user.employee.pernr,current_user.company_id)
    if @favorite.present?
      if @favorite.destroy
        @reply = {status: :deleted}
        @number_status = 200
      else
        @reply = {}
        @number_status = 400
      end
    else
      @reply = {}
      @number_status = 404
    end
  rescue => e
    unless rescue_with_handler(e)
      if Rails.env.development?
        @reply = {status: e.message, details: e.backtrace.inspect }
        @number_status = 202
      else
        Airbrake.notify(e)
        @reply = {}
        @number_status = 500
      end
    end
  ensure
    render json: @reply,status:@number_status
  end

  private
  def params_favorite
    params.permit(:favorite_employee)
  end
  def params_search
    params.permit(:name_search,:type_scroll,:numrecords,:record)
  end
  def info_select(type)
    case type
      when 'favorites'
        [:id,:name,:lastname,:image,:phone,:email,:pernr]
      when 'nofavorites'
        [:id,:name,:lastname,:image,:pernr]
    end
  end
  def info_favorite
    [:id,:name,:lastname,:image,:phone,:email,:pernr]
  end
  def info_nofavorite
    [:id,:name,:lastname,:image,:pernr]
  end
  def subquery_favorites
    current_user.employee.favorite_employees_user.select('favorite_employee')
  end
  def search_conditions
    ["lower(name_complete) LIKE :search",search: "%#{params_search[:name_search].downcase.gsub(' ',' % ')}%"]
  end
  def search_new
    ["lower(name_complete) LIKE :search",search: "%#{params[:search].downcase.gsub(' ',' % ')}%"]
  end
  def favorite_info favorites
    new_favorites = []
    favorites.each do |favorite|
      new_favorites << {
          id:favorite.id,
          name:favorite.name,
          lastname:favorite.lastname,
          email:favorite.email,
          phone:favorite.phone,
          pernr:favorite.pernr,
          image:favorite.image,
          short_name:favorite.short_name,
          favorite_info:favorite.favorite_info.find_by(employee_id: current_user.employee.pernr)
      }
    end
    new_favorites
  end
end