class Api::V2::VacationBalanceRecordsController < AuthorizeController
  skip_load_and_authorize_resource :only => [:by_employee]
  def by_employee
    employee_id = params[:employee_id].to_i == 0 ? current_user.employee.id : employee_id
    company_id  = current_user.company_id
    employee    = Employee.find_by_id_and_company_id(employee_id, company_id)

    labels = {
      :field_0 => DataTableHelper.info_field("Descripción", "string", true),
      :field_1 => DataTableHelper.info_field("Fecha Inicial", "date", true),
      :field_2 => DataTableHelper.info_field("Fecha Final", "date", false),
      :field_3 => DataTableHelper.info_field("Días totales", "day", false),
      :field_4 => DataTableHelper.info_field("Días tomados", "day", false),
      :field_5 => DataTableHelper.info_field("Días pendientes", "day", false),
    }

    vacation_balance_records = employee.vacation_balance_records
    .select("ktext, begda, endda, anzhl, kverb")
    .order(endda: :desc)
    .as_json only: [], methods: [:field_0, :field_1, :field_2, :field_3, :field_4, :field_5]
    @reply = {
      "labels" => labels,
      "list"   => vacation_balance_records
    }.as_json
    @number_status = 200
  rescue => e
    unless rescue_with_handler(e)
      if Rails.env.development?
        @reply = {status: e.message, details: e.backtrace.inspect }
        @number_status = 202
      else
        Airbrake.notify(e)
        @reply = {}
        @number_status = 500
      end
    end
  ensure
    render json: @reply,status: @number_status
  end
end
