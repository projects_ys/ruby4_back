class Api::V2::TypesRequestController < AuthorizeController
  skip_load_and_authorize_resource :only => [:index]

  def index
    employee_login = current_user.employee
    my_types_request = employee_login
                           .group_employees_request
                           .types_requests_records
                           .where(type_request:params[:type].upcase,active:true)
                           .order(description: :asc )
    types_with_contingent = []
    my_types_request.each do |type|
      types_with_contingent << {
          id: type.id,
          id_activity: type.id_activity,
          subtype: type.subtype,
          description: type.description,
          moabw:type.moabw,
          type_request:type.type_request,
          form:type.form,
          company_id:type.company_id,
          available_contingent: available_contingent(employee_login,type)
      }
    end

    @reply = {
        :types_request => types_with_contingent
    }

  rescue => e
    rescue_with_handler(e)
  ensure
    render json: @reply,status:@number_status
  end

  private

  def params_permit
    params.permit(:type)
  end

  def available_contingent(employee,type_request_user)
    case type_request_user.id_activity
      when 'VACA','VCCP'
        contingents = employee.vacation_balance_records.where(:subty => type_request_user.contingent)
        available = contingents.sum(:anzhl)
        used = contingents.sum(:kverb)
        contingent_available = available - used

        #solicitudes activas con contingentes
        requests_open = employee.requests_employees.where(:status_id => ['ReqEmpStaPenAproJef','ReqEmpStaPenRH'],:id_activity => type_request_user.id_activity).sum(:days_request)
        total_data_available = contingent_available.to_f - requests_open.to_f
      else
        total_data_available = nil
    end
    total_data_available
  end

end
