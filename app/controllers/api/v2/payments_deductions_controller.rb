class Api::V2::PaymentsDeductionsController < AuthorizeController
  skip_authorize_resource :only => :by_employee

  def by_employee
      puts current_user.inspect
      employee_id = params[:employee_id].to_i == 0 ? current_user.employee.id : employee_id
      company_id  = current_user.company_id
      employee    = Employee.find_by_id_and_company_id(employee_id, company_id)

      labels = {
        :field_0 => DataTableHelper.info_field("Periodo", "string", true),
        :field_1 => DataTableHelper.info_field("Concepto acumulado", "string", true),
        :field_2 => DataTableHelper.info_field("Concepto de nómina", "string", false),
        :field_3 => DataTableHelper.info_field("Valor", "money", false)
      }

      payments_deductions = PaymentsDeduction
      .select("year_report, text_cumulative_concept, description_payroll_concept, amount")
      .where('employee_id = ?',employee.id)
      .order('year_report DESC')
      .as_json only: [], methods: [:field_0, :field_1, :field_2, :field_3]

      @reply = {
        "labels" => labels,
        "list"   => payments_deductions
      }.as_json
      @number_status = 200
  rescue => e
    unless rescue_with_handler(e)
      if Rails.env.development?
        @reply = {status: e.message, details: e.backtrace.inspect }
        @number_status = 202
      else
        Airbrake.notify(e)
        @reply = {}
        @number_status = 500
      end
    end
  ensure
    render json: @reply,status: @number_status
  end
end
