class Api::V2::RequestsEmployeesController < AuthorizeController
  before_action :set_request, only: [:show,:destroy]
  skip_load_and_authorize_resource :only => [:index,:create,:show,:requests_managed]
  include RequestsConcern
  def index
    if params[:state].downcase == 'pending'
      state_to_search= 'ReqEmpStaPenAproJef'
      order = :asc
    elsif params[:state].downcase == 'managed'
      state_to_search = ["ReqEmpStaCanJef","ReqEmpStaPenRH","ReqEmpStaCanRH","ReqEmpStaAprRH"]
      order = :desc
    end

    unless params[:page].present?
      params[:page] = 1
    end

    requests = current_user.employee
                   .requests_employees
                   .where(:status_id => state_to_search)
                   .order(created_at: order)
                   .paginate(:page => params[:page],:per_page => 10)

    @reply = requests.as_json(
        include:{
            types_requests:{
            only:[:form,:description]
            },
            status:{
                only:[:identifier,:value]
            },
            current_approver:{
                only:[:posicion,:image],
                methods:[:short_name]
            }
    })
    @number_status = 200
  rescue => e
    unless rescue_with_handler(e)
      if Rails.env.development?
        @reply = {status: e.message, details: e.backtrace.inspect }
        @number_status = 202
      else
        Airbrake.notify(e)
        @reply = {}
        @number_status = 500
      end
    end
  ensure
    render json: @reply,status: @number_status
  end

  def show
    authorize! :read, @request
    @reply = @request.as_json(
        only:[:id,:date_begin,:date_end,:created_at,:company_id,:observation_request,:comment_first_approver,:date_first_approval,:comment_first_approver,:date_second_approval,:comment_second_approver,:file_support,:days_request],
           include:{
               types_requests:{
                   only:[:form,:description]
               },
               status:{
                   only:[:identifier,:value]
               },
               current_approver:{
                   only:[:image,:posicion],
                   methods:[:short_name]
               },
           }
    )
    @number_status = 200
  rescue => e
    unless rescue_with_handler(e)
      if Rails.env.development?
        @reply = {status: e.message, details: e.backtrace.inspect }
        @number_status = 202
      else
        Airbrake.notify(e)
        @reply = {}
        @number_status = 500
      end
    end
  ensure
    render json: @reply,status: @number_status
  end

  def request_states
    states =  current_user.company.custom_lists
                  .select(:identifier,:value,:order_priority)
                  .where(:field => :status,:object_name => RequestsEmployee,:enabled => true)
                  .order(order_priority: :asc)
    @reply = {
        states: states
    }
    @number_status = 200
  rescue => e
    unless rescue_with_handler(e)
      if Rails.env.development?
        @reply = {status: e.message, details: e.backtrace.inspect }
        @number_status = 202
      else
        Airbrake.notify(e)
        @reply = {}
        @number_status = 500
      end
    end
  ensure
    render json: @reply,status: @number_status
  end


  def create
    RequestsEmployee.transaction do
      @request = RequestsEmployee.new(request_params)
      employee = current_user.employee
      @request.pernr = employee.pernr
      @request.company_id = employee.company_id
      @request.status_id = 'ReqEmpStaPenAproJef'
      @request.name_applicant = employee.name_complete
      @request.typedoc_applicant = employee.type_document
      @request.numdoc_applicant = employee.number_document

      unless request_params[:types_requests_record_id].nil?
        activityTypeRequest = TypesRequestsRecord.find_by_form_and_company_id request_params[:types_requests_record_id],@request.company_id
        first_approver = approver_asigned(activityTypeRequest.id_activity,:first,employee)
        second_approver = approver_asigned(activityTypeRequest.id_activity,:second,employee)
        @request.id_activity = activityTypeRequest.id_activity
      end

      unless first_approver.nil?
        @request.first_approver_assigned = first_approver.pernr
      end

      unless second_approver.nil?
        @request.second_approver_assigned = second_approver.pernr
      end

      if(@request.days_request == nil && @request.date_end.present?)
        @request.days_request = calculate_days_request
      end
      if @request.save
        @number_status = 200
        @reply = @request.as_json
        company = @request.company
        type_request = @request.types_requests
        @employee_requesting = {
            :name => employee.short_name,
            :email => current_user.email,
            :email_approver => first_approver.email,
            :approver => first_approver.short_name,
            :type_request => type_request.description,
            :name_company => company.name,
            :domain =>  company.front_domain
        }
        @first_approval = {
            :name => first_approver.short_name,
            :link_request => "#{company.front_domain}/#/approved/pending_detail/#{@request.id}",
            :name_company => company.name,
            :email_approver => first_approver.email,
            :employee_requesting =>employee.short_name,
            :type_request => type_request.description
        }
=begin
        unless Rails.env.production?
          @employee_requesting[:email] = 'juan.contreras@hrsolutions-co.com'
          @first_approval[:email_approver] = 'andres.suarez@hrsolutions-co.com'
        end
=end
        EmployeeNotificationsApprovals.registered_request(@employee_requesting).deliver
        FirstApproverNotifications.new_request_received(@first_approval).deliver
      else
        @number_status = 400
        @reply = @request.errors.full_messages.as_json
      end
    end
  rescue => e
    unless rescue_with_handler(e)
      if Rails.env.development?
        @reply = {error: e.inspect, details:e.backtrace.inspect }
        @number_status = 500
      else
        Airbrake.notify(e)
        @reply = {}
        @number_status = 500
      end
    end
  ensure
    render json: @reply, status: @number_status
  end



  def destroy
    if @request.destroy
      @reply = {status: :deleted}
      @number_status = 200
    else
      @reply = @request.errors.full_messages
      @number_status = 400
    end
    @number_status = 200
  rescue => e
    unless rescue_with_handler(e)
      if Rails.env.development?
        @reply = {error: e.inspect, details:e.backtrace.inspect }
        @number_status = 500
      else
        Airbrake.notify(e)
        @reply = {}
        @number_status = 500
      end
    end
  ensure
    render json: @reply, status: @number_status
  end

  private

  def set_request
    @request = RequestsEmployee.find_by_id_and_pernr params[:id],current_user.employee.pernr
  end

  def request_params
    params.permit(:date_begin,:date_end,:observation_request,:types_requests_record_id,:days_request,:file_support)
  end

  def calculate_days_request
    calendar = current_user.employee.calendar_breaks.where("date_break >= ? AND date_break <= ?",@request.date_begin,@request.date_end)
    days_calendar = calendar.size.to_f
    days_breaks_calendar = ((@request.date_end - @request.date_begin)/1.day).to_f + 1
    days_breaks_calendar - days_calendar
  end

end
