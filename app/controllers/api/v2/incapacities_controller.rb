class Api::V2::IncapacitiesController < AuthorizeController
  skip_load_and_authorize_resource :only => [:by_employee]
  def by_employee
    employee_id = params[:employee_id].to_i == 0 ? current_user.employee.id : employee_id
    company_id  = current_user.company_id
    employee    = Employee.find_by_id_and_company_id(employee_id, company_id)

    labels = {
      :field_0 => DataTableHelper.info_field("Motivo de la Incapcidad", "string", true),
      :field_1 => DataTableHelper.info_field("Fecha Inicial", "date", true),
      :field_2 => DataTableHelper.info_field("Fecha Final", "date", false),
      :field_3 => DataTableHelper.info_field("Días hábiles asignados", "day", false),
      :field_4 => DataTableHelper.info_field("Días calendario recibidos", "day", false)
    }

    incapacities = Incapacity.select("text_incapacity, begda, endda, days_working_incapacity, days_taken_incapacity")
    .where("employee_id = ?", employee.id)
    .where("company_id = ?", company_id)
    .order(begda: :desc)
    .as_json only: [], methods: [:field_0, :field_1, :field_2, :field_3, :field_4]

    @reply = {
      "labels" => labels,
      "list"   => incapacities
    }.as_json
    @number_status = 200
  rescue => e
    unless rescue_with_handler(e)
      if Rails.env.development?
        @reply = {status: e.message, details: e.backtrace.inspect }
        @number_status = 202
      else
        Airbrake.notify(e)
        @reply = {}
        @number_status = 500
      end
    end
  ensure
    render json: @reply,status: @number_status
  end
end
