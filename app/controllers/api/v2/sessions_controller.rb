class Api::V2::SessionsController < DeviseTokenAuth::SessionsController
  before_filter :company_info, only: :create
  def create
    # Check
    field = (resource_params.keys.map(&:to_sym) & resource_class.authentication_keys).first

    @resource = nil

    if field
      q_value = resource_params[field]

      if resource_class.case_insensitive_keys.include?(field)
        q_value.downcase!
      end

      q = "#{field.to_s} = ? AND provider='email' AND company_id = ?"

      if ActiveRecord::Base.connection.adapter_name.downcase.starts_with? 'mysql'
        q = "BINARY " + q
      end
      @ldap_authenticated = false
      @failed_authenticated_ldap = false
      @resource = resource_class.where(q, q_value, @company.id).first
    end
    if !@resource.nil? && !@resource.access_locked?
      if @company.login_ldap == true
        if @company.fixed_ip == true
          @ldap_authenticated, @failed_authenticated_ldap = connection_fixed_ip['ldap_authenticated'],connection_fixed_ip['failed_authenticated_ldap']
        else
          if @company.ldap_connection == 'ADLDS'
            credentials = { method: :simple, :username => q_value, :password => resource_params[:password] }
          else
            credentials = { method: :simple, :username => @company.username_ldap, :password => @company.password_ldap }
          end
          if @company.ldap_ssl == true
            ldap = Net::LDAP.new(
                host: @company.host_ldap,    # Thankfully this is a standard name
                port: @company.port_ldap,
                encryption: :simple_tls,
                auth: credentials
            )
          else
            ldap = Net::LDAP.new(
                host: @company.host_ldap,    # Thankfully this is a standard name
                port: @company.port_ldap,
                auth: credentials
            )
          end
          if ldap.bind
            if @company.ldap_connection == 'ADLDS'
              @ldap_authenticated = true
              @failed_authenticated_ldap = false
            else
              if @company.encryption_method_ldap != nil
                encrypted_password = encrypted_password(resource_params['password'])
              else
                encrypted_password = resource_params['password']
              end
              filter = Net::LDAP::Filter.eq( "mail", q_value) & Net::LDAP::Filter.eq( "userpassword", encrypted_password )
              treebase = @company.treebase_ldap

              ldap.search( :base => treebase, :filter => filter ) do |u|
                @ldap_authenticated  = true
              end
              if @ldap_authenticated == false
                filter_failed = Net::LDAP::Filter.eq( "mail", q_value)
                ldap.search( :base => treebase, :filter => filter_failed ) do |u|
                  @failed_authenticated_ldap = true
                end
              end
            end
          else
            @failed_authenticated_ldap = true
            puts ldap.get_operation_result
          end
        end
      end
    end


    if @resource and valid_params?(field, q_value) and (@resource.valid_password?(resource_params[:password]) or @ldap_authenticated) and !@failed_authenticated_ldap and (!@resource.respond_to?(:active_for_authentication?) or @resource.active_for_authentication?)
      # create client id
      @client_id = SecureRandom.urlsafe_base64(nil, false)
      @token     = SecureRandom.urlsafe_base64(nil, false)

      @resource.tokens[@client_id] = {
          token: BCrypt::Password.create(@token),
          expiry: (Time.now + DeviseTokenAuth.token_lifespan).to_i
      }
      @resource.save

      sign_in(:user, @resource, store: false, bypass: false)

      yield if block_given?

      render_create_success
    elsif @resource and not (!@resource.respond_to?(:active_for_authentication?) or @resource.active_for_authentication?)
      render_create_error_not_confirmed
    else
      if @resource != nil
        @resource.failed_attempt_user
      end
      render_create_error_bad_credentials
    end
  rescue => e
    unless rescue_with_handler(e)
      unless Rails.env.development?
        Airbrake.notify(e)
      end
    else
      @reply = e.inspect
      render json: @reply, status: @number_status
    end
  end
  protected

  def company_info
    @company = Company.find_by_front_domain request.headers['origin']
    unless @company.present?
      url = request.headers['origin'].gsub('http://','').gsub('https://','').gsub('www','')
      subdomain = url.split('.')[0]
      @company = Company.find_by_front_subdomain_and_multiple_domains subdomain,true
    end
  end

  def encrypted_password(password)
    case @company.encryption_method_ldap
      when 'md5'
        encryptedPassword = Net::LDAP::Password.generate(:md5, password)
      when 'sha1'
        encryptedPassword = Net::LDAP::Password.generate(:sha, password)
      when 'ssha'
        encryptedPassword = Net::LDAP::Password.generate(:ssha, password)
      else
        encryptedPassword = "#{@company.encryption_method_ldap} Metodo de cifrado no soportado"
    end
    return encryptedPassword
  end

  def render_create_success
    render json: {
        success: true,
        data: @resource.as_json(
            include:  {employee:{include:[],methods:[:short_name,:has_articles]}},
            methods: [],
            except: [
                :tokens, :created_at, :updated_at
            ]
        )
    }
  end

  def connection_fixed_ip
    if Rails.env.production? || Rails.env.staging?
      url_ldap = 'https://admin.hrinteractive.co/login_ldap/index'
      api = @company.oauth_applications.find_by_name  OauthApplication::LDAP_NAME
      response = RestClient.post 'https://admin.hrinteractive.co/oauth/token', {
          grant_type: 'client_credentials',
          client_id: api.uid,
          client_secret: api.secret
      }
      token = JSON.parse(response)["access_token"]

      ldap = RestClient::Request.execute(
          method: :post,
          url: url_ldap,
          payload: {
              :company_id => @company.id,
              :credential_email => @resource.email,
              :password => resource_params['password']
          },
          timeout: 25,
          headers: {
              'Authorization' => "Bearer #{token}",
              :content_type => :json,
              :accept => :json
          },
          :verify_ssl => false
      )
    elsif Rails.env.development?
      url_ldap = 'http://localhost:3001/login_ldap/index'
      api = @company.oauth_applications.find_by_name  OauthApplication::LDAP_NAME
      response = RestClient.post 'http://localhost:3001/oauth/token', {
          grant_type: 'client_credentials',
          client_id: api.uid,
          client_secret: api.secret
      }

      token = JSON.parse(response)["access_token"]

      ldap_fixed = RestClient::Request.execute(
          method: :post,
          url: url_ldap,
          payload: {
              :credential_email => @resource.email,
              :password => resource_params['password']
          },
          timeout: 25,
          headers: {
              'Authorization' => "Bearer #{token}",
              :content_type => :json,
              :accept => :json
          },
          :verify_ssl => false
      )
    end
    JSON.parse(ldap_fixed)
  end

end
