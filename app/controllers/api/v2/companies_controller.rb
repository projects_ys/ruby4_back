class Api::V2::CompaniesController < ApplicationController
  def info_company
    company = Company.select(params_send).find_by_front_domain(request.headers['origin'])
    unless company.present?
      url = request.headers['origin'].gsub('http://','').gsub('https://','').gsub('www','')
      subdomain = url.split('.')[0]
      company = Company.select(params_send).find_by_front_subdomain_and_multiple_domains(subdomain,true)
    end

    render :json => {
        :company => company
    }

  end
  private
  def params_send
    [:id, :logo_dashboard, :logo_inside, :background_login, :background_header_menu, :background_lockscreen, :name_file_css, :primary_color, :text_primary_color,:body_text,:background_wrapper_color ,:login_ldap,:show_birthday,:show_labor,:show_new_employees,:show_articles,:show_certificates_labor,:show_certificates_vacations,:show_certificates_payroll,:show_certificates_income,:show_licenses,:show_loans,:show_vacations,:show_vacations_c,:show_inhabilities,:show_approvals_licenses,:show_services_management,:show_scesa,:show_hoex,:show_approvals_requirements,:show_approvals_master_data,:show_indebtedness_levels,:show_shart_severance,:show_shart_severance_interest,:make_request,:make_approvals,:show_document_management,:show_organizate_chart,:image_organizate_chart]
  end
end
