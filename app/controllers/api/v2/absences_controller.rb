class Api::V2::AbsencesController < AuthorizeController
  skip_load_and_authorize_resource :only => [:show]
  def show
    authorize! :show, Absence
    employee_id = params[:id].to_i == 0 ? current_user.employee.id : params[:id].to_i
    company_id  = current_user.company_id
    employee    = Employee.find_by_id_and_company_id(employee_id, company_id)
    if employee.present?
      labels = {
          :field_0 => DataTableHelper.info_field("Tipo", "string", true),
          :field_1 => DataTableHelper.info_field("Descripción", "string", true),
          :field_2 => DataTableHelper.info_field("Fecha Inicial", "date", true),
          :field_3 => DataTableHelper.info_field("Fecha Final", "date", true),
          :field_4 => DataTableHelper.info_field("Dias Nomina", "int", false),
          :field_5 => DataTableHelper.info_field("Dias Naturales", "int", false)
      }
      absences = absences_employee(employee)
      @reply = {
          "labels" => labels,
          "list"   => absences
      }.as_json
    end
    @number_status = 200
  rescue => e
    unless rescue_with_handler(e)
      if Rails.env.development?
        @reply = {status: e.message, details: e.backtrace.inspect }
        @number_status = 202
      else
        Airbrake.notify(e)
        @reply = {}
        @number_status = 500
      end
    end
  ensure
    render json: @reply,status: @number_status
  end

  protected
  def absences_employee(employee)
    abasences_emple = employee.absences
    if abasences_emple.present?
      absences = []
      abasences_emple.each do |absence|
        case absence.type_absence
          when "P"
            type_absence = "Permiso"
          when "I"
            type_absence = "Incapacidad"
          when "V"
            type_absence = "Vacaciones"
        end
        absences << {
            'field_0' => type_absence,
            'field_1' => absence.description_absence,
            'field_2' => absence.start_absence,
            'field_3' => absence.end_absence,
            'field_4' => absence.days_payroll,
            'field_5' => absence.days_natural
        }
      end
      return absences
    else
      return nil
    end
  end
end
