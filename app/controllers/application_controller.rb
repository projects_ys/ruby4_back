class ApplicationController < ActionController::Base

  include DeviseTokenAuth::Concerns::SetUserByToken

  if Rails.env.development?
    before_action :sign_in_dev
  end
  # rescue_from DeviseLdapAuthenticatable::LdapException do |exception|
  #   render :text => exception, :status => 500
  # end
  before_filter :set_locale
  respond_to :html, :json


  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session
  # protect_from_forgery with: :exception

  # or we can try this antes de filtrar sacar el toquen de autorización
  # after_filter :set_csrf_cookie_for_ng
  # before_action :configure_permitted_parameters, if: :devise_controller?
  #
  #
  # def set_csrf_cookie_for_ng
  #   cookies['XSRF-TOKEN'] = form_authenticity_token if protect_against_forgery?
  # end
  #
  # rescue_from ActionController::InvalidAuthenticityToken do |exception|
  #   puts "ActionController::InvalidAuthenticityToken"
  #   cookies['XSRF-TOKEN'] = form_authenticity_token if protect_against_forgery?
  #   # render :error => 'invalid token', {:status => :unprocessable_entity}
  # end
  #
  # protected
  #
  # # In Rails 4.2 and above extender que sea para las llamadas que no sean get
  # def verified_request?;
  #   puts "super: #{super}"
  #   puts "valid_authenticity_token: #{valid_authenticity_token?(session, request.headers['X-XSRF-TOKEN'])}"
  #   super || valid_authenticity_token?(session, request.headers['X-XSRF-TOKEN'])
  # end
  #
  # # para devise que pasa el parametro de name
  # def configure_permitted_parameters
  #     devise_parameter_sanitizer.for(:sign_up) << :name
  #     devise_parameter_sanitizer.for(:account_update) do |u|
  #           u.permit(:login, :name, :pic, :email, :password, :password_confirmation, :current_password, :image, :company_id, :employee_id)
  #     end
  # end
  #

  protected

  def set_locale
    I18n.locale = :es
  end

  private

  def sign_in_dev
    #sign_in(:user, User.find_by_email('andres.suarez@hrsolutions-co.com'))
    unless current_user.present?
      sign_out :user
      #sign_in(:user, User.find_by_email('juan.contreras@hrsolutions-co.com'))
      #sign_in(:user, User.find_by_email('carlos.rodriguez2@publicar.com'))
      #sign_in(:user, User.find_by_email('andres.suarez@hrsolutions-co.com'))
      #sign_in(:user, User.find_by_email('andrea.martinez@hrsolutions-co.com'))
      #sign_in(:user, User.find_by_email('nbeltran@rcntv.com.co'))
      #sign_in(:user, User.find_by_email('adelgado@rcntv.com.co'))
      #sign_in(:user, User.find_by_email('greyes@rcntv.com.co'))
      #sign_in(:user, User.find_by_email('lbohorquez@rcntv.com.co'))
    end
  end
end
