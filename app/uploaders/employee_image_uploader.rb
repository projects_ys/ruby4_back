# encoding: utf-8

class EmployeeImageUploader < CarrierWave::Uploader::Base

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

   def default_url
     "/images/generic_user_image.jpg"
   end

   def extension_white_list
     %w(jpg jpeg gif png)
   end

end
