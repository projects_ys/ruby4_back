# encoding: utf-8

class CompanyNameFileCssUploader < CarrierWave::Uploader::Base
  
  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{model.id}/#{mounted_as}/"
  end

  def extension_white_list
      %w(css)
  end

end
