class Company < ActiveRecord::Base

  crypt_keeper :password_sap,:host_ldap,:username_ldap,:password_ldap,:encryptor => :aes_new, :key => Rails.application.secrets.secret_key_encryption, salt: 'salt', :encoding => 'UTF-8'

  has_many :users, dependent: :destroy
  has_many :employees, dependent: :destroy
  has_many :employee_infos
  has_many :custom_lists
  has_many :articles
  has_many :comments_articles
  has_many :loans
  has_many :severances
  has_many :vacation_requirements
  has_many :compensatory_vacations
  has_many :infos
  has_many :inhability_requirements
  has_many :license_requirements
  has_many :extra_requirements

  has_many :requests_employees
  has_many :labors
  has_many :positions
  has_many :employee_turnovers
  has_many :embargos
  has_many :oauth_applications
  has_many :file_employees
  #Types request
  #has_many :types_requests_records, dependent: :destroy
  has_many :types_request_for_groups

  has_many :pdf_types
  has_one :master_datum

  def total_users_last_months
    company_users   = self.last_mounths_company_user
    total_users     = self.total_employees_or_users company_users, "total_users"
    total_employees = self.total_employees_or_users company_users, "total_employees"
    {users:total_users, employees:total_employees}
  end


  mount_uploader :logo, CompanyImagesUploader
  mount_uploader :logo_dashboard, CompanyImagesUploader
  mount_uploader :logo_inside, CompanyImagesUploader
  mount_uploader :background_login, CompanyImagesUploader
  mount_uploader :background_header_menu, CompanyImagesUploader
  mount_uploader :background_lockscreen, CompanyImagesUploader
  mount_uploader :image_organizate_chart, CompanyImagesUploader
  mount_uploader :name_file_css, CompanyNameFileCssUploader
  mount_uploader :file_translate, CompanyFileTranslateUploader

  protected
  def last_mounths_company_user
    time = Time.zone.now - 5.months
    company_users = CompanyUser.select("extract(year from created_at) as year, extract(month from created_at) as month").distinct
    .where("company_id = ?", self.id)
    .where("created_at >= ?", time)
    .order("year, month asc")
  end

  def total_employees_or_users company_users, field
    total = []
    company_users.each do |value|
      max_users = CompanyUser.select("id, #{field}, extract(year from created_at) as year, extract(month from created_at) as month, created_at")
      .where("company_id = ?", self.id)
      .where("TO_CHAR(created_at, 'YYYY-MM') = ?", "#{value.year.to_i}-#{TaskUtil.number_valid_month(value.month.to_i)}")
      .order("#{field} desc").first
      total << max_users
    end
    total
  end


end
