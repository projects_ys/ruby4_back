class Severance < ActiveRecord::Base

  include ActionView::Helpers::NumberHelper

  STATUS_ID = { pendiente: 1, aprobado: 2, denegado: 3 }
  STATUS_NAME = { 1 => 'Pendiente', 2 => 'Aprobado', 3 => 'Denegado'}

  belongs_to :employee
  belongs_to :company

  belongs_to :approver, foreign_key: :approver_id_posicion,  primary_key:  :id_posicion, class_name: 'Employee'
  belongs_to :approved, foreign_key: :approved_by_identification,  primary_key:  :pernr, class_name: 'Employee'

  validates_presence_of :employee_id, :company_id, :subty, :monto, :status, :approver_id_posicion, :numero_de_actorizacion, :fecha_de_corte
  validate :exists_status

  def exists_status
    unless STATUS_NAME[self.status.to_i]
      errors.add(:status, "Estado no existe" )
    end
  end

  def monto_current
    number_to_currency(self.monto, :separator => ",", :delimiter => ".", :precision => 0 )
  end

  # def is_possible
  #   url = company.mainurl
  #   if status == STATUS_ID[:aprobado]
  #     @operation = "PRCVC";
  #   else
  #     @operation = "CHKVC";
  #   end
  #   response = RestClient.post @url, :OPERATION => @operation, :PERNR => employee.identification, :SUBTY => tipo, :BEGDA =>  "#{start_date.strftime("%F")}", :ENDDA => "#{end_date.strftime("%F")}"
  #   @code = response.code
  #   @headers = response.headers
  #   @description = response.description
  #   @llamada = response.to_str
  #   case @code
  #     when 200
  #       p "It worked !"
  #     when 423
  #       raise SomeCustomExceptionIfYouWant
  #       errors.add(:status, e)
  #     else
  #       response.return!(request, result, &block)
  #       errors.add(:status, e)
  #   end
  # rescue => e
  #   @error  = e.response
  #   errors.add(:status, @error)
  # end

end
