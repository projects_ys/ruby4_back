class VarianceAnalysis < ActiveRecord::Base
  belongs_to :employee
  belongs_to :position_variance, class_name: 'Position', foreign_key: :position, primary_key: :id_position
  belongs_to :position_boss_variance, class_name: 'Position', foreign_key: :position_boss, primary_key: :id_position
end
