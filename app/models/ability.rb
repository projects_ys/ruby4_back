class Ability
  include CanCan::Ability

  #Fecha de Ingreso
  def show_input_date_employee user
    return user.company.show_labor || user.company.show_birthday || user.company.show_new_employees
  end

  def show_certificates user
    return user.company.show_certificates_labor || user.company.show_certificates_vacations || user.company.show_certificates_payroll || user.company.show_certificates_income
  end

  def extra_hour_approver user
    user.employee.hoex_approver != '00000000' &&  user.employee.hoex_approver != nil
  end

  def inhability_approver user
    user.employee.inca_approver != '00000000' &&  user.employee.inca_approver != nil
  end

  def severance_approver user
    user.employee.cesa_approver != '00000000' &&  user.employee.cesa_approver != nil
  end

  def licensece_approver user
    user.employee.perm_approver != '00000000' &&  user.employee.perm_approver != nil
  end

  def loan_approver user
    user.employee.pres_approver != '00000000' &&  user.employee.pres_approver != nil
  end

  def vacation_approver user
    user.employee.vaca_approver != '00000000' &&  user.employee.vaca_approver != nil
  end

  def compensatory_vacation_approver user
    user.employee.vccp_approver != '00000000' &&  user.employee.vccp_approver != nil
  end

  #metodo recursivo capaza de determinar si es padre o no
  def authorize_parent(user,employee)
    if user.employee.pernr == employee.pernr || (user.employee.employee_boss.present? && user.employee.employee_boss.pernr == employee.pernr)
      true
    else
      boss = employee.employee_boss
      unless boss.nil?
        if boss.pernr == user.employee.pernr
          true
        else
          authorize_parent(user,boss)
        end
      else
        false
      end
    end
  end

  def initialize(user,second_param = nil)
    ### Dashboard ###
    ##Solicitudes
    if user.present?
      unless second_param.nil?
        if self.authorize_parent(user,second_param)
          can :index,:parent_organizate
        end
      end

      if user.company.make_request == true
        can :read, RequestsEmployee, :pernr => user.employee.pernr
        can [:create,:destroy], RequestsEmployee
      end
      ##Aprobaciones
      if user.company.make_approvals == true
        if user.is_approver?
          can [:read,:update], RequestsEmployee,:first_approver_assigned => user.employee.pernr
          can [:read,:update], RequestsEmployee,:second_approver_assigned => user.employee.pernr
        end
      end
       #Gestor de Contenido
      if user.company.show_articles
        can [:read,:create,:update], ViewArticle
        can :read, Article
        can :read, CustomList
        can [:read,:create], CommentsArticle
        can :destroy, CommentsArticle, :employee_id => user.employee_id
        #can :create, ViewArticle
        if user.employee.new_cont == 'true'
          can :manage, Article, :employee_id => user.employee.pernr
        end
      end
      if self.show_input_date_employee user
        can :manage, Employee
      end
      if self.show_certificates user
        #can :read, Employee
        can :read, FileEmployee
      end

      ### Solicitudes ###
      #Anticipo de cesantías (severance)
      if self.severance_approver(user) && user.labor_relation.scesa == 'X' && user.company.show_scesa
        can :manage, Severance
      end
      #Permisos (licenses)
      if self.licensece_approver(user) && user.labor_relation.sperm == 'X' &&  user.company.show_licenses
        can :manage, LicenseRequirement
      end
      #Prestamos (loans)
      if self.loan_approver(user) && user.labor_relation.spres == 'X' && user.company.show_loans
        can :manage, Loan
      end
      #Vacaciones (vacations)
      if self.vacation_approver(user) && user.labor_relation.svaca == 'X' && user.company.show_vacations
        can :manage, VacationRequirement
      end
      #Vacaciones compensadas (compensatory_vacations)
      if self.compensatory_vacation_approver(user) && user.labor_relation.svccp == 'X' && user.company.show_vacations_c
        can :manage, CompensatoryVacation
      end

      ### Registros ###
      #Horas extras (extras)
      if self.extra_hour_approver(user) && user.labor_relation.shoex == 'X' && user.company.show_hoex
        can :manage, ExtraRequirement
      end
      #Incapacidades (inhabilities)
      if self.inhability_approver(user) && user.labor_relation.sinca == 'X' && user.company.show_inhabilities
        can :manage, InhabilityRequirement
      end

      if user.employee.see_rpgen == 'true'
        can :read, Employee
        can :read, Absence
        can :read, PaymentsDeduction
        can :read, Position
        can :read, HistoricalPosition
        can :read, EmployeeTurnover
        can :read, VarianceAnalysis
      end
    end
  end

end
