class Position < ActiveRecord::Base
  has_one :position_boos, class_name: 'Employee', foreign_key: :boss, primary_key: :id_position
  has_one :employee, class_name: 'Employee', foreign_key: :id_posicion, primary_key: :id_position
  has_one :position_history, class_name: 'HistoricalPosition', foreign_key: :position, primary_key: :id_position
  has_one :position_boss_history, class_name: 'HistoricalPosition', foreign_key: :position_boss, primary_key: :id_position
  has_one :position_variance, class_name: 'VarianceAnalysis', foreign_key: :position, primary_key: :id_position
  has_one :position_boss_variance, class_name: 'VarianceAnalysis', foreign_key: :position_boss, primary_key: :id_position
  belongs_to :company
  #organigram manager
  has_many :subordinates_all, class_name:'Position', foreign_key: :position_boss, primary_key: :id_position
end

