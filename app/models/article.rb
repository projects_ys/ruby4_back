class Article < ActiveRecord::Base

  #validates_presence_of :titulo, :imagen, :cuerpo, :ini, :category, :employee_id, :company_id
  validates_presence_of :titulo, :cuerpo, :employee_id, :company_id, :category

  validates :cuerpo, length: { maximum: 5242880 }

  belongs_to :employee,->(article) { where({ company_id: article.company_id}) },:class_name => "Employee",:foreign_key => :employee_id,:primary_key => :pernr
  belongs_to :company
  has_many :view_articles
  has_many :comments_articles
  belongs_to :my_category,->(article) { where({ company_id: article.company_id}) },:class_name => "CustomList",:foreign_key => :category,:primary_key => :identifier
  mount_uploader :imagen, ArticleRequirementUploader

  def num_views
    self.view_articles.size
  end

  def num_comments
    self.comments_articles.size
  end

end
