class CustomList < ActiveRecord::Base
  #validates :identifier, uniqueness: true
  has_many  :articles, -> (categories) { where({ company_id: categories.company_id}) }, :class_name => "Article",:foreign_key => :category,:primary_key => :identifier
  belongs_to :requests_employee,-> (status) { where({ company_id: status.company_id}) }, :class_name => "CustomList",:foreign_key => :status_id,:primary_key => :identifier
  belongs_to :company
end
