class IndebtednessLevel < ActiveRecord::Base
  crypt_keeper  :deven,:deduc,:encryptor => :aes_new, :key => Rails.application.secrets.secret_key_encryption, salt: 'salt', :encoding => 'UTF-8'
  belongs_to :employee, foreign_key: :employee_id, class_name: 'Employee'

  def field_0
    DataTableHelper.format_date(self.payroll_date)
  end
  def field_1
    self.payroll_concept_txt
  end
  def field_2
    self.deven.to_f
  end
  def field_3
    self.deduc.to_f
  end
end
