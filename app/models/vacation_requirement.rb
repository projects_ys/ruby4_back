class VacationRequirement < ActiveRecord::Base

  STATUS_ID = { pendiente: 1, aprobado: 2, denegado: 3 }
  STATUS_NAME = { 1 => 'Pendiente', 2 => 'Aprobado', 3 => 'Denegado'}

  validates_presence_of :start_date, :end_date, :more_text, :status, :employee_id, :tipo, :approver_id_posicion, :company_id

  belongs_to :employee
  belongs_to :company
  belongs_to :approver, class_name: 'Employee', foreign_key: :id_posicion, primary_key: :approver_id_posicion

  mount_uploader :attachment, VacationRequirementUploader

  validate :exists_status, :is_possible

  def exists_status
    unless STATUS_NAME[self.status.to_i]
      errors.add(:status, "Estado del prestamo no existe" )
    end
  end

  def dias
    (end_date-start_date).to_i
  end
  
  def is_possible  
  # CHEQUEAR
  @url = employee.company.mainurl
  if status.to_s == STATUS_ID[:aprobado].to_s
    @operation = "PRCVC";
  else
    @operation = "CHKVC";
  end
  response = RestClient.post @url, :OPERATION => @operation, :PERNR => employee.pernr, :SUBTY => tipo, :BEGDA =>  "#{start_date.strftime("%F")}", :ENDDA => "#{end_date.strftime("%F")}"
  @code = response.code
  @headers = response.headers
  @description = response.description
  @llamada = response.to_str
  case @code
    when 200
      p "It worked !"
    when 423
      raise SomeCustomExceptionIfYouWant
      errors.add(:status, e)
    else
      response.return!(request, result, &block)
      errors.add(:status, e)
    end
    rescue => e
      error  = e.response.force_encoding("ISO-8859-1").encode("UTF-8")
      errors.add(:status, error)
  end
  
end
