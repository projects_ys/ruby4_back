class Loan < ActiveRecord::Base

  include ActionView::Helpers::NumberHelper

  STATUS_ID = { pendiente: 1, aprobado: 2, denegado: 3 }
  STATUS_NAME = { 1 => 'Pendiente', 2 => 'Aprobado', 3 => 'Denegado'}

  belongs_to :employee
  belongs_to :company

  belongs_to :approver, foreign_key: :approver_id_posicion,  primary_key:  :id_posicion, class_name: 'Employee'
  belongs_to :approved, foreign_key: :approved_by_identification,  primary_key:  :pernr, class_name: 'Employee'

  validates_presence_of :employee_id, :company_id, :subty, :monto, :status, :approver_id_posicion
  validate :exists_status, :approver_valid

  def exists_status
    unless STATUS_NAME[self.status.to_i]
      errors.add(:status, "Estatus del prestamo no existe" )
    end
  end

  def approver_valid
    if STATUS_ID[:aprobado] == status || STATUS_ID[:denegado] == status
      unless employee.pres_approver == approver_id_posicion
        errors.add(:approver_id_posicion, "aprovador impedido" )
      end
    end
  end

  def monto_current
    number_to_currency(self.monto, :separator => ",", :delimiter => ".", :precision => 0 )
  end


  # validate :is_possible

  # def is_possible
  #   @url =  company.mainurl
  #   p @url
  #   if approved == true
  #     response = RestClient.post @url, :OPERATION => 'UPDDM', :SUBTY => subty,  :PERNR => employee.identification
  #     @code = response.code
  #     @headers = response.headers
  #     @description = response.description
  #     @llamada = response.to_str
  #     case @code
  #       when 200
  #         p "It worked !"
  #         p @code
  #         p @headers
  #         p @description
  #         p @llamada
  #       when 423
  #         e = raise SomeCustomExceptionIfYouWant
  #         errors.add(:status, e)
  #       else
  #         e = response.return!(request, result, &block)
  #         errors.add(:status, e)
  #     end
  #
  #   end
  # rescue => e
  #   @error  = e.response
  #   errors.add(:status, @error)
  # end

end
