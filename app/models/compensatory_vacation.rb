class CompensatoryVacation < ActiveRecord::Base

  STATUS_ID = { pendiente: 1, aprobado: 2, denegado: 3 }
  STATUS_NAME = { 1 => 'Pendiente', 2 => 'Aprobado', 3 => 'Denegado'}

  belongs_to :employee
  belongs_to :company

  belongs_to :approver, foreign_key: :approver_id_posicion,  primary_key:  :id_posicion, class_name: 'Employee'
  belongs_to :approved, foreign_key: :approved_by_identification,  primary_key:  :pernr, class_name: 'Employee'

  validates_presence_of :employee_id, :company_id, :at_date, :reason, :tipo, :status, :approver_id_posicion
  validate :exists_status, :is_possible

  mount_uploader :attachment, VacationRequirementUploader

  def exists_status
    unless STATUS_NAME[self.status.to_i]
      errors.add(:status, "Estatus del prestamo no existe" )
    end
  end

  def is_possible
    url = self.company.mainurl
    puts status
    puts STATUS_ID[:aprobado]
    if status.to_s == STATUS_ID[:aprobado].to_s
      puts "PRVCVM"
      operation = "PRCVCM";
    else
      puts "CHKVCM"
      operation = "CHKVCM";
    end
    # PERNR / BEGDA / NUMBR / NOPAY
    response = RestClient.post url, :OPERATION => operation, :PERNR => self.employee.pernr, :SUBTY => self.tipo, :BEGDA =>  "#{Time.zone.now.strftime("%F")}", :NUMBR => self.at_date, :NOPAY => self.pay ? 'X':''
    puts response.inspect
    @code = response.code
    @headers = response.headers
    @description = response.description
    @llamada = response.to_str
    case response.code
      when 200
        p "It worked !"
      when 423
        raise SomeCustomExceptionIfYouWant
        errors.add(:status, e)
      else
        response.return!(request, result, &block)
        errors.add(:status, e)
    end
  rescue => e
    puts e.inspect
    error  = e.response.force_encoding("ISO-8859-1").encode("UTF-8")# ASCII-8BIT
    errors.add(:status, error)
  end


end
