class Incapacity < ActiveRecord::Base
  belongs_to :employee, foreign_key: :employee_id, class_name: 'Employee'
  def field_0
    self.text_incapacity
  end
  def field_1
    DataTableHelper.format_date self.begda
  end
  def field_2
    DataTableHelper.format_date self.endda
  end
  def field_3
    self.days_working_incapacity
  end
  def field_4
    self.days_taken_incapacity
  end
end
