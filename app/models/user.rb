class User < ActiveRecord::Base
  # Include default devise modules.
  devise :database_authenticatable, :registerable,
          :recoverable, :rememberable, :trackable, :validatable, :lockable, :omniauthable


  include DeviseTokenAuth::Concerns::User
  
  has_one :employee, dependent: :destroy
  belongs_to :company
  
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable


  def labor_relation
    begin
      self.employee.labor_relation
    rescue => e
      puts e.inspect
    end
  end

  #timeout
  def timeout_in
    minutes = self.company.timeout.to_i
    unless minutes.nil?
      minutes.minutes
    else
      10.minutes
    end
  end

  def employee_info
    begin
      info_employee = self.employee.employee_info
      info_decrypt = {
          :datos_personales => eval(info_employee.datos_personales.to_s),
          :datos_contacto => eval(info_employee.datos_contacto.to_s),
          :datos_familiares => eval(info_employee.datos_familiares.to_s),
          :datos_beneficiarios => eval(info_employee.datos_beneficiarios.to_s),
          :datos_estudios => eval(info_employee.datos_estudios.to_s),
          :datos_empresariales => eval(info_employee.datos_empresariales.to_s),
          :datos_bancarios => eval(info_employee.datos_bancarios.to_s),
          :datos_seguridadsocial => eval(info_employee.datos_seguridadsocial.to_s),
          :datos_impuestos => eval(info_employee.datos_impuestos.to_s),
          :datos_retefuente => eval(info_employee.datos_retefuente.to_s)
      }
    rescue => e
      puts e.inspect
    end
  end

  def favorite_employees
    begin
      self.employee.favorite_employees_user.as_json(:only => [], :methods => [:id_favorite,:user_identification,:employee_identification], include: [:employee ])
    rescue => e
      puts e.inspect
    end
  end

  def files
    begin
      self.employee.file_employees
    rescue => e
      puts e.inspect
    end
  end

  def files_last_months
    begin
      self.employee.file_employees.order('name_iden DESC')
    rescue => e
      if Rails.env.development?
        puts e.inspect
      end
    end
  end

  def indebtedness_levels
    begin
      self.employee.indebtedness_levels.order(payroll_date: :desc,deven: :desc, deduc: :desc)
    rescue => e
      if Rails.env.development?
        puts e.inspect
      end
    end
  end

  def embargoes
    begin
      self.employee.embargos
    rescue => e
      if Rails.env.development?
        puts e.inspect
      end
    end
  end

  def loan_records
    begin
      self.employee.loan_records
    rescue => e
      if Rails.env.development?
        puts e.inspect
      end
    end
  end

  def vacation_records
    begin
      self.employee.vacation_records
    rescue => e
      if Rails.env.development?
        puts e.inspect
      end
    end
  end

  def file_types
    begin
      self.employee.company.pdf_types
    rescue => e
      if Rails.env.development?
        puts e.inspect
      end
    end
  end

  def saldos
    begin
      saldos_employee = self.employee.saldo
      saldos_decrypt = {
          :burks => saldos_employee.bukrs,
          :company_id => saldos_employee.company_id,
          :created_at => saldos_employee.created_at,
          :employee_id => saldos_employee.employee_id,
          :id => saldos_employee.id,
          :intsaldo => saldos_employee.intsaldo,
          :intvcausado => saldos_employee.intvcausado,
          :pernr => saldos_employee.pernr,
          :saldo => saldos_employee.saldo,
          :t_cesantias => eval(saldos_employee.t_cesantias.to_s),
          :t_endeudamiento => eval(saldos_employee.t_endeudamiento.to_s),
          :t_intcesantias => eval(saldos_employee.t_intcesantias.to_s),
          :totdeducciones => saldos_employee.totdeducciones,
          :totdevengos => saldos_employee.totdevengos,
          :updated_at => saldos_employee.updated_at,
          :vcausado => saldos_employee.vcausado
      }
    rescue => e
      if Rails.env.development?
        puts e.inspect
      end
    end
  end
  #
  def company_type
    begin
      self.company.type
    rescue => e
      if Rails.env.development?
        puts e.inspect
      end
    end
  end
  #Vacation's for employee id
  def vacation
    begin
      self.employee.vacation
    rescue => e
      if Rails.env.development?
        puts e.inspect
      end
    end
  end

  def labor
    begin
      self.employee.company.labors
    rescue => e
      if Rails.env.development?
        puts e.inspect
      end
    end
  end
  #permissions form employee
  def permissions
    begin
      self.employee.permissions
    rescue => e
      if Rails.env.development?
        puts e.inspect
      end
    end
  end

  def compensatory_vacation_records
    begin
      self.employee.compensatory_vacation_records
    rescue => e
      if Rails.env.development?
        puts e.inspect
      end
    end
  end

  def extra_hour_records
    begin
      self.employee.extra_hour_records
    rescue => e
      if Rails.env.development?
        puts e.inspect
      end
    end
  end

  #incapcieties from employees
  def incapacities
    begin
      self.employee.incapacities
    rescue => e
      if Rails.env.development?
        puts e.inspect
      end
    end
  end

  def vacation_balance_records
    begin
      self.employee.vacation_balance_records
    rescue => e
      if Rails.env.development?
        puts e.inspect
      end
    end
  end

  def failed_attempt_user
    begin
      self.failed_attempts ||= 0
      self.failed_attempts += 1
      if attempts_exceeded?
        lock_access! unless access_locked?
      else
        save(validate: false)
      end
    rescue => e
      if Rails.env.development?
        puts e.inspect
      end
    end
  end

  #aprover

  def number_pending_approval
    number_first_approver = self.employee.first_approver_requests_received.where(:status_id => 'ReqEmpStaPenAproJef' ).size
    number_second_approver = self.employee.second_approver_requests_received.where(:status_id => 'ReqEmpStaPenRH' ).size
    if number_first_approver.present? || number_second_approver.present?
      total_pengings_approvals = number_first_approver + number_second_approver
    else
      total_pengings_approvals = nil
    end
    total_pengings_approvals
  end

  def is_approver?
    number_first_approver = self.employee.first_approver_requests_received.size
    number_second_approver = self.employee.second_approver_requests_received.size
    if number_first_approver > 0 || number_second_approver > 0
      true
    else
      false
    end
  end


end
