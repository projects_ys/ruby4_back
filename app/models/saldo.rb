class Saldo < ActiveRecord::Base
  serialize :t_endeudamiento
  serialize :t_intcesantias
  serialize :t_cesantias
  crypt_keeper :saldo,:vcausado,:intsaldo,:intvcausado,:totdevengos,:totdeducciones,:t_endeudamiento,:t_intcesantias,:t_cesantias, :encryptor => :aes_new, :key => Rails.application.secrets.secret_key_encryption, salt: 'salt', :encoding => 'UTF-8'
  belongs_to :employee
end
