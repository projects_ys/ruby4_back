class VacationRecord < ActiveRecord::Base
  belongs_to :employee
  def field_0
    self.atext
  end
  def field_1
    DataTableHelper.format_date self.begda
  end
  def field_2
    DataTableHelper.format_date self.endda
  end
  def field_3
    self.abwtg
  end
  def field_4
    self.kaltg
  end
end
