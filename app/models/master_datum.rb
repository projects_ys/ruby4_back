class MasterDatum < ActiveRecord::Base
  serialize :countries
  serialize :state
  serialize :family
  serialize :institutes
  serialize :measurement_units
  serialize :marital_status
  serialize :formations
  serialize :titles
  serialize :specialties
  serialize :labor_relations
  serialize :banks
  serialize :nationality
  serialize :provinces
  serialize :gender
  serialize :tax_procedure
  serialize :account_types
  belongs_to :company
end
