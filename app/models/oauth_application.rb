class OauthApplication < ActiveRecord::Base
  #has_many :access_grants, dependent: :destroy, class_name: 'Doorkeeper::AccessGrant'
  #has_many :access_tokens, dependent: :destroy, class_name: 'Doorkeeper::AccessToken'

  # validates :name, :secret, :uid, presence: true
  # validates :uid, uniqueness: true
  # validates :redirect_uri, redirect_uri: true

  #before_validation :generate_uid, :generate_secret, on: :create
  LDAP_NAME = "Login LDAP"

  belongs_to :company

  # def generate_secret
  #   if secret.blank?
  #     self.secret = Doorkeeper::OAuth::Helpers::UniqueToken.generate
  #   end
  # end
  #
  # def generate_uid
  #   if uid.blank?
  #     self.uid = Doorkeeper::OAuth::Helpers::UniqueToken.generate
  #   end
  # end
end