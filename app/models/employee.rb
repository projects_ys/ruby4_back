class Employee < ActiveRecord::Base

  has_one :vacation

  belongs_to :user
  belongs_to :company

  attr_accessor :short_name

  has_many :articles,->(employee) { where({ company_id: employee.company_id}) },:class_name => "Article",:foreign_key => :employee_id,:primary_key => :pernr
  has_many :loans
  has_many :severances
  has_many :comments_articles
  has_many :vacation_requirements, dependent: :destroy
  has_many :inhability_requirements, dependent: :destroy
  has_many :extra_requirements, dependent: :destroy
  has_many :license_requirements, dependent: :destroy
  has_many :compensatory_vacations
  has_many :infos
  has_one :employee_info, dependent: :destroy
  has_one :certificate_work, dependent: :destroy
  has_one :saldo, dependent: :destroy

  #Solicitudes
  belongs_to :group_employees_request,->(employee) { where({ company_id: employee.company_id}) },:class_name => "GroupEmployeesRequest",:foreign_key => :group_request,:primary_key => :identifier_group
  has_many :calendar_breaks,
                          ->(employee) { where({company_id: employee.company_id,grouper:employee.grouper_calendar })},
                          :class_name =>  "EmployeeCalendarBreak",
                          :foreign_key => :horary,
                          :primary_key => :horary_calendar
  has_many :requests_employees,->(employee) { where({ company_id: employee.company_id}) },:class_name => "RequestsEmployee",:foreign_key => :pernr,:primary_key => :pernr
  has_many :first_approver_requests_received,->(employee) { where({ company_id: employee.company_id}) },:class_name => "RequestsEmployee",:foreign_key => :first_approver_assigned,:primary_key => :pernr
  has_many :second_approver_requests_received,->(employee) { where({ company_id: employee.company_id}) },:class_name => "RequestsEmployee",:foreign_key => :second_approver_assigned,:primary_key => :pernr

  #has_many :favorite_employees_user, :foreign_key => :user_identification, primary_key: :identification, class_name: 'FavoriteEmployee'
  #Favoritos V1
  #has_many :favorite_employees_user, :foreign_key => :employee_id, primary_key: :id, class_name: 'FavoriteEmployee'
  #Favoritos V2
  has_many :favorite_employees_user,->(employee) { where({ company_id: employee.company_id}) }, :foreign_key => :employee_id, primary_key: :pernr, class_name: 'FavoriteEmployee'

  has_many :favorite_info,->(employee) { where({ company_id: employee.company_id}) }, :foreign_key => :favorite_employee, primary_key: :pernr, class_name: 'FavoriteEmployee'
  #has_many :file_employees, :foreign_key => :employee_identification, primary_key: :identification, class_name: 'FileEmployee'
  has_many :file_employees
  has_many :indebtedness_levels
  has_many :embargos
  has_many :loan_records
  has_many :vacation_records
  has_many :compensatory_vacation_records
  has_many :extra_hour_records
  has_many :vacation_balance_records,->(employee) { where({ company_id: employee.company_id}) },:class_name => "VacationBalanceRecord",:foreign_key => :employee_id,:primary_key => :pernr
  has_many :types_requests_records,through: :vacation_balance_records
  #v1
  has_many :last_view_articles, :foreign_key => :employee_id, primary_key: :pernr, class_name: 'LastViewArticle'
  #v2
  has_many :view_articles

  #Permisos e incapacidades
  has_many :permissions
  has_many :incapacities
  #MANAGE REPORTS
  #Rotations (Staff_turnover)
  has_many :employee_turnovers
  #Historical positions
  has_many :historical_positions
  #variance analysis
  has_many :variance_analysis
  #absences
  has_many :absences
  #payments_deduction
  has_many :payments_deductions

  # Jerarquía
  belongs_to :employee_boss, class_name: 'Employee', foreign_key: :boss, primary_key: :id_posicion
  has_many :subordinates, class_name: 'Employee', foreign_key: :boss, primary_key: :id_posicion
  has_many :fellows, class_name: 'Employee', foreign_key: :boss, primary_key: :boss
  belongs_to :my_position, class_name: 'Position', foreign_key: :id_posicion, primary_key: :id_position
  belongs_to :position_boss, class_name: 'Position', foreign_key: :boss, primary_key: :id_position


  # approvals
  has_many :approvals_vacation_requirements, class_name: 'VacationRequirement', foreign_key: :approver_id_posicion, primary_key: :id_posicion
  has_many :approvals_compensatory_vacations, class_name: 'CompensatoryVacation', foreign_key: :approver_id_posicion, primary_key: :id_posicion
  has_many :approvals_extra_requirements, class_name: 'ExtraRequirement', foreign_key: :approver_id_posicion, primary_key: :id_posicion
  has_many :approvals_inhability_requirements, class_name: 'InhabilityRequirement', foreign_key: :approver_id_posicion, primary_key: :id_posicion
  has_many :approvals_license_requirements, class_name: 'LicenseRequirement', foreign_key: :approver_id_posicion, primary_key: :id_posicion
  has_many :approvals_info_requirements, class_name: 'Info', foreign_key: :approver_id_posicion, primary_key: :id_posicion
  has_many :approvals_severances, class_name: 'Severance', foreign_key: :approver_id_posicion, primary_key: :id_posicion

  #Crypt phone
  crypt_keeper :phone,:encryptor => :aes_new, :key => Rails.application.secrets.secret_key_encryption, salt: 'salt', :encoding => 'UTF-8'
  serialize :vacations, Array
  serialize :volpago, Array

  mount_uploader :image, EmployeeImageUploader

  def labor_relation
    Labor.find_by_company_id_and_ansvh self.company_id, self.rel_lab
  end

  def unseen_articles
    # Article.joins(:view_articles).where('"view_articles"."employee_id" <> ?', self.identification )
    views = ViewArticle.where( :employee_id =>  self.pernr )
    if views.count > 0
      self.company.articles.where('id NOT IN (?)', views.pluck(:article_id) ).where( publicado: true )
    else
      self.company.articles.where( publicado: true )
    end
  end

  def age
    now = Time.now.utc.to_date
    now.year - fecha_nac.year - (fecha_nac.to_date.change(:year => now.year) > now ? 1 : 0)
  end
  
  def antiguedad
    now = Time.now.utc.to_date
    now.year - fecha_ingreso.year - (fecha_ingreso.to_date.change(:year => now.year) > now ? 1 : 0)
  end

  def number_notification
    last_view = self.last_view_articles.last
    created_at = Time.zone.now - 1.month
    if last_view
      created_at = last_view.created_at
    end
    a = self.unseen_articles.count

    vr = self.approvals_vacation_requirements.where(status: VacationRequirement::STATUS_ID[:pendiente]).count
    vc = self.approvals_compensatory_vacations.where(status: CompensatoryVacation::STATUS_ID[:pendiente]).count
    er = self.approvals_extra_requirements.where(status: ExtraRequirement::STATUS_ID[:pendiente]).count
    ir = self.approvals_inhability_requirements.where(status: InhabilityRequirement::STATUS_ID[:pendiente]).count
    lr = self.approvals_license_requirements.where(status: LicenseRequirement::STATUS_ID[:pendiente]).count
    dm = self.approvals_info_requirements.where(status: Info::STATUS_ID[:pendiente]).count
    sv = self.approvals_severances.where(status: Severance::STATUS_ID[:pendiente]).count

    {  articles: a, vacation: vr, compensatory_vacation: vc,extra: er, inhability: ir, license: lr, info: dm , severances: sv, count: ( a + vr + vc + er + ir + lr + dm + sv )  }

  end

  def short_name
    new_short_name = ""
    if self.name.present?
      new_short_name += self.name.capitalize + " "
    end
    if self.lastname.present?
      new_short_name += self.lastname.capitalize
    end
    self.short_name = new_short_name
  end

  #servicios gerenciales

  def amount
    today = Date.today
    current_position = self.historical_positions.find_by("begda <= ? AND endda >= ?",today,today)
    if current_position.present?
      current_position.amount_accrued.to_s
    else
      0
    end
  end

  def type
    I18n.t('api.v2.employees.subordinates.occupation')
  end

  #Metodo contador de articulos
  def has_articles
    count_articles = self.articles.size
    if count_articles > 0
      true
    else
      false
    end
  end

  #Metodo de consulta favoritos /  no favoritos

  def self.consult_employees_fav(type_employee,subquery_favorites)
    case type_employee
      when 'favorites'
        where(:pernr =>subquery_favorites)
      when 'nofavorites'
        where.not(:pernr =>subquery_favorites)
      else

    end
  end

  #Metodo para el tipo de relaciones en organigrama

  def relationship
    relationship = [0,0,0]
    boss = self.employee_boss
    unless boss.nil?
      relationship[0] = 1
      number_equal_hierarchy = boss.subordinates.where.not(:pernr => self.pernr).size
      if number_equal_hierarchy > 0
        relationship[1] = 1
      end
    end
    if self.subordinates.present?
      relationship[2] = 1
    end
    relationship.join
  end

  def total_children
    self.subordinates.size
  end
  def children(page = false)
    if page === false
      page = 1
    end
    childrens = self.subordinates.order(name: :asc,lastname: :asc).paginate(:page => page,:per_page => 5).as_json(only:[:pernr,:posicion,:email,:image],methods:[:short_name,:relationship])
    childrens.each do |children|
      children.merge!({
          pernr_parent: self.pernr,
          page: page
      })
    end

    childrens
  end

  def siblings(page)
    boss = self.employee_boss
    unless boss.nil?
      siblings = boss.subordinates.where.not(:pernr => self.pernr).as_json(only:[:pernr,:posicion,:email,:image],methods:[:short_name,:relationship])
    end
    siblings
  end

end
