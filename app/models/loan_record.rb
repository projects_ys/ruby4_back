class LoanRecord < ActiveRecord::Base
  crypt_keeper  :lolci,:lolrp,:lolim,:totcu,:lollb,:totsa,:anrte,:darbt,:encryptor => :aes_new, :key => Rails.application.secrets.secret_key_encryption, salt: 'salt', :encoding => 'UTF-8'
  belongs_to :employee
  def field_0
    DataTableHelper.format_date self.fpend
  end
  def field_1
    self.stext
  end
  def field_2
    self.totcu.to_f
  end
  def field_3
    self.lolim
  end
  def field_4
    self.zsoll
  end
  def field_5
    self.totsa.to_f
  end
end
