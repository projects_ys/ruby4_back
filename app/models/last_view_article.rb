class LastViewArticle < ActiveRecord::Base

  validates_presence_of :employee_id

  belongs_to :employee, foreign_key: :employee_id, primary_key: :pernr, class_name: 'Employee'

end
