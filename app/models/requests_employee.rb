class RequestsEmployee < ActiveRecord::Base
  include RequestsConcern
  include ActiveModel::Serializers::JSON
  has_one :status, -> (request) { where({ company_id: request.employee.company_id}) }, :class_name => "CustomList",:foreign_key => :identifier,:primary_key => :status_id
  belongs_to :first_approver, -> (request) { where({ company_id: request.company_id}) }, :class_name => "Employee",:foreign_key => :first_approver_assigned,:primary_key => :pernr
  belongs_to :second_approver, -> (request) { where({ company_id: request.company_id}) }, :class_name => "Employee",:foreign_key => :second_approver_assigned,:primary_key => :pernr
  belongs_to :company
  belongs_to :employee,->(request) { where({ company_id: request.company_id}) },:class_name => "Employee",:foreign_key => :pernr,:primary_key => :pernr
  belongs_to :types_requests,->(request) { where({ company_id: request.company_id}) },:class_name => "TypesRequestsRecord",:foreign_key => :types_requests_record_id,:primary_key => :form
  validates_uniqueness_of :pernr, scope: [:date_begin, :date_end,:types_requests_record_id,:days_request,:status_id], conditions: -> {where.not(status_id: 'ReqEmpStaAprRH')}
  validates_presence_of :types_requests_record_id,:pernr,:first_approver_assigned,:second_approver_assigned, :on => :create
  validates_uniqueness_of :pernr, scope: [:date_begin, :date_end,:types_requests_record_id,:days_request,:status_id], conditions: -> {where.not(status_id: 'ReqEmpStaAprRH')}
  before_create :validate_request_for_employee
  validate :validate_basic,:validate_contingent, :on => :create, :unless => "types_requests.nil?"
  validate :allow_update, :on => :update
  validate :allow_delete, :on => :delete

  mount_uploader :file_support, RequerimentFileSupportUploader

  private
  def allow_update
    puts self.status_id_changed?
    if (self.status_id == 'ReqEmpStaPenRH' || self.status_id == 'ReqEmpStaCanJef') && self.status_id_changed?
      true
    else
      errors.add(:first_approver_assigned,:not_authorized)
      false
    end
  end

  def allow_delete
    if self.date_first_approval.nil? && self.date_second_approval.nil?
      true
    else
      errors.add(:id,:not_delete)
      false
    end
  end

  def validate_request_for_employee
    query = self.employee.group_employees_request.types_request_for_groups.where(:type_request => self.types_requests_record_id)
    if query.present?
      true
    else
      errors.add(:type_request,:not_authorized)
      false
    end
  end

  def validate_basic
    type = self.types_requests
    if type.type_request.downcase == 's'
      if self.date_end.present?
        unless self.date_begin <= self.date_end
          errors.add(:date_begin,:date_end_higher)
        end
      end
    end
  end

  def validate_contingent
    type = self.types_requests
    if type.contingent != nil
      employee = self.employee
      case type.id_activity
        when 'VACA','VCCP'
          contingent_days = available_contingent(employee,self.types_requests)

          if contingent_days < self.days_request
            errors.add(:days_request,:insufficient_days)
          else
            true
          end
      end
    else
      true
    end
  end

  def self.crossed(date_begin,date_end)
    where("(date_begin BETWEEN :date_begin AND :date_end OR date_end BETWEEN :date_begin AND :date_end)",date_begin: date_begin,date_end: date_end)
  end

  def current_approver
    status_first_approver = ['ReqEmpStaPenAproJef','ReqEmpStaCanJef']
    status_second_approver = ['ReqEmpStaPenRH','ReqEmpStaCanRH','ReqEmpStaAprRH']
    if status_first_approver.include?(self.status_id)
      self.first_approver
    elsif status_second_approver.include?(self.status_id)
      self.second_approver
    else
      nil
    end
  end

  def applicants_crossed
    if self.status_id == 'ReqEmpStaPenAproJef' || self.status_id == 'ReqEmpStaCanJef'
      if self.id_activity == 'VACA'
        self.first_approver.first_approver_requests_received
            .select(:id,:pernr,:date_begin,:date_end,:company_id,:status_id)
            .where(:types_requests_record_id => self.types_requests_record_id)
            .where.not(:id => self.id)
            .crossed(self.date_begin,self.date_end)
            .order(date_begin: :asc)
      end
    else
      nil
    end
  end


end
