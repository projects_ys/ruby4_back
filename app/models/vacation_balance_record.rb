class VacationBalanceRecord < ActiveRecord::Base
  belongs_to :employee
  belongs_to :types_requests_record,->(vacation) {where({company_id:vacation.company_id})},:class_name => "TypesRequestsRecord",:foreign_key => :subty,:primary_key => :contingent
  #has_one :type_request,->(vacation) {where({ company_id: vacation.company_id})},:class_name=>"TypesRequestsRecord",:foreign_key => :form,:primary_key => :type_request

  def field_0
    self.ktext
  end
  def field_1
    self.begda
  end
  def field_2
    self.endda
  end
  def field_3
    self.anzhl
  end
  def field_4
    self.kverb
  end
  def field_5
    self.anzhl - self.kverb
  end

end
