class CommentsArticle < ActiveRecord::Base
  belongs_to :employee
  belongs_to :article
  belongs_to :company
  def data_complete
    {
        id:self.id,
        comment_text:self.comment_text,
        created_at:self.created_at,
        employee_short_name:self.employee.short_name,
        employee_image:self.employee.image
    }
  end
end
