class InhabilityRequirement < ActiveRecord::Base

  STATUS_ID = { pendiente: 1, aprobado: 2, denegado: 3 }
  STATUS_NAME = { 1 => 'Pendiente', 2 => 'Aprobado', 3 => 'Denegado'}

  belongs_to :employee
  belongs_to :company

  belongs_to :approver, foreign_key: :approver_id_posicion,  primary_key:  :id_posicion, class_name: 'Employee'
  belongs_to :approved, foreign_key: :approved_by_identification,  primary_key:  :pernr, class_name: 'Employee'

  mount_uploader :attachment, LicenseRequirementUploader

  validates_presence_of :start_date, :end_date, :motivo, :status, :employee_id, :approver_id_posicion, :attachment

  validate :exists_status, :is_possible

  def exists_status
    unless STATUS_NAME[self.status.to_i]
      errors.add(:status, "Estado no existe" )
    end
  end

  def is_possible
   @url =  employee.company.mainurl
    if status.to_s == STATUS_ID[:aprobado].to_s
      @operation = "PRCIC";
    else
      @operation = "CHKIC";
    end
   response = RestClient.post @url, :OPERATION => @operation, :PERNR => employee.pernr, :SUBTY => motivo, :BEGDA =>  "#{start_date.strftime("%F")}", :ENDDA => "#{end_date.strftime("%F")}"
   @code = response.code
   @headers = response.headers
   @description = response.description
   @llamada = response.to_str
   case @code
     when 200
       p "It worked !"
       p @code
       p @headers
       p @description
       p @llamada
     when 423
       raise SomeCustomExceptionIfYouWant
       errors.add(:status, e)
     else
       response.return!(request, result, &block)
       errors.add(:status, e)
     end
     rescue => e
       @error  =  e.response.force_encoding("ISO-8859-1").encode("UTF-8")
       errors.add(:status, @error)
  end
 
end
