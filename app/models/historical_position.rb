class HistoricalPosition < ActiveRecord::Base
  crypt_keeper :amount_accrued,:encryptor => :aes_new, :key => Rails.application.secrets.secret_key_encryption, salt: 'salt', :encoding => 'UTF-8'
  belongs_to :employee
  belongs_to :position_history, class_name: 'Position', foreign_key: :position, primary_key: :id_position
  belongs_to :position_boss_history, class_name: 'Position', foreign_key: :position_boss, primary_key: :id_position
  has_many :timeline_positions, foreign_key: :position_id, primary_key: :position
end
