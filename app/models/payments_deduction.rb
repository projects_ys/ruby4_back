class PaymentsDeduction < ActiveRecord::Base
  crypt_keeper :amount,:encryptor => :aes_new, :key => Rails.application.secrets.secret_key_encryption, salt: 'salt', :encoding => 'UTF-8'
  belongs_to :employee

  #alias utilizados en los modelos de 'consultas' y 'reportes gerenciales'
  def field_0
    self.year_report
  end
  def field_1
    self.text_cumulative_concept
  end
  def field_2
    self.description_payroll_concept
  end
  def field_3
    self.amount.to_f
  end
end
