class CompensatoryVacationRecord < ActiveRecord::Base
  belongs_to :employee
  def field_0
    self.qctxt
  end
  def field_1
    DataTableHelper.format_date self.begda
  end
  def field_2
    DataTableHelper.format_date self.endda
  end
  def field_3
    self.numbr
  end
end
