class GroupEmployeesRequest < ActiveRecord::Base
  belongs_to :company
  has_many :employees,->(employee) { where({ company_id: employee.company_id}) },class_name: "Employee",:foreign_key => :group_request,:primary_key => :identifier_group
  has_many :types_request_for_groups,->(type) { where({ company_id: type.company_id}) },class_name: "TypesRequestForGroup",:foreign_key => :identifier_group,:primary_key => :identifier_group
  has_many :types_requests_records, through: :types_request_for_groups
  validates_uniqueness_of :identifier_group,scope: [:company_id]
end
