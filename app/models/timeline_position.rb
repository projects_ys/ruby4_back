class TimelinePosition < ActiveRecord::Base
  belongs_to :historical_position, :foreign_key => :position_id, :primary_key => :position
end
