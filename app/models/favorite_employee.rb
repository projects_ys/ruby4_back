class FavoriteEmployee < ActiveRecord::Base

  #Favoritos V2
  belongs_to :user,->(favorite) { where({ company_id: favorite.company_id}) }, foreign_key: :employee_id, primary_key: :pernr, class_name: 'Employee'
  belongs_to :employee,->(favorite) { where({ company_id: favorite.company_id}) }, foreign_key: :favorite_employee, primary_key: :pernr, class_name: 'Employee'
  def id_favorite
    self.id
  end
  def user_identification
    self.employee_id
  end
  def employee_identification
    self.favorite_employee
  end
end
