class TypesRequestsRecord < ActiveRecord::Base
  belongs_to :company
  has_many :requests_employees,:class_name => "RequestsEmployee",:foreign_key => :types_requests_record_id,:primary_key => :form
  has_many :group_employees_requests,:class_name => "GroupEmployeesRequest",:foreign_key => :types_requests_record_id,:primary_key => :form
  #vacations_contigent
  has_many :vacation_balance_record,->(typeReq) {where({company_id:typeReq.company_id})},:class_name=>"VacationBalanceRecord",:foreign_key => :subty,:primary_key => :contingent
  has_many :employees, through: :vacation_balance_record
  #many many groups
  has_many :types_request_for_groups,:class_name => "TypesRequestForGroup",:foreign_key => :type_request,:primary_key => :form
  has_many :group_employees_requests,through: :types_request_for_groups
end
