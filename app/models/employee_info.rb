class EmployeeInfo < ActiveRecord::Base
  serialize :datos_personales
  serialize :datos_contacto
  serialize :datos_familiares
  serialize :datos_beneficiarios
  serialize :datos_estudios
  serialize :datos_empresariales
  serialize :datos_bancarios
  serialize :datos_seguridadsocial
  serialize :datos_impuestos
  serialize :datos_retefuente
  belongs_to :employee
  crypt_keeper :datos_personales,:datos_contacto,:datos_familiares,:datos_beneficiarios,:datos_estudios,:datos_empresariales,:datos_bancarios,:datos_seguridadsocial,:datos_impuestos,:datos_retefuente, :encryptor => :aes_new, :key => Rails.application.secrets.secret_key_encryption, salt: 'salt', :encoding => 'UTF-8'
end
