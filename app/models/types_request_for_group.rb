class TypesRequestForGroup < ActiveRecord::Base
  belongs_to :group_employees_request,class_name: "GroupEmployeesRequest",:foreign_key => :identifier_group,:primary_key => :identifier_group
  belongs_to :types_requests_record,->(group) { where({ company_id: group.company_id}) },:class_name => "TypesRequestsRecord",:foreign_key => :type_request,:primary_key => :form
  belongs_to :company
  validates_uniqueness_of :type_request,scope: [:identifier_group,:company_id]
end
