class ViewArticle < ActiveRecord::Base

  validates_presence_of :employee_id, :article_id

  belongs_to :employee
  belongs_to :article

end
