class FileEmployee < ActiveRecord::Base

  mount_uploader :file, FileEmployeeUploader
  belongs_to :employee, foreign_key: :employee_id , class_name: 'Employee'
  belongs_to :company

  INCOME = 'CIYR'

  def pdf_type
    types = PdfType.where("activ = ?", self.op)
    .where("ident = ?", self.pdf_type_ident)
    .where("company_id = ?", self.company_id)
    .first
  end
end
