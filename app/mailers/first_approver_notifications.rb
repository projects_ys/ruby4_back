class FirstApproverNotifications < ApplicationMailer
  default :from => "Notificaciones <mobile@hrsolutions-co.com>"

  def new_request_received(user)
    @user = user
    mail(to: @user[:email_approver], subject: 'Tienes una nueva solicitud.')
  end
end
