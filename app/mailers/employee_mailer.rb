class EmployeeMailer < ApplicationMailer
  default :from => "Notificaciones <mobile@hrsolutions-co.com>"

  def notification_approvals employee, vacation, extra, inhability, license

    @employee = employee
    @vacation = vacation
    @extra = extra
    @inhability = inhability
    @license = license

    if Rails.env.production?
      # mail :to => employee.email, :subject => 'Aprobaciones pendientes'
      mail :to => 'hola@fabianrios.co, edgar.salcedo@hrsolutions-co.com, erika.ocando@hrsolutions-co.com', :subject => 'Aprobaciones pendientes'
    else
      mail :to => 'hola@fabianrios.co, edgar.salcedo@hrsolutions-co.com, erika.ocando@hrsolutions-co.com', :subject => 'Aprobaciones pendientes'
    end

  end

end
