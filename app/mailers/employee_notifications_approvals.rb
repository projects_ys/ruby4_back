class EmployeeNotificationsApprovals < ApplicationMailer
  default :from => "Notificaciones <mobile@hrsolutions-co.com>"
  def registered_request(user)
    @user = user
    mail(to: @user[:email], subject: 'Su solicitud ha sido registrada con éxito.')
  end

  def first_approval_update(request)
    @request = request
    mail(to: @request[:email_requesting], subject: "Su solicitud se encuenta en estado de #{@request[:status]}" )
  end

end
