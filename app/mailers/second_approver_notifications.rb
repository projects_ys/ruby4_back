class SecondApproverNotifications < ApplicationMailer
  default :from => "Notificaciones <mobile@hrsolutions-co.com>"

  def new_request_registered request
    @request = request
    mail(to: @request[:second_approver_email], subject: "Su solicitud se encuenta en estado de #{@request[:status]}" )
  end
end
