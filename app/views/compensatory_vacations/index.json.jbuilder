json.array!(@compensatory_vacations) do |compensatory_vacation|
  json.extract! compensatory_vacation, :id, :at_date, :rule, :quantity, :reason, :pay, :status, :tipo, :attachment, :employee_id
  json.url compensatory_vacation_url(compensatory_vacation, format: :json)
end
