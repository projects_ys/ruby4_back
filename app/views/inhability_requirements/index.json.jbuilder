json.array!(@inhability_requirements) do |inhability_requirement|
  json.extract! inhability_requirement, :id, :start_date, :end_date, :motivo, :status, :employee_id, :employee, :attachment
  json.url inhability_requirement_url(inhability_requirement, format: :json)
end
