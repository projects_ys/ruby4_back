json.array!(@vacations) do |vacation|
  json.extract! vacation, :id, :company_id, :available, :resumen, :detalle, :user_id, :pernr
  json.url vacation_url(vacation, format: :json)
end
