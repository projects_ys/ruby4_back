json.array!(@certificate_works) do |certificate_work|
  json.extract! certificate_work, :id, :data, :fecha, :company_id, :employee_id
  json.url certificate_work_url(certificate_work, format: :json)
end
