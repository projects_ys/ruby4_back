json.array!(@service_vacations) do |service_vacation|
  json.extract! service_vacation, :id, :attachment, :tipo, :fecha, :more_text, :status, :employee_id, :days, :approver_id_posicion, :approved_by_identification
  json.url service_vacation_url(service_vacation, format: :json)
end
