json.array!(@labors) do |labor|
  json.extract! labor, :id, :mandt, :ansvh, :scesa, :shoex, :sinca, :sperm, :spres, :svaca, :employee_id
  json.url labor_url(labor, format: :json)
end
