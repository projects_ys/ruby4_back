if @employee_info.present?
  json.extract! @employee_info, :id, :employee_id, :bukrs, :pernr, :datos_personales, :datos_contacto, :datos_familiares, :datos_beneficiarios, :datos_estudios, :datos_empresariales, :datos_bancarios, :datos_seguridadsocial, :datos_impuestos, :created_at, :updated_at, :company_id
else
  json.extract! @employee_info
  json.error "No hay datos personales asociados con este empleado"
end
