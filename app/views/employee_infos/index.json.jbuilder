json.array!(@employee_infos) do |employee_info|
  json.extract! employee_info, :id, :employee_id, :bukrs, :pernr, :datos_personales, :datos_contacto, :datos_familiares, :datos_beneficiarios, :datos_estudios, :datos_empresariales, :datos_bancarios, :datos_seguridadsocial, :datos_impuestos, :company_id
  json.url employee_info_url(employee_info, format: :json)
end
