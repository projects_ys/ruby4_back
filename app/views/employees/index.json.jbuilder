json.array!(@employees) do |employee|
  json.extract! employee, :id, :name, :second_name, :lastname, :second_lastname, :boss, :fecha_nac, :fecha_ingreso, :id_posicion, :id_unidad_org, :email, :phone, :user_id, :short_name, :identification, :image, :admin, :superadmin, :apply_reviewer, :data_reviewer, :posicion, :unidad_org, :area, :division_per, :subdivision_per, :moabw, :company, :vacations, :volpago, :rel_lab, :see_all_dm, :see_rpgen, :see_organ, :is_admin, :cesa_approver, :dams_approver, :hoex_approver, :inca_approver, :notifications, :notification_time, :perm_approver, :pres_approver, :vaca_approver
  json.url employee_url(employee, format: :json)
  json.cuantos @employees.count
end

