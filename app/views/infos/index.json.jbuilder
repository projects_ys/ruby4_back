json.array!(@infos) do |info|
  json.extract! info, :id, :pernr, :subty, :ncamp, :dcamp, :ccamp, :objid, :boss, :approved,  :comparador, :employee_id, :created_at, :where
  json.url info_url(info, format: :json)
  json.employee info.employee
end
