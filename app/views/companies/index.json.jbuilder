json.array!(@companies) do |company|
  json.extract! company, :id, :name, :logo, :mainurl, :user, :password, :banner_dashboard, :banner_cms, :banner_solicitudes, :active, :sv_cesantias, :sv_coworkers, :sv_encabezado, :sv_organigrama, :sv_endeudamiento
  json.url company_url(company, format: :json)
end

