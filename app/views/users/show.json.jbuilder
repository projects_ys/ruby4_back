json.extract! @user, :name, :email, :password, :password_confirmation, :pic, :image, :employee_id, :company_id, :employee, :vacation, :company
json.employee_info @user.employee.employee_info
json.favorite_employees @user.employee.favorite_employees_user, :employee, :user_identification, :employee_identification
json.files @user.employee.file_employees
json.saldos @user.employee.saldo
json.type @user.company.type
json.labor @labor
