json.array!(@saldos) do |saldo|
  json.extract! saldo, :id, :bukrs, :pernr, :saldo, :vcausado, :intsaldo, :intvcausado, :totdevengos, :totdeducciones, :t_cesantias, :t_intcesantias, :t_endeudamiento, :employee_id
  json.url saldo_url(saldo, format: :json)
end
