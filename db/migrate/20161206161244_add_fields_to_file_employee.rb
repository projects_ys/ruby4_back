class AddFieldsToFileEmployee < ActiveRecord::Migration
  def change
    add_column :file_employees, :notificated_at, :datetime
    add_column :companies, :notification_new_volpg, :boolean, default: false
  end
end
