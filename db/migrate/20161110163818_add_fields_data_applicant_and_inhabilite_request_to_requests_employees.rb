class AddFieldsDataApplicantAndInhabiliteRequestToRequestsEmployees < ActiveRecord::Migration
  def change
    add_column :requests_employees, :name_applicant, :string
    add_column :requests_employees, :typedoc_applicant, :string
    add_column :requests_employees, :numdoc_applicant, :string
    add_column :requests_employees, :is_active, :boolean,default: true
  end
end
