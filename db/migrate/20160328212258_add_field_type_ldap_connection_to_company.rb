class AddFieldTypeLdapConnectionToCompany < ActiveRecord::Migration
  def change
    add_column :companies, :ldap_connection, :text
  end
end
