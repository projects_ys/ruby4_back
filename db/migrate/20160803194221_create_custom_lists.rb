class CreateCustomLists < ActiveRecord::Migration
  def change
    create_table :custom_lists do |t|
      t.string :object_name
      t.string :field
      t.string :value
      t.text   :properties
      t.boolean :enabled
      t.integer :company_id

      t.timestamps null: false
    end
  end
end
