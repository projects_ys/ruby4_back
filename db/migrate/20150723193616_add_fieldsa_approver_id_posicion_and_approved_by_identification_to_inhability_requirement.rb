class AddFieldsaApproverIdPosicionAndApprovedByIdentificationToInhabilityRequirement < ActiveRecord::Migration
  def change
    add_column :inhability_requirements, :approver_id_posicion, :integer
    add_column :inhability_requirements, :approved_by_identification, :integer
  end
end
