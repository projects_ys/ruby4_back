class ChangeTypeAcnumToEmbargos < ActiveRecord::Migration
  def up
    change_table :embargos do |t|
      t.change :acnum,:text
      t.change :cedor,:text
    end
  end
end
