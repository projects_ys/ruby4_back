class CreateSaldos < ActiveRecord::Migration
  def change
    create_table :saldos do |t|
      t.string :bukrs
      t.integer :pernr
      t.text :saldo
      t.text :vcausado
      t.text :intsaldo
      t.text :intvcausado
      t.text :totdevengos
      t.text :totdeducciones
      t.text :t_cesantias
      t.text :t_intcesantias
      t.text :t_endeudamiento
      t.belongs_to :employee, index: true

      t.timestamps null: false
    end
  end
end
