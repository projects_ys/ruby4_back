class CreateFileEmployees < ActiveRecord::Migration
  def change
    create_table :file_employees do |t|
      t.integer :employee_identification
      t.string :file
      t.string :op
      t.string :archivo

      t.timestamps null: false
    end
  end
end
