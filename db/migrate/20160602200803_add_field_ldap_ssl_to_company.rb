class AddFieldLdapSslToCompany < ActiveRecord::Migration
  def change
    add_column :companies, :ldap_ssl, :boolean
  end
end
