class CreateEmployeeInfos < ActiveRecord::Migration
  def change
    create_table :employee_infos do |t|
      t.belongs_to :employee, index: true
      t.integer :company_id
      t.string :bukrs
      t.integer :pernr
      t.text :datos_personales
      t.text :datos_contacto
      t.text :datos_familiares
      t.text :datos_beneficiarios
      t.text :datos_estudios
      t.text :datos_empresariales
      t.text :datos_bancarios
      t.text :datos_seguridadsocial
      t.text :datos_impuestos

      t.timestamps null: false
    end
  end
end
