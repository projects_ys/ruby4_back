class AddPropertiesToUsers < ActiveRecord::Migration
  def change
    add_column :users, :name, :string
    add_column :users, :company_id, :string
    add_column :users, :employee_id, :string
    add_column :users, :image, :string
  end
end
