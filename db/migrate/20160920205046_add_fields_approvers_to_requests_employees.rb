class AddFieldsApproversToRequestsEmployees < ActiveRecord::Migration
  def change
    add_column :requests_employees, :first_approval_id, :integer
    add_column :requests_employees, :first_approval_position, :integer
    add_column :requests_employees, :second_approval_id, :integer
    add_column :requests_employees, :second_approval_position, :integer
    rename_column :requests_employees, :second_aprproval, :second_approval_name
    rename_column :requests_employees, :first_approval, :first_approval_name
    add_column :requests_employees, :first_approver_assigned, :integer
    add_column :requests_employees, :second_approver_assigned, :integer
  end
end
