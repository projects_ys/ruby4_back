class AddFieldOrderPriorityToCustomList < ActiveRecord::Migration
  def change
    add_column :custom_lists, :order_priority, :integer
    add_column :custom_lists, :identifier, :string
    remove_column :articles, :custom_list_id, :integer
    add_column :articles, :category, :string
  end
end
