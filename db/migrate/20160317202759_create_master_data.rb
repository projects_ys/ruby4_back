class CreateMasterData < ActiveRecord::Migration
  def change
    create_table :master_data do |t|
      t.text :countries
      t.text :state
      t.text :family
      t.text :institutes
      t.text :measurement_units
      t.text :marital_status
      t.text :formations
      t.text :titles
      t.text :specialties
      t.text :labor_relations
      t.text :banks
      t.integer :company_id

      t.timestamps null: false
    end
  end
end
