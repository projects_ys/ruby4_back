class AddFieldCompanyIdToLabors < ActiveRecord::Migration
  def change
    add_column :labors, :company_id, :integer
  end
end
