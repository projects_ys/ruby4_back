class CreateVacationBalanceRecords < ActiveRecord::Migration
  def change
    create_table :vacation_balance_records do |t|
      t.date :begda
      t.date :endda
      t.string :subty
      t.string :ktext
      t.integer :anzhl
      t.integer :kverb
      t.integer :employee_id

      t.timestamps null: false
    end
  end
end
