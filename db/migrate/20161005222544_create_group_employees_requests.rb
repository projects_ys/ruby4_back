class CreateGroupEmployeesRequests < ActiveRecord::Migration
  def change
    create_table :group_employees_requests do |t|
      t.string :identifier_group
      t.integer :company_id
      t.timestamps null: false
    end
  end
end
