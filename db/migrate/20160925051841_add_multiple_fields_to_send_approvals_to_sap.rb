class AddMultipleFieldsToSendApprovalsToSap < ActiveRecord::Migration
  def change
    add_column :employees, :second_cesa_approver, :string
    add_column :employees, :second_dams_approver, :string
    add_column :employees, :second_hoex_approver, :string
    add_column :employees, :second_inca_approver, :string
    add_column :employees, :second_perm_approver, :string
    add_column :employees, :second_pres_approver, :string
    add_column :employees, :second_vaca_approver, :string
    add_column :employees, :second_vccp_approver, :string
    add_column :requests_employees, :synch_message_sap, :string
  end
end
