class CreateVacationRecords < ActiveRecord::Migration
  def change
    create_table :vacation_records do |t|
      t.date :begda
      t.date :endda
      t.string :awart
      t.string :atext
      t.integer :abwtg
      t.integer :kaltg
      t.integer :employee_id

      t.timestamps null: false
    end
  end
end
