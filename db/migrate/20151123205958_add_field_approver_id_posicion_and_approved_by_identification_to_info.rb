class AddFieldApproverIdPosicionAndApprovedByIdentificationToInfo < ActiveRecord::Migration
  def change
    add_column :infos, :approver_id_posicion, :integer
    add_column :infos, :approved_by_identification, :integer
    add_column :infos, :company_id, :integer
    add_column :infos, :status, :string
  end
end
