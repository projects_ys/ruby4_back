class AddFieldsVerifyApproversToEmployee < ActiveRecord::Migration
  def change
    add_column :employees, :is_first_approver, :boolean, default: true
    add_column :employees, :is_second_approver, :boolean, default: true
  end
end
