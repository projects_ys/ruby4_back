class AddFieldsDaysToRequestsEmployees < ActiveRecord::Migration
  def change
    add_column :requests_employees, :days_request, :float
    add_column :types_requests_records, :contingent, :string
  end
end
