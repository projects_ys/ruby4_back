class AddFieldsImageToCompany < ActiveRecord::Migration
  def change
    add_column :companies, :logo_dashboard, :string
    add_column :companies, :logo_inside, :string
    add_column :companies, :background_login, :string
    add_column :companies, :background_header_menu, :string
  end
end
