class CreateLoanRecords < ActiveRecord::Migration
  def change
    create_table :loan_records do |t|
      t.string  :dlart
      t.string  :stext
      t.integer :objps
      t.string  :fpper
      t.date :fpbeg
      t.date :fpend
      t.integer :zsoll
      t.integer :nrdys
      t.text :lolci
      t.text :lolrp
      t.text :lolim
      t.text :totcu
      t.text :lollb
      t.text :totsa
      t.text :anrte
      t.text :darbt
      t.integer :employee_id

      t.timestamps null: false
    end
  end
end
