class AddFieldStatusToSeverance < ActiveRecord::Migration
  def change
    add_column :severances, :status, :integer
  end
end
