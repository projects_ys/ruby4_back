class AddFieldCompanyIdToInhabilityRequirements < ActiveRecord::Migration
  def change
    add_column :inhability_requirements, :company_id, :integer
  end
end
