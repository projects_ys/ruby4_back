class AddNotificationsToEmployees < ActiveRecord::Migration
  def change
    add_column :employees, :notifications, :string
    add_column :employees, :notification_time, :datetime
  end
end
