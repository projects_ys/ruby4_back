class CreateHistoricalPositions < ActiveRecord::Migration
  def change
    create_table :historical_positions do |t|
      t.date :begda
      t.date :endda
      t.integer :position
      t.integer :position_boss
      t.float :amount_accrued
      t.string :currency
      t.integer :employee_id
      t.string :society

      t.timestamps null: false
    end
  end
end
