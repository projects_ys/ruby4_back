class AddFieldPdfTypeIdentToFileEmployee < ActiveRecord::Migration
  def change
    add_column :file_employees, :pdf_type_ident, :string
  end
end
