class CreateWebServicesSapSyncs < ActiveRecord::Migration
  def change
    create_table :web_services_sap_syncs do |t|
      t.text :name_service
      t.text :description_service

      t.timestamps null: false
    end
  end
end
