class AddShowNewEmployeesToCompanies < ActiveRecord::Migration
  def change
    add_column :companies, :show_new_employees, :boolean
  end
end
