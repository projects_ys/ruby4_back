class CreateCompanyUser < ActiveRecord::Migration
  def change
    create_table :company_users do |t|
      t.integer :total_users
      t.integer :total_employees
      t.integer :company_id

      t.timestamps
    end
  end
end
