class CreateEmployees < ActiveRecord::Migration
  def change
    create_table :employees do |t|
      t.belongs_to :user, index: true
      t.string :name
      t.string :second_name
      t.string :lastname
      t.string :second_lastname
      t.string :email
      t.text :phone
      t.string :short_name
      t.integer :identification
      t.string :image
      t.boolean :admin
      t.boolean :superadmin
      t.integer :apply_reviewer
      t.integer :data_reviewer

      t.timestamps null: false
    end
  end
end
