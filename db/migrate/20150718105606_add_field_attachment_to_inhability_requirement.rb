class AddFieldAttachmentToInhabilityRequirement < ActiveRecord::Migration
  def change
    add_column :inhability_requirements, :attachment, :string
  end
end
