class ChangeSvOrganigramaFormatToCompanies < ActiveRecord::Migration
  def change
    remove_column :companies, :sv_organigrama
    add_column :companies, :show_organizate_chart, :boolean
  end
end
