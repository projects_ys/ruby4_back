class CreatePdfTypes < ActiveRecord::Migration
  def change
    create_table :pdf_types do |t|
      t.string :activ
      t.string :ident
      t.string :nomid
      t.integer :company_id

      t.timestamps null: false
    end
  end
end
