class RenameCompensatedVacationToCompensatoryVacationRecord < ActiveRecord::Migration
  def change
    rename_table :compensated_vacations, :compensatory_vacation_records
  end
end
