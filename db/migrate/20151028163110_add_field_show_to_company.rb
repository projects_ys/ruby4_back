class AddFieldShowToCompany < ActiveRecord::Migration
  def change
    add_column :companies, :show_articles, :boolean, default: true
    add_column :companies, :show_labor, :boolean, default: true
    add_column :companies, :show_birthday, :boolean, default: true
    add_column :companies, :show_certificates_labor, :boolean, default: true
    add_column :companies, :show_certificates_vacations, :boolean, default: true
    add_column :companies, :show_certificates_payroll, :boolean, default: true
    add_column :companies, :show_certificates_income, :boolean, default: true
    add_column :companies, :show_scesa, :boolean, default: true
    add_column :companies, :show_licenses, :boolean, default: true
    add_column :companies, :show_loans, :boolean, default: true
    add_column :companies, :show_vacations, :boolean, default: true
    add_column :companies, :show_vacations_c, :boolean, default: true
    add_column :companies, :show_hoex, :boolean, default: true
    add_column :companies, :show_inhabilities, :boolean, default: true
    add_column :companies, :show_approvals_licenses, :boolean, default: true
    add_column :companies, :show_approvals_requirements, :boolean, default: true
    add_column :companies, :show_approvals_master_data, :boolean, default: true
  end
end
