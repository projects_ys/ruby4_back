class CreateEmployeeTurnovers < ActiveRecord::Migration
  def change
    create_table :employee_turnovers do |t|
      t.string :type_rotation
      t.string :denomination_class
      t.string :denomination_motive
      t.date :begda
      t.integer :employee_id
      t.integer :company_id
      t.string :society
      t.integer :position_id

      t.timestamps null: false
    end
  end
end
