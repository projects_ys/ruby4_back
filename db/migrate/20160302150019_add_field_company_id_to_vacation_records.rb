class AddFieldCompanyIdToVacationRecords < ActiveRecord::Migration
  def change
    add_column :vacation_records, :company_id, :integer
  end
end
