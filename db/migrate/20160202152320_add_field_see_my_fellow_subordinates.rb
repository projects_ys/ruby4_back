class AddFieldSeeMyFellowSubordinates < ActiveRecord::Migration
  def change
    add_column :companies, :see_my_fellow_subordinates, :boolean, default: true
  end
end
