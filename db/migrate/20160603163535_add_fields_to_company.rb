class AddFieldsToCompany < ActiveRecord::Migration
  def change
    add_column :companies, :front_protocol, :string
    add_column :companies, :front_subdomain, :string
    add_column :companies, :widget_color, :string
  end
end
