class AddFieldsaApproverIdPosicionAndApprovedByIdentificationToVacationRequirement < ActiveRecord::Migration
  def change
    add_column :vacation_requirements, :approver_id_posicion, :integer
    add_column :vacation_requirements, :approved_by_identification, :integer
  end
end
