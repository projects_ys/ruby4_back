class CreateServiceExecutionLogs < ActiveRecord::Migration
  def change
    create_table :service_execution_logs do |t|
      t.text :details
      t.text :status_service
      t.datetime :datetime_start_service
      t.datetime :datetime_end_service
      t.integer :duration_seconds
      t.integer :company_id
      t.timestamps null: false
      t.integer :web_services_sap_sync_id
    end
  end
end
