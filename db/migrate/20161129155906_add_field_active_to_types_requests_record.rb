class AddFieldActiveToTypesRequestsRecord < ActiveRecord::Migration
  def change
    add_column :types_requests_records, :active, :boolean, default: false
  end
end
