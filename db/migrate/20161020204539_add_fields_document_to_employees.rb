class AddFieldsDocumentToEmployees < ActiveRecord::Migration
  def change
    add_column :employees, :type_document, :string
    add_column :employees, :number_document, :string
    add_column :employees, :grouper_calendar, :string
    add_column :employees, :horary_calendar, :string
  end
end
