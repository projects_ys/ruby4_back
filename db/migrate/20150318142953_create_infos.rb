class CreateInfos < ActiveRecord::Migration
  def change
    create_table :infos do |t|
      t.integer :pernr
      t.string :subty
      t.string :ncamp
      t.string :ccamp
      t.string :objid
      t.string :boss
      t.boolean :approved
      t.belongs_to :employee, index: true

      t.timestamps null: false
    end
    add_foreign_key :infos, :employees
  end
end
