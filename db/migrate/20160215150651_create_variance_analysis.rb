class CreateVarianceAnalysis < ActiveRecord::Migration
  def change
    create_table :variance_analysis do |t|
      t.integer :position
      t.integer :position_boss
      t.string :id_cc_payroll
      t.string :description_payroll
      t.string :type_accumulation, limit: 1
      t.float :previous_amount
      t.float :current_amount
      t.float :difference_amount
      t.integer :percentage_difference, limit: 3
      t.string :currency, limit: 5
      t.string :society
      t.integer :employee_id
      t.integer :company_id

      t.timestamps null: false
    end
  end
end
