class CreateIncapacities < ActiveRecord::Migration
  def change
    create_table :incapacities do |t|
      t.date :begda
      t.date :endda
      t.integer :id_incapacity
      t.string :text_incapacity
      t.integer :days_working_incapacity
      t.integer :days_taken_incapacity
      t.integer :employee_id
      t.integer :company_id

      t.timestamps null: false
    end
  end
end
