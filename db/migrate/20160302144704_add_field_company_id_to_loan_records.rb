class AddFieldCompanyIdToLoanRecords < ActiveRecord::Migration
  def change
    add_column :loan_records, :company_id, :integer
  end
end
