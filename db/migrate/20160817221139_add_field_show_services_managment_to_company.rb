class AddFieldShowServicesManagmentToCompany < ActiveRecord::Migration
  def change
    add_column :companies, :show_services_management, :boolean
  end
end
