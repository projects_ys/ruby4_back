class AddFieldProfileViewToCompany < ActiveRecord::Migration
  def change
    add_column :companies, :profile_view, :string
  end
end
