class CreatePositions < ActiveRecord::Migration
  def change
    create_table :positions do |t|
      t.string :name_position
      t.integer :id_position
      t.integer :company_id

      t.timestamps null: false
    end
  end
end
