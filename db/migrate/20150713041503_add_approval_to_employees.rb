class AddApprovalToEmployees < ActiveRecord::Migration
  def change
    add_column :employees, :rel_lab, :string
    add_column :employees, :see_all_dm, :string
    add_column :employees, :see_rpgen, :string
    add_column :employees, :see_organ, :string
    add_column :employees, :is_admin, :string
    add_column :employees, :cesa_approver, :string
    add_column :employees, :dams_approver, :string
    add_column :employees, :hoex_approver, :string
    add_column :employees, :inca_approver, :string
    add_column :employees, :perm_approver, :string
    add_column :employees, :pres_approver, :string
    add_column :employees, :vaca_approver, :string
  end
end
