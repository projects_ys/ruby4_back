class CreatePermissions < ActiveRecord::Migration
  def change
    create_table :permissions do |t|
      t.date :begda
      t.date :endda
      t.integer :id_permission
      t.string :text_permission
      t.integer :days_working
      t.integer :days_taken
      t.integer :employee_id
      t.integer :company_id

      t.timestamps null: false
    end
  end
end
