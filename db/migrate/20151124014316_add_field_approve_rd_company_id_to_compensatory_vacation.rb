class AddFieldApproveRdCompanyIdToCompensatoryVacation < ActiveRecord::Migration
  def change
    add_column :compensatory_vacations, :approver_id_posicion, :integer
    add_column :compensatory_vacations, :approved_by_identification, :integer
    add_column :compensatory_vacations, :company_id, :integer
  end
end
