class CreateServiceVacations < ActiveRecord::Migration
  def change
    create_table :service_vacations do |t|
      t.string :attachment
      t.string :tipo
      t.date :fecha
      t.text :more_text
      t.string :status
      t.belongs_to :employee, index: true
      t.integer :days
      t.integer :approver_id_posicion
      t.integer :approved_by_identification

      t.timestamps null: false
    end
    add_foreign_key :service_vacations, :employees
  end
end
