class AddMultipleDomainsToCompany < ActiveRecord::Migration
  def change
    add_column :companies, :multiple_domains, :boolean
  end
end
