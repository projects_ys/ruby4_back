class CreateTimelinePositions < ActiveRecord::Migration
  def change
    create_table :timeline_positions do |t|
      t.integer :position_id
      t.date :begda
      t.date :endda
      t.text :name_position
      t.integer :company_id

      t.timestamps null: false
    end
  end
end
