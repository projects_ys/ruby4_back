class CreateTypeInhabilities < ActiveRecord::Migration
  def change
    create_table :type_inhabilities do |t|
      t.string :descd
      t.string :descr

      t.timestamps null: false
    end
  end
end
