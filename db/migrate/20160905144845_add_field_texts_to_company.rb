class AddFieldTextsToCompany < ActiveRecord::Migration
  def change
    add_column :companies, :texts_json, :text
  end
end
