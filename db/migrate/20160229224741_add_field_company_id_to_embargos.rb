class AddFieldCompanyIdToEmbargos < ActiveRecord::Migration
  def change
    add_column :embargos, :company_id, :integer
  end
end
