class AddFieldsTotalUsersAndTotalEmployeesToCompany < ActiveRecord::Migration
  def change
    add_column :companies, :total_users, :integer
    add_column :companies, :total_employees, :integer
  end
end
