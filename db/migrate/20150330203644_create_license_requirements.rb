class CreateLicenseRequirements < ActiveRecord::Migration
  def change
    create_table :license_requirements do |t|
      t.date :start_date
      t.date :end_date
      t.string :motivo
      t.string :status
      t.belongs_to :employee, index: true

      t.timestamps null: false
    end
    add_foreign_key :license_requirements, :employees
  end
end
