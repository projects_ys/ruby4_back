class AddFieldFooterHtmlToCompany < ActiveRecord::Migration
  def change
    add_column :companies, :footer_html, :text
  end
end
