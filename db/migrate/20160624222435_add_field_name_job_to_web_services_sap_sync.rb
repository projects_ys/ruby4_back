class AddFieldNameJobToWebServicesSapSync < ActiveRecord::Migration
  def change
    add_column :web_services_sap_syncs, :name_job, :string
  end
end
