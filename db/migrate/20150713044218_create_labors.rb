class CreateLabors < ActiveRecord::Migration
  def change
    create_table :labors do |t|
      t.string :mandt
      t.string :ansvh
      t.string :scesa
      t.string :shoex
      t.string :sinca
      t.string :sperm
      t.string :spres
      t.string :svaca
      t.belongs_to :employee, index: true

      t.timestamps null: false
    end
    add_foreign_key :labors, :employees
  end
end
