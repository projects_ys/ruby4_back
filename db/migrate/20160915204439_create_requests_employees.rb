class CreateRequestsEmployees < ActiveRecord::Migration
  def change
    create_table :requests_employees do |t|
      t.datetime :date_begin
      t.datetime :date_end
      t.string :status_id
      t.integer :types_requests_record_id
      t.integer :employee_id

      t.timestamps null: false
    end
  end
end
