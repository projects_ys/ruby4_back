class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.string :titulo
      t.string :imagen
      t.text :cuerpo
      t.text :ini
      t.boolean :publicado
      t.string :category
      t.belongs_to :employee, index: true

      t.timestamps null: false
    end
    add_foreign_key :articles, :employees
  end
end
