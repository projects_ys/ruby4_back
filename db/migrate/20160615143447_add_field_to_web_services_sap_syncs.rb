class AddFieldToWebServicesSapSyncs < ActiveRecord::Migration
  def change
    add_column :web_services_sap_syncs, :operation, :string
    add_column :web_services_sap_syncs, :url, :string
  end
end
