class AddFieldCompanyIdToFileEmployees < ActiveRecord::Migration
  def change
    add_column :file_employees, :company_id, :integer
  end
end
