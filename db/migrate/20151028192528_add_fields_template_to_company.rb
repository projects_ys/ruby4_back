class AddFieldsTemplateToCompany < ActiveRecord::Migration
  def change
    add_column :companies, :image_1, :string
    add_column :companies, :image_2, :string
    add_column :companies, :image_3, :string
    add_column :companies, :image_4, :string
    add_column :companies, :image_5, :string
    add_column :companies, :image_6, :string
    add_column :companies, :color_1, :string, default: '#123A73'
    add_column :companies, :color_2, :string, default: '#2ED63B'
    add_column :companies, :color_3, :string, default: '#00A651'
    add_column :companies, :fuente, :string
  end
end
