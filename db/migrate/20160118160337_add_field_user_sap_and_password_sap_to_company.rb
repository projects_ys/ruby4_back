class AddFieldUserSapAndPasswordSapToCompany < ActiveRecord::Migration
  def change
    add_column :companies, :user_sap, :string
    add_column :companies, :password_sap, :string
  end
end
