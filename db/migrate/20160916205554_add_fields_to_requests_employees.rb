class AddFieldsToRequestsEmployees < ActiveRecord::Migration
  def change
    add_column :requests_employees, :first_approval, :string
    add_column :requests_employees, :date_first_approval, :datetime
    add_column :requests_employees, :second_aprproval, :string
    add_column :requests_employees, :date_second_approval, :datetime
    add_column :requests_employees, :company_id, :integer
  end
end
