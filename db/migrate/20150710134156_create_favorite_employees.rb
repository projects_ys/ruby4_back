class CreateFavoriteEmployees < ActiveRecord::Migration
  def change
    create_table :favorite_employees do |t|
      t.integer :user_identification
      t.integer :employee_identification

      t.timestamps null: false
    end
  end
end
