class AddAttachmentToExtraRequirements < ActiveRecord::Migration
  def change
    add_column :extra_requirements, :attachment, :string
  end
end
