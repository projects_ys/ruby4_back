class ChangeTypeAmountAccruedToHistoricalPosition < ActiveRecord::Migration
  def up
    change_table :historical_positions do |t|
      t.change :amount_accrued, :text
    end
  end
end
