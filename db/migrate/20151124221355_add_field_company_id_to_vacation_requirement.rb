class AddFieldCompanyIdToVacationRequirement < ActiveRecord::Migration
  def change
    add_column :vacation_requirements, :company_id, :integer
  end
end
