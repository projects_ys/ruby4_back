class AddFieldManagementDocumentoToCompany < ActiveRecord::Migration
  def change
    add_column :companies, :show_document_management, :boolean, default:false
  end
end
