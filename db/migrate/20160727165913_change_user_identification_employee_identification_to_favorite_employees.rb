class ChangeUserIdentificationEmployeeIdentificationToFavoriteEmployees < ActiveRecord::Migration
  def change
    rename_column :favorite_employees, :user_identification, :employee_id
    rename_column :favorite_employees, :employee_identification, :favorite_employee
  end
end
