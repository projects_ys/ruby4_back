class ChangeVacationsUserId < ActiveRecord::Migration
  def self.up
    change_table :vacations do |t|
      t.rename :user_id, :employee_id
    end
  end
  def self.down
    change_table :vacations do |t|
      t.rename :employee_id, :user_id
    end
  end
end
