class AddFieldCompanyIdToTypesRequestForGroups < ActiveRecord::Migration
  def change
    add_column :types_request_for_groups, :company_id, :integer
  end
end
