class AddFieldsRecordsToServiceExecutionLogs < ActiveRecord::Migration
  def change
    add_column :service_execution_logs, :deleted_records, :integer
    add_column :service_execution_logs, :failed_records, :integer
    add_column :service_execution_logs, :saved_records, :integer
  end
end
