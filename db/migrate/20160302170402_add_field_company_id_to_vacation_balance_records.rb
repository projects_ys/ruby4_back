class AddFieldCompanyIdToVacationBalanceRecords < ActiveRecord::Migration
  def change
    add_column :vacation_balance_records, :company_id, :integer
  end
end
