class CreateLoans < ActiveRecord::Migration
  def change
    create_table :loans do |t|
      t.integer :employee_id
      t.integer :company_id
      t.string :subty
      t.integer :monto

      t.timestamps null: false
    end
  end
end
