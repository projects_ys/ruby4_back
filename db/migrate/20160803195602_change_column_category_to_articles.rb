class ChangeColumnCategoryToArticles < ActiveRecord::Migration
  def change
    remove_column :articles, :category
    add_column :articles,:custom_list_id,:integer
  end
end
