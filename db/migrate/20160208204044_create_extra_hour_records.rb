class CreateExtraHourRecords < ActiveRecord::Migration
  def change
    create_table :extra_hour_records do |t|
      t.date :fecha
      t.string :lgart
      t.string :lgtxt
      t.integer :anzhl
      t.integer :employee_id

      t.timestamps null: false
    end
  end
end
