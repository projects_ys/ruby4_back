class AddFieldsLdapToCompany < ActiveRecord::Migration
  def change
    add_column :companies, :login_ldap, :boolean, default: false
    add_column :companies, :host_ldap, :text
    add_column :companies, :port_ldap, :integer
    add_column :companies, :username_ldap, :text
    add_column :companies, :password_ldap, :text
    add_column :companies, :treebase_ldap, :text
    add_column :companies, :encryption_method_ldap, :text
  end
end
