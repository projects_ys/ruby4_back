class AddFieldToPaymentsDeductions < ActiveRecord::Migration
  def change
    add_column :payments_deductions, :identification_payroll_concept, :text
    add_column :payments_deductions, :description_payroll_concept, :text
  end
end
