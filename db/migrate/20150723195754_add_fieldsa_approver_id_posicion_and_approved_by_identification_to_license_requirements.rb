class AddFieldsaApproverIdPosicionAndApprovedByIdentificationToLicenseRequirements < ActiveRecord::Migration
  def change
    add_column :license_requirements, :approver_id_posicion, :integer
    add_column :license_requirements, :approved_by_identification, :integer
  end
end
