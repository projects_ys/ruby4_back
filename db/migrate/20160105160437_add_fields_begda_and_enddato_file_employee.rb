class AddFieldsBegdaAndEnddatoFileEmployee < ActiveRecord::Migration
  def change
    add_column :file_employees, :begda, :date
    add_column :file_employees, :endda, :date
  end
end
