class AddFieldCompanyIdToLicenseRequirement < ActiveRecord::Migration
  def change
    add_column :license_requirements, :company_id, :integer
  end
end
