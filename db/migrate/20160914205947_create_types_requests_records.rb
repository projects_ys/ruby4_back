class CreateTypesRequestsRecords < ActiveRecord::Migration
  def change
    create_table :types_requests_records do |t|
      t.string :id_activity
      t.string :subtype
      t.string :description
      t.string :moabw
      t.string :type_request
      t.string :form
      t.integer :company_id

      t.timestamps null: false
    end
  end
end
