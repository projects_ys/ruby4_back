class AddFieldsApproverIdPosicionAndApprovedByIdentificationToLoan < ActiveRecord::Migration
  def change
    add_column :loans, :approver_id_posicion, :integer
    add_column :loans, :approved_by_identification, :integer
  end
end
