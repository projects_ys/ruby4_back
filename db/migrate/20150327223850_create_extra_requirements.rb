class CreateExtraRequirements < ActiveRecord::Migration
  def change
    create_table :extra_requirements do |t|
      t.date :fecha
      t.integer :hours
      t.string :motivo
      t.string :status
      t.belongs_to :employee, index: true

      t.timestamps null: false
    end
    add_foreign_key :extra_requirements, :employees
  end
end
