class ChangeFileEmployeesEmployeeIdentification < ActiveRecord::Migration
  def self.up
    change_table :file_employees do |t|
      t.rename :employee_identification, :employee_id
    end
  end
  def self.down
    change_table :file_employees do |t|
      t.rename :employee_id, :employee_identification
    end
  end
end
