class AddFieldsCaaToExtraRequirement < ActiveRecord::Migration
  def change
    add_column :extra_requirements, :company_id, :integer
    add_column :extra_requirements, :approver_id_posicion, :integer
    add_column :extra_requirements, :approved_by_identification, :integer
  end
end
