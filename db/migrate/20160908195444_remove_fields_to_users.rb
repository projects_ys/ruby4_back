class RemoveFieldsToUsers < ActiveRecord::Migration
  def change
    remove_column :users, :name
    remove_column :users, :employee_id
    remove_column :users, :image
    remove_column :users, :pic
    add_column  :users, :employee_id, :integer
  end
end
