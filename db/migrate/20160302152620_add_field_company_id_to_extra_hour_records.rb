class AddFieldCompanyIdToExtraHourRecords < ActiveRecord::Migration
  def change
    add_column :extra_hour_records, :company_id, :integer
  end
end
