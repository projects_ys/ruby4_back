class CreateCommentsArticles < ActiveRecord::Migration
  def change
    create_table :comments_articles do |t|
      t.text :comment_text
      t.integer :employee_id
      t.integer :article_id
      t.integer :company_id

      t.timestamps null: false
    end
  end
end
