class ChangeTypePasswordSapToCompanies < ActiveRecord::Migration
  def up
    change_table :companies do |t|
      t.change :password_sap, :text
    end
  end
end
