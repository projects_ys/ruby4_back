class AddFieldCompanyIdToCompensatoryVacationRecords < ActiveRecord::Migration
  def change
    add_column :compensatory_vacation_records, :company_id, :integer
  end
end
