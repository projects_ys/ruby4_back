class AddFieldVccpApproverToEmployee < ActiveRecord::Migration
  def change
    add_column :employees, :vccp_approver, :string
  end
end
