class ChangeBossFormatInMyInfos < ActiveRecord::Migration
  def up
    change_column :infos, :boss, 'integer USING CAST(boss AS integer)'
  end

  def down
    change_column :infos, :boss, :string
  end
end
