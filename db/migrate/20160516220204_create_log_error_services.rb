class CreateLogErrorServices < ActiveRecord::Migration
  def change
    create_table :log_error_services do |t|
      t.integer :pernr
      t.text :society
      t.integer :company_id
      t.text :error_inspect
      t.integer :service_execution_log_id

      t.timestamps null: false
    end
  end
end
