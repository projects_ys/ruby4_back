class CreateCompensatedVacations < ActiveRecord::Migration
  def change
    create_table :compensated_vacations do |t|
      t.date :begda
      t.date :endda
      t.string :subty
      t.string :qctxt
      t.integer :numbr
      t.integer :employee_id

      t.timestamps null: false
    end
  end
end
