class AddCompanyDataToEmployees < ActiveRecord::Migration
  def change
    add_column :employees, :posicion, :string
    add_column :employees, :unidad_org, :string
    add_column :employees, :area, :string
    add_column :employees, :division_per, :string
    add_column :employees, :subdivision_per, :string
  end
end
