class AddFieldFileSupportToRequestsEmployee < ActiveRecord::Migration
  def change
    add_column :requests_employees, :file_support, :string
  end
end
