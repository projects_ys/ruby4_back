class CreateTypes < ActiveRecord::Migration
  def change
    create_table :types do |t|
      t.text :tipos
      t.belongs_to :company, index: true

      t.timestamps null: false
    end
    add_foreign_key :types, :companies
  end
end
