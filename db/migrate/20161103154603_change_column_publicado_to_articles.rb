class ChangeColumnPublicadoToArticles < ActiveRecord::Migration
  def change
    change_column :articles,:publicado,:boolean,default: false
  end
end
