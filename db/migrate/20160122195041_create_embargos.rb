class CreateEmbargos < ActiveRecord::Migration
  def change
    create_table :embargos do |t|
      #t.date    :fpper
      t.string    :fpper
      t.string :gcase
      t.date :edate
      t.string :desga
      t.string :gproc
      t.integer :value
      t.integer :gardv
      t.integer :balan
      t.decimal :perde
      t.string :gorna
      t.string :pnach
      t.integer :cedor, :limit => 8
      t.integer :acnum
      t.integer :employee_id

      t.timestamps null: false
    end
  end
end
