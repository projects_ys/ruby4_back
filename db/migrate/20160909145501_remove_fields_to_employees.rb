class RemoveFieldsToEmployees < ActiveRecord::Migration
  def change
    remove_column :employees, :short_name
    remove_column :employees, :admin
    remove_column :employees, :superadmin
    remove_column :employees, :moabw
    remove_column :employees, :vacations
    remove_column :employees, :volpago
    remove_column :employees, :notifications
    remove_column :employees, :notification_time
    rename_column :employees, :identification, :pernr
  end
end
