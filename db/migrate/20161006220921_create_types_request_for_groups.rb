class CreateTypesRequestForGroups < ActiveRecord::Migration
  def change
    create_table :types_request_for_groups do |t|
      t.string :identifier_group
      t.string :type_request
      t.timestamps null: false
    end
  end
end
