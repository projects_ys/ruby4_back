class AddFieldsColorsToCompany < ActiveRecord::Migration
  def change
    add_column :companies, :primary_color, :string
    add_column :companies, :text_primary_color, :string
    add_column :companies, :body_text, :string
    add_column :companies, :background_wrapper_color, :string
  end
end
