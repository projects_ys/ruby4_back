class CreateCertificateWorks < ActiveRecord::Migration
  def change
    create_table :certificate_works do |t|
      t.belongs_to :employee, index: true
      t.text :data
      t.string :fecha
      t.integer :company_id

      t.timestamps null: false
    end
  end
end
