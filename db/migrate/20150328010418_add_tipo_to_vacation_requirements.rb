class AddTipoToVacationRequirements < ActiveRecord::Migration
  def change
    add_column :vacation_requirements, :tipo, :string
  end
end
