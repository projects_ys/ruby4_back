class CreateEmployeeCalendarBreaks < ActiveRecord::Migration
  def change
    create_table :employee_calendar_breaks do |t|
      t.string :grouper
      t.string :horary
      t.date :date_break
      t.integer :company_id

      t.timestamps null: false
    end
  end
end
