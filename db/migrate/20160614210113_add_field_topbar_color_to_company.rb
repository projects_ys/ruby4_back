class AddFieldTopbarColorToCompany < ActiveRecord::Migration
  def change
    add_column :companies, :topbar_color, :string
  end
end
