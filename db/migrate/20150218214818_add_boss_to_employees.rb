class AddBossToEmployees < ActiveRecord::Migration
  def change
    add_column :employees, :boss, :integer
    add_column :employees, :fecha_nac, :date
    add_column :employees, :fecha_ingreso, :date
    add_column :employees, :id_posicion, :integer
    add_column :employees, :id_unidad_org, :integer
  end
end
