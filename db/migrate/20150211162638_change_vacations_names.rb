class ChangeVacationsNames < ActiveRecord::Migration
  def change
    change_table :vacations do |t|
      t.rename :until, :detalle
      t.change :detalle, :string, :array => true, :default => '{}'
      t.rename :from, :resumen
      t.change :resumen, :string, :array => true, :default => '{}'
      t.rename :taken, :company_id
    end
  end
end

