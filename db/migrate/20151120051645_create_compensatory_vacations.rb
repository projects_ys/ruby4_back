class CreateCompensatoryVacations < ActiveRecord::Migration
  def change
    create_table :compensatory_vacations do |t|
      t.integer :at_date
      t.string :rule
      t.decimal :quantity
      t.text :reason
      t.boolean :pay
      t.string :status
      t.string :tipo
      t.string :attachment
      t.belongs_to :employee, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
