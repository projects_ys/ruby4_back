class CreateVacationRequirements < ActiveRecord::Migration
  def change
    create_table :vacation_requirements do |t|
      t.date :start_date
      t.date :end_date
      t.text :more_text
      t.text :response_text
      t.string :status
      t.belongs_to :employee

      t.timestamps null: false
    end
  end
end
