class AddLaborConnectionTaxProcedureToMasterData < ActiveRecord::Migration
  def change
    add_column :master_data, :tax_procedure, :text
    add_column :master_data, :account_types, :text
  end
end
