class AddFieldMakeRequestToCompany < ActiveRecord::Migration
  def change
    add_column :companies, :make_request, :boolean, default: false
    add_column :companies, :make_approvals, :boolean, default: false
  end
end
