class AddFieldShowIndebtednessLevelsToCompanies < ActiveRecord::Migration
  def change
    add_column :companies, :show_indebtedness_levels, :boolean
  end
end
