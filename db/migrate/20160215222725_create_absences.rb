class CreateAbsences < ActiveRecord::Migration
  def change
    create_table :absences do |t|
      t.string :type_absence, limit: 1
      t.string :class_absence, limit: 4
      t.string :description_absence
      t.date :start_absence
      t.date :end_absence
      t.integer :days_payroll
      t.integer :days_natural
      t.integer :code_disease
      t.string :description_disease
      t.string :society
      t.integer :employee_id
      t.integer :company_id

      t.timestamps null: false
    end
  end
end
