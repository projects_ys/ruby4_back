class AddVacationsToEmployees < ActiveRecord::Migration
  def change
    add_column :employees, :vacations, :text
  end
end
