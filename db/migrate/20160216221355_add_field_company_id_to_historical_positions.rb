class AddFieldCompanyIdToHistoricalPositions < ActiveRecord::Migration
  def change
    add_column :historical_positions, :company_id, :integer
  end
end
