class AddFieldToFavoriteEmployees < ActiveRecord::Migration
  def change
    add_column :favorite_employees, :company_id, :integer
    add_column :employees, :name_complete, :string
  end
end
