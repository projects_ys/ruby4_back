class AddFieldAttachmentToVacationRequirement < ActiveRecord::Migration
  def change
    add_column :vacation_requirements, :attachment, :string
  end
end
