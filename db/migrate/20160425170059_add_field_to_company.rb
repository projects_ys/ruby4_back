class AddFieldToCompany < ActiveRecord::Migration
  def change
    add_column :companies, :alternative_connection_ldap, :boolean
  end
end
