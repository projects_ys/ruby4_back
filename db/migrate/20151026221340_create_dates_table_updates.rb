class CreateDatesTableUpdates < ActiveRecord::Migration
  def change
    create_table :dates_table_updates do |t|
      t.integer :company_id
      t.string :table_name
      t.integer :records

      t.timestamps null: false
    end
  end
end
