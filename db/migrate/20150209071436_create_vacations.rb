class CreateVacations < ActiveRecord::Migration
  def change
    create_table :vacations do |t|
      t.integer :taken
      t.integer :available
      t.integer :pernr
      t.date :from, :array => true, :default => '{}'
      t.date :until, :array => true, :default => '{}'
      t.belongs_to :user

      t.timestamps null: false
    end
  end
end
