class AddNationalityProvincesGenderToMasterDatum < ActiveRecord::Migration
  def change
    add_column :master_data, :nationality, :text
    add_column :master_data, :provinces, :text
    add_column :master_data, :gender, :text
  end
end
