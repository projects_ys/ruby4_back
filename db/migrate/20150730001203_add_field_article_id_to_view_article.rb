class AddFieldArticleIdToViewArticle < ActiveRecord::Migration
  def change
    add_column :view_articles, :article_id, :integer
  end
end
