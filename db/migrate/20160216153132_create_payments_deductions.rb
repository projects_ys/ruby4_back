class CreatePaymentsDeductions < ActiveRecord::Migration
  def change
    create_table :payments_deductions do |t|
      t.integer :year_report, limit: 4
      t.string :compression_concept, limit: 2
      t.integer :number_form, limit: 8
      t.string :type_identification, limit: 2
      t.string :number_identification, limit: 15
      t.string :text_cumulative_concept, limit: 70
      t.text :amount
      t.string :currency, limit: 5
      t.integer :employee_id
      t.string :society
      t.integer :company_id

      t.timestamps null: false
    end
  end
end
