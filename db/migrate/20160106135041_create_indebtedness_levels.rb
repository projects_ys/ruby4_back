class CreateIndebtednessLevels < ActiveRecord::Migration
  def change
    create_table :indebtedness_levels do |t|
      t.date    :payroll_date
      t.string  :payroll_concept_code
      t.string  :payroll_concept_txt
      t.text   :deven
      t.text   :deduc
      t.integer :employee_identification
      t.integer :company_id
      t.integer :employee_id
      t.timestamps null: false
    end
  end
end
