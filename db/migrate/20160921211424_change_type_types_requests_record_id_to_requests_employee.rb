class ChangeTypeTypesRequestsRecordIdToRequestsEmployee < ActiveRecord::Migration
  def change
    remove_column :requests_employees,:types_requests_record_id,:integer
    add_column :requests_employees,:types_requests_record_id,:string
    add_column :requests_employees,:observation_request,:text
    add_column :requests_employees,:comment_first_approver,:text
    add_column :requests_employees,:comment_second_approver,:text
    rename_column :requests_employees,:employee_id,:pernr
    rename_column :requests_employees,:first_approval_id,:first_approval_pernr
    rename_column :requests_employees,:second_approval_id,:second_approval_pernr
  end
end
