class AddFieldsImageAndVpnConnectionToCompany < ActiveRecord::Migration
  def change
    add_column :companies, :image_organizate_chart, :string
    add_column :companies, :vpn_web_services, :boolean, default: false
  end
end
