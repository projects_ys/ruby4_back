class CreateLastViewArticles < ActiveRecord::Migration
  def change
    create_table :last_view_articles do |t|
      t.integer :employee_id

      t.timestamps null: false
    end
  end
end
