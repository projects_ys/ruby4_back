class AddFieldBgLockScreenToCompany < ActiveRecord::Migration
  def change
    add_column :companies, :background_lockscreen, :string
  end
end
