class AddFieldAttachmentToLicenseRequirement < ActiveRecord::Migration
  def change
    add_column :license_requirements, :attachment, :string
  end
end
