class CreateSeverances < ActiveRecord::Migration
  def change
    create_table :severances do |t|
      t.integer :employee_id
      t.integer :company_id
      t.string :numero_de_actorizacion
      t.date :fecha_de_corte
      t.string :subty
      t.integer :approver_id_posicion
      t.integer :approver_by_identification

      t.timestamps null: false
    end
  end
end
