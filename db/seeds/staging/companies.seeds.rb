footer_rcn = <<HTML
  <div class="row">
    <div class="large-12 small-12 columns">
      <p class="copyright">&copy; RCN Television Todos los derechos reservados</p>
      Powered by <a href="http://hrsolutions-co.com" target="_blank">
      <img ng-src="images/hrs_logo.png" lat="hrs_logo" src="images/hrs_logo.png" height="70" width="70"></a>
    </div>
    <div class="large-6 small-6 columns"></div>
  </div>
HTML

footer_harinera = <<HTML
  <div class='row'>
    <div class='large-6 columns'>
      <div class='footer-logo'>
        <a href='/#/pages'><img alt='harinera del valle' ng-src='images/harinera_logo.png' src='images/harinera_logo.png' /></a>
      </div>
      <p class='text-center'>Harinera del valle &copy; 2015</p>
      <br/>
      <br/>
      <p>Powered by <a href="http://hrsolutions-co.com" target="_blank">
      <img ng-src="images/hrs_logo.png" lat="hrs_logo" src="images/hrs_logo.png" height="70" width="70"></a></p>
    </div>
    <div class='large-3 columns'>
      <h5>SECCIONES</h5>
      <ul class='no-bullet for-footer'>
        <li><a href=''>Quiénes Somos</a></li>
        <li><a href=''>Nuestras Áreas</a></li>
        <li><a href=''>GH</a></li>
        <li><a href=''>Tecnología Informática</a></li>
        <li><a href=''>Sitios de Trabajo</a></li>
        <li><a href=''>Un Cliente Industrial</a></li>
        <li><a href=''>Un Cliente Masivo</a></li>
      </ul>
    </div>
    <div class='large-3 columns'>
      <h5>INFORMACIÓN</h5>
      <ul class='no-bullet for-footer'>
        <li><a href=''>Clasificados</a></li>
        <li><a href=''>Formatos</a></li>
        <li><a href=''>Indicadores de Venta</a></li>
        <li><a href=''>Enlaces</a></li>
      </ul>
    </div>
  </div>
  <hr />
  <img ng-src='/images/logo-footer.png' alt='' />
HTML

footer_publicar = <<HTML
  <div class="row">
    <div class="large-12 small-12 columns">
      <p class="copyright">&copy; Publicar Todos los derechos reservados</p>
      <p>Powered by <a href="http://hrsolutions-co.com" target="_blank"></p>
      <img ng-src="images/hrs_logo.png" lat="hrs_logo" src="images/hrs_logo.png" height="70" width="70"></a>
    </div>
    <div class="large-6 small-6 columns"></div>
  </div>
HTML

footer_demo = <<HTML
  <div class="row">
    <div class="large-12 small-12 columns">
      <p class="copyright">&copy; Todos los derechos reservados</p>
      <p>Powered by <a href="http://hrsolutions-co.com" target="_blank"></p>
      <img ng-src="images/hrs_logo.png" lat="hrs_logo" src="images/hrs_logo.png" height="70" width="70"></a>
    </div>
    <div class="large-6 small-6 columns"></div>
  </div>
HTML
#mainurl: "http://hdvprd002.harineradelvalle.com:8000/sap/bc/ihr?sap-client=100&bukrs=0100&",
company_data_1 = {
    name: 'Harinera del valle',
    logo: "harinera_logo.png",
    mainurl: "http://hdvtst001.harineradelvalle.com:8000/sap/bc/ihr?sap-client=100&bukrs=0100&",
    user: nil, password: nil,
    banner_dashboard: "dashboard-profile",
    banner_cms: nil,
    banner_solicitudes: nil,
    active: true,
    sv_cesantias: "",
    sv_coworkers: nil,
    sv_encabezado: nil,
    show_organizate_chart: false,
    sv_endeudamiento: nil,
    profile_view: "smallish",
    show_inhabilities: false,
    show_scesa: false,
    show_loans: false,
    show_vacations: false,
    show_vacations_c: false,
    show_new_employees: false,
    show_indebtedness_levels: false,
    show_birthday: false,
    show_hoex:false,
    show_licenses: false,
    show_labor: true,
    show_articles: false,
    user_sap: 'ALEREMOTE', password_sap: 'biw0202',
    front_domain: 'https://harinera.hrinteractive.co',
    footer_html: footer_harinera.to_s,
    show_shart_severance: true,
    show_shart_severance_interest: true,
    login_ldap:true,
    host_ldap:'200.31.17.100',
    port_ldap:'389',
    username_ldap:'ihr.ad@harineradelvalle.com',
    password_ldap:'w1Hsd-9/16',
    treebase_ldap:'DC=harineradelvalle,DC=com',
    encryption_method_ldap:nil,
    timeout:600,
    ldap_connection: 'ADLDS',
    fixed_ip: false,
    show_services_management: true,
    front_subdomain: 'demo-hdv',
    multiple_domains: true,
    logo_dashboard: 'hdv_logo.png',
    logo_inside: 'hdv_logo.png',
    background_login: 'login_hdv.jpg',
    background_header_menu: 'header-profile.png',
    background_lockscreen: 'login_hdv.jpg',
    name_file_css: 'hdv.css',
    file_translate: 'demo-hdv.json',
    primary_color: '#123A76',
    text_primary_color: '#fff',
    body_text: '#676a6c',
    background_wrapper_color: '#f3f3f4',
    make_request: true,
    make_approvals:true

}

company_data_2 = {
    name: 'RCNTV',
    logo: "rcn.png",
    mainurl: "http://sd_internet.rcntv.com.co:8000/sap/bc/ihr?bukrs=RCN&",
    user: nil,
    password: nil,
    banner_dashboard: "dashboard-profile-rcn",
    banner_cms: nil,
    banner_solicitudes: nil,
    active: true,
    sv_cesantias: "",
    sv_coworkers: nil,
    sv_encabezado: nil,
    show_organizate_chart: false,
    sv_endeudamiento: nil,
    show_inhabilities: false,
    show_scesa: false,
    show_loans: false,
    show_vacations: false,
    show_vacations_c: false,
    show_new_employees: true,
    show_indebtedness_levels: true,
    show_birthday: true,
    show_hoex:false,
    show_licenses: false,
    show_labor: true,
    show_articles: true,
    user_sap: 'IHR_SOLUTION', password_sap: 'hrsol2016*3',
    front_domain: 'https://rcntv.hrinteractive.co',
    footer_html: footer_rcn.to_s,
    see_my_fellow_subordinates: false,
    show_shart_severance: false,
    show_shart_severance_interest: false,
    login_ldap:true,
    host_ldap:'CXLDP.RCNTV.COM.CO',
    port_ldap:50000,
    username_ldap:'rcnsapihr',
    password_ldap:'1hR54P-16',
    treebase_ldap:'DC=EXTLDAP,DC=RCNTV,DC=COM',
    encryption_method_ldap:nil,
    timeout:600,
    ldap_connection:'ADLDS',
    fixed_ip: true,
    show_services_management: true,
    front_subdomain: 'demo-rcntv',
    multiple_domains: true,
    logo_dashboard: 'rcntv_logo.png',
    logo_inside: 'rcntv_logo.png',
    background_login: 'login_rcntv.jpg',
    background_header_menu: 'header-profile.png',
    background_lockscreen: 'login_rcntv.jpg',
    name_file_css: 'rcntv.css',
    file_translate: 'demo-rcntv.json',
    primary_color: '#69b33e',
    text_primary_color: '#FFF',
    body_text: '#676a6c',
    background_wrapper_color: '#f3f3f4',
    make_request: true,
    make_approvals:true
}
company_data_3 = {
    name: 'Publicar',
    logo: "publicar_logo.png",
    mainurl: "https://websapprd.publicar.com/sap/bc/ihr?sap-client=400&bukrs=0004&",
    user: nil,
    password: nil,
    banner_dashboard: "dashboard-profile",
    banner_cms: nil,
    banner_solicitudes: nil,
    active: true,
    sv_cesantias: "true",
    sv_coworkers: "true",
    sv_encabezado: "true",
    show_organizate_chart: false,
    sv_endeudamiento: "true",
    show_inhabilities: false,
    show_scesa: true,
    show_loans: true,
    show_vacations: true,
    show_vacations_c: true,
    show_new_employees: true,
    show_indebtedness_levels: true,
    show_birthday: true,
    show_hoex:false,
    show_licenses: true,
    show_labor: true,
    show_articles: true,
    user_sap: 'PORTAL', password_sap: 'SAPprd2080#!',
    front_domain: 'https://publicar.hrinteractive.co',
    footer_html: footer_publicar.to_s,
    show_shart_severance: true,
    show_shart_severance_interest: true,
    login_ldap:false,
    host_ldap:nil,
    port_ldap:nil,
    username_ldap:nil,
    password_ldap:nil,
    treebase_ldap:nil,
    encryption_method_ldap:nil,
    timeout:600,
    ldap_connection:nil,
    fixed_ip: false,
    show_services_management: false,
    front_subdomain: 'demo-publicar',
    multiple_domains: true,
    logo_dashboard: 'publicar_logo.png',
    logo_inside: 'publicar_logo.png',
    background_login: 'login_publicar.png',
    background_header_menu: 'header-profile.png',
    background_lockscreen: 'login_publicar.png',
    name_file_css: 'publicar.css',
    file_translate: 'demo-publicar.json',
    primary_color: '#FFCC25',
    text_primary_color: '#000100',
    body_text: '#000',
    background_wrapper_color: '#F8F8F8',
    make_request: true,
    make_approvals:true
}
company_data_4 = {
    name: 'HRS',
    logo: "hrs_logo.png",
    mainurl: "http://hr-solutions.hrsolutions-co.com:8000/sap/bc/ihr?sap-client=111&bukrs=FNGS&",
    user: nil,
    password: nil,
    banner_dashboard: "dashboard-profile",
    banner_cms: nil,
    banner_solicitudes: nil,
    active: true,
    sv_cesantias: "true",
    sv_coworkers: "true",
    sv_encabezado: "true",
    show_organizate_chart: true,
    sv_endeudamiento: "true",
    show_inhabilities: true,
    show_scesa: true,
    show_loans: true,
    show_vacations: true,
    show_vacations_c: true,
    show_new_employees: true,
    show_indebtedness_levels: true,
    show_birthday: true,
    show_hoex:true,
    show_certificates_labor:true,
    show_certificates_vacations:true,
    show_certificates_payroll:true,
    show_certificates_income:true,
    show_licenses: true,
    show_labor: true,
    show_articles: true,
    front_subdomain: 'demo-hrsolutions',
    user_sap: 'S0013764379', password_sap: 'hrsol2016',
    front_domain: 'http://demo-hrsolutions.hrinteractive.co',
    footer_html: footer_demo.to_s,
    show_shart_severance: true,
    show_shart_severance_interest: true,
    login_ldap:false,
    host_ldap:nil,
    port_ldap:nil,
    username_ldap:nil,
    password_ldap:nil,
    treebase_ldap:nil,
    encryption_method_ldap:nil,
    timeout:600,
    ldap_connection:nil,
    fixed_ip: true,
    show_services_management: true,
    multiple_domains: true,
    make_request: true,
    make_approvals:true
}



company_1 = Company.find_by_name 'Harinera del valle'
if company_1.present?
  company_1.update_columns company_data_1
else
  Company.create company_data_1
end

company_2 = Company.find_by_name 'RCNTV'
if company_2
  company_2.update_columns company_data_2
else
  Company.create company_data_2
end

company_3 = Company.find_by_name 'Publicar'
if company_3.present?
  company_3.update_columns company_data_3
else
  Company.create company_data_3
end

company_4 = Company.find_by_name 'HRS'
if company_4.present?
  company_4.update_columns company_data_4
else
  Company.create company_data_4
end
