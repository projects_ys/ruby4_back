# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
#----------------Services Basicos------------------------------
service_employees_data = {
    name_service: "Empleados y Usuarios",
    description_service: "Actualiza la informacion del empleado junto con su acceso a iHR siempre y cuando el mismo cuente con correo electronico",
    operation: "EMPLE",
    url: "launch_service_update_employees_web_services_sap_syncs",
    name_job: "UpdateEmployeesJob"
}
service_labors_data = {
    name_service: "Acciones según relación laboral",
    description_service: "Actualiza permisos segun la relación laboral asignada al empleado",
    operation: "GETRL",
    url: "launch_service_update_labor_relation_web_services_sap_syncs",
    name_job: "UpdateLaborRelationJob"
}
service_vacations_data = {
    name_service: "Vacaciones 1",
    description_service: "Consolidado de los dias de vacaciones de cada usuario.",
    operation: "vacat",
    url: "launch_service_update_vacations_web_services_sap_syncs",
    name_job: "UpdateVacationsJob"
}
service_employees_saldos_data = {
    name_service: "Saldos",
    description_service: "Infomación consolidad de saldos, cesantias y demas",
    operation: "SALDS",
    url: "launch_service_update_saldos_web_services_sap_syncs",
    name_job: "UpdateEmployeesSaldosJob"
}
service_employees_info_types_data = {
    name_service: "Tipos de solicitud",
    description_service: "Información de los tipos de solicitudes y registros",
    operation: "GETACT",
    url: "launch_service_update_type_request_web_services_sap_syncs",
    name_job: "UpdateTypeRequestJob"
}
service_employee_info_data = {
    name_service: "Datos maestros Usuario",
    description_service: "Información de datos maestros por usuario, encriptados mediante AES256.",
    operation: "DMEMP",
    url: "launch_service_update_employee_info_web_services_sap_syncs",
    name_job: "UpdateEmployeesInfoJob"
}
service_master_data_info = {
    name_service: "Información de los datos maestros SAP",
    description_service: "Información de datos maestros almacenados en SAP.",
    operation: "BSCTBL",
    url: "launch_service_update_master_data_info_web_services_sap_syncs",
    name_job: "UpdateMasterDataInfoJob"
}
service_pdf_types_info_data = {
    name_service: "Tipos de PDF",
    description_service: "Tipos de archivos pdf que se encuentran disponibles por compañia en los reportes de autoservicios.",
    operation: "GETRDF",
    url: "launch_service_update_pdf_types_web_services_sap_syncs",
    name_job: "UpdatePdfTypesJob"
}
service_pdfs_data = {
    name_service: "PDFs",
    description_service: "Autoservicios generados de los pdf's",
    operation: "PDFS",
    url: "launch_service_update_pdfs_web_services_sap_syncs",
    name_job: "UpdatePdfsJob"
}
service_boss_data = {
    name_service: "Actualizacion jefe",
    description_service: "Organigrama, determina el jefe de cada empleado",
    operation: "ORGAN",
    url: "launch_service_update_boss_employee_web_services_sap_syncs",
    name_job: "UpdateBossEmployeesJob"
}
service_timeline_positions_data = {
    name_service: "Posiciones a lo largo del tiempo",
    description_service: "Posiciones que no aparecen vigentes, pero que deben tenerse en cuanta para algunos reportes de consultas y gerenciales",
    operation: "HISPOS",
    url: "launch_service_update_timeline_positions_web_services_sap_syncs",
    name_job: "UpdateTimelinePositionsJob"
}
service_calendar_breaks_data = {
    name_service: "Calendario de descansos",
    description_service: "Descansos por grupo de empleados en los futuros 3 meses",
    operation: "GETCLD",
    url: "launch_service_update_calendar_breaks_web_services_sap_syncs",
    name_job: "UpdateCalendarBreaksJob"
}

#Service 1
service_employees = WebServicesSapSync.find_by_operation 'EMPLE'
if service_employees.present?
  service_employees.update_columns service_employees_data
else
  WebServicesSapSync.create service_employees_data
end
#Service 2
service_labors = WebServicesSapSync.find_by_operation 'GETRL'
if service_labors.present?
  service_labors.update_columns service_labors_data
else
  WebServicesSapSync.create service_labors_data
end
#Service 3
service_vacations = WebServicesSapSync.find_by_operation 'vacat'
if service_vacations.present?
  service_vacations.update_columns service_vacations_data
else
  WebServicesSapSync.create service_vacations_data
end
#Service 4
service_employees_saldos = WebServicesSapSync.find_by_operation 'SALDS'
if service_employees_saldos.present?
  service_employees_saldos.update_columns service_employees_saldos_data
else
  WebServicesSapSync.create service_employees_saldos_data
end
#Service 5
service_employees_info_types = WebServicesSapSync.find_by_operation 'GETACT'
if service_employees_info_types.present?
  service_employees_info_types.update_columns service_employees_info_types_data
else
  WebServicesSapSync.create service_employees_info_types_data
end
#Service 6
service_employees_info = WebServicesSapSync.find_by_operation 'DMEMP'
if service_employees_info.present?
  service_employees_info.update_columns service_employee_info_data
else
  WebServicesSapSync.create service_employee_info_data
end
#Service 7
service_data_master_employees_info = WebServicesSapSync.find_by_operation 'BSCTBL'
if service_data_master_employees_info.present?
  service_data_master_employees_info.update_columns service_master_data_info
else
  WebServicesSapSync.create service_master_data_info
end
#Service 8
service_pdf_types = WebServicesSapSync.find_by_operation 'GETRDF'
if service_pdf_types.present?
  service_pdf_types.update_columns service_pdf_types_info_data
else
  WebServicesSapSync.create service_pdf_types_info_data
end
#Service 9
service_pdfs = WebServicesSapSync.find_by_operation 'PDFS'
if service_pdfs.present?
  service_pdfs.update_columns service_pdfs_data
else
  WebServicesSapSync.create service_pdfs_data
end
#Service 10
service_boss = WebServicesSapSync.find_by_operation 'ORGAN'
if service_boss.present?
  service_boss.update_columns service_boss_data
else
  WebServicesSapSync.create service_boss_data
end
#Service 11

service_timeline = WebServicesSapSync.find_by_operation 'HISPOS'
if service_timeline.present?
  service_timeline.update_columns service_timeline_positions_data
else
  WebServicesSapSync.create service_timeline_positions_data
end

## New Services
#Service 26
service_calendar_breaks = WebServicesSapSync.find_by_operation 'GETCLD'
if service_calendar_breaks.present?
  service_calendar_breaks.update_columns service_calendar_breaks_data
else
  WebServicesSapSync.create service_calendar_breaks_data
end


#-------------------------Services Consultas-------------------------------#

service_indebtedness_levels_data = {
    name_service: "Consulta de nivel de endeudamiento",
    description_service: "Niveles de endeudamiento de los empleados",
    operation: "CNIVEN",
    url: "launch_service_update_indebtedness_levels_web_services_sap_syncs",
    name_job: "UpdateIndebtednessLevelsJob"
}


service_embargoes_data = {
    name_service: "Consulta de embargos",
    description_service: "Embargos aplicados a los usuarios",
    operation: "CEMBRG",
    url: "launch_service_update_embargoes_web_services_sap_syncs",
    name_job: "UpdateEmbargoesJob"
}

service_loan_records_data = {
    name_service: "Consulta de prestamos",
    description_service: "Prestamos del empleado",
    operation: "CPREST",
    url: "launch_service_update_loan_records_web_services_sap_syncs",
    name_job: "UpdateLoanRecordsJob"
}
service_permissions_data = {
    name_service: "Consulta de permisos",
    description_service: "Premisos del empleado",
    operation: "CPERMS",
    url: "launch_service_update_permissions_web_services_sap_syncs",
    name_job: "UpdatePermissionsJob"
}
service_incapacity_data = {
    name_service: "Consulta de incapacidades",
    description_service: "Incapacidades del empleado",
    operation: "CINCAP",
    url: "launch_service_update_incapcity_web_services_sap_syncs",
    name_job: "UpdateIncapacityJob"
}
service_vacation_records_data = {
    name_service: "Consulta de vacaciones disfrutadas",
    description_service: "Vacaciones disfrutadas del empleado",
    operation: "CVACAS",
    url: "launch_service_update_vacaction_records_web_services_sap_syncs",
    name_job: "UpdateVacationRecordsJob"
}
service_extra_hours_data = {
    name_service: "Consulta de horas extras",
    description_service: "Horas extras del empleado",
    operation: "CHOEXT",
    url: "launch_service_update_extra_hour_records_web_services_sap_syncs",
    name_job: "UpdateExtraHourRecordsJob"
}
service_compensatory_vacations_data = {
    name_service: "Consulta de vacaciones compensadas",
    description_service: "Consulta las Vacaciones compensadas del usuario",
    operation: "CVCCMP",
    url: "launch_service_update_compensatory_vacations_records_web_services_sap_syncs",
    name_job: "UpdateCompensatoryVacationsRecordsJob"
}
service_vacation_balance_records_data = {
    name_service: "Consulta de balance/saldo de vacaciones",
    description_service: "Consulta Saldo de vacaciones de cada empleado",
    operation: "CCNTVC",
    url: "launch_service_update_vacation_balance_records_web_services_sap_syncs",
    name_job: "UpdateVacationBalanceRecordsJob"
}
#Service 12

service_indebtedness = WebServicesSapSync.find_by_operation 'CNIVEN'
if service_indebtedness.present?
  service_indebtedness.update_columns service_indebtedness_levels_data
else
  WebServicesSapSync.create service_indebtedness_levels_data
end

# Service 13
service_embargoes = WebServicesSapSync.find_by_operation 'CEMBRG'
if service_embargoes.present?
  service_embargoes.update_columns service_embargoes_data
else
  WebServicesSapSync.create service_embargoes_data
end
# Service 14
service_loan_records = WebServicesSapSync.find_by_operation 'CPREST'
if service_loan_records.present?
  service_loan_records.update_columns service_loan_records_data
else
  WebServicesSapSync.create service_loan_records_data
end
# Service 15
service_permissions = WebServicesSapSync.find_by_operation 'CPERMS'
if service_permissions.present?
  service_permissions.update_columns service_permissions_data
else
  WebServicesSapSync.create service_permissions_data
end
#Service 16
service_incapacity = WebServicesSapSync.find_by_operation 'CINCAP'
if service_incapacity.present?
  service_incapacity.update_columns service_incapacity_data
else
  WebServicesSapSync.create service_incapacity_data
end
#Service 17
service_vacation_records = WebServicesSapSync.find_by_operation 'CVACAS'
if service_vacation_records.present?
  service_vacation_records.update_columns service_vacation_records_data
else
  WebServicesSapSync.create service_vacation_records_data
end
#Service 18
service_extra_hours = WebServicesSapSync.find_by_operation 'CHOEXT'
if service_extra_hours.present?
  service_extra_hours.update_columns service_extra_hours_data
else
  WebServicesSapSync.create service_extra_hours_data
end
#Service 19
service_vacation_compensatory = WebServicesSapSync.find_by_operation 'CVCCMP'
if service_vacation_compensatory.present?
  service_vacation_compensatory.update_columns service_compensatory_vacations_data
else
  WebServicesSapSync.create service_compensatory_vacations_data
end
#Service 20
service_vacation_balance_records = WebServicesSapSync.find_by_operation  'CCNTVC'
if service_vacation_balance_records.present?
  service_vacation_balance_records.update_columns service_vacation_balance_records_data
else
  WebServicesSapSync.create service_vacation_balance_records_data
end

#-------------------------Services Reportes Gerenciales -------------------------------#
service_payments_deductions_data = {
    name_service: "Reporte gerencial de Pagos y deducciones",
    description_service: "Pagos y deducciones de los empleados",
    operation: "RGGCIR",
    url: "launch_service_update_payments_deductions_web_services_sap_syncs",
    name_job: "UpdatePaymentsDeductionsJob"
}

service_absences_data = {
    name_service: "Reporte Gerencial de Ausentismos",
    description_service: "Almacena los ausentismos que seran usados en los reportes gerenciales",
    operation: "RGABSN",
    url: "launch_service_update_absences_web_services_sap_syncs",
    name_job: "UpdateAbsencesJob"
}
service_variance_analysis_data = {
    name_service: "Reporte Gerencial de Analisis de variaciones",
    description_service: "Almacena los analisis de variaciones que sera visualizados en los reportes gerenciales.",
    operation: "RGANVA",
    url: "launch_service_update_variance_analyses_web_services_sap_syncs",
    name_job: "UpdateVarianceAnalysesJob"
}
service_historical_positions_data = {
    name_service: "Reporte Gerencial de Historico de posiciones",
    description_service: "Almacena el historico de posiciones que sera visualizados en los reportes gerenciales.",
    operation: "RGHICR",
    url: "launch_service_update_historical_positions_web_services_sap_syncs",
    name_job: "UpdateHistoricalPositionsJob"
}
service_staff_turnover_data = {
    name_service: "Reporte de rotaciones de personal",
    description_service: "Almacena las rotaciones del personal que sera visualizados en los reportes gerenciales.",
    operation: "RGRTPR",
    url: "launch_service_update_update_staff_turnover_web_services_sap_syncs",
    name_job: "UpdateStaffTurnoverJob"
}
#Service 21
service_payments_deductions = WebServicesSapSync.find_by_operation 'RGGCIR'
if service_payments_deductions.present?
  service_payments_deductions.update_columns service_payments_deductions_data
else
  WebServicesSapSync.create service_payments_deductions_data
end
#Service 22
service_absences = WebServicesSapSync.find_by_operation 'RGABSN'
if service_absences.present?
  service_absences.update_columns service_absences_data
else
  WebServicesSapSync.create service_absences_data
end
#Service 23
services_variance_analysis = WebServicesSapSync.find_by_operation 'RGANVA'
if services_variance_analysis.present?
  services_variance_analysis.update_columns service_variance_analysis_data
else
  WebServicesSapSync.create service_variance_analysis_data
end
#Service 24
services_historical_positions = WebServicesSapSync.find_by_operation 'RGHICR'
if services_historical_positions.present?
  services_historical_positions.update_columns service_historical_positions_data
else
  WebServicesSapSync.create service_historical_positions_data
end
#Service 25
service_staff_turnover = WebServicesSapSync.find_by_operation 'RGRTPR'
if service_staff_turnover.present?
  service_staff_turnover.update_columns service_staff_turnover_data
else
  WebServicesSapSync.create service_staff_turnover_data
end

service_send_requests_data = {
    name_service: "Enviar solicitudes aprobadas a SAP",
    description_service: "Envia las solicitudes aprobadas por el aprobador 1 a SAP para su segunda aprobacion.",
    operation: "PRCSOL",
    url: "launch_service_send_first_approvals_to_sap_web_services_sap_syncs",
    name_job: "SendFirstApprovalsToSapJob"
}

service_send_records_data = {
    name_service: "Enviar registros aprobadas a SAP",
    description_service: "Envia las registros aprobadas por el aprobador 1 a SAP para su segunda aprobacion.",
    operation: "PRCREG",
    url: nil,
    name_job: nil
}

service_fetch_requests_data = {
    name_service: "Busca las solicitudes gestionadas en SAP",
    description_service: "Una vez completada la gestion del aprobador 2 se procede a consultarlas.",
    operation: "GETAPS",
    url: "launch_service_fetch_approvals_web_services_sap_syncs",
    name_job: "FetchApprovalsJob"
}

#Service 27
service_send_requests = WebServicesSapSync.find_by_operation 'PRCSOL'
if service_send_requests.present?
  service_send_requests.update_columns service_send_requests_data
else
  WebServicesSapSync.create service_send_requests_data
end

#Service 28
service_send_records = WebServicesSapSync.find_by_operation 'PRCREG'
if service_send_records.present?
  service_send_records.update_columns service_send_records_data
else
  WebServicesSapSync.create service_send_records_data
end

#Service 29
service_fetch_requests = WebServicesSapSync.find_by_operation 'GETAPS'
if service_fetch_requests.present?
  service_fetch_requests.update_columns service_fetch_requests_data
else
  WebServicesSapSync.create service_fetch_requests_data
end

#Custom List
optionsArticles = {
    'default' =>  [
              {:value=>'Nomina',:order_priority=>2,:identifier =>'ArtCatNom'},
              {:value=>'Noticias y eventos de bienestar',:order_priority=>1,:identifier =>'ArtCatNotievent'},
              {:value=>'Salud ocupacional',:order_priority=>3,:identifier =>'ArtCatSaluOcup'},
              {:value=>'Talento humano',:order_priority=>4,:identifier =>'ArtCatTaleHuma'}
            ]
  }

#Flujo de aprobacion

statusApprovalFlow = {
    'default' =>  [
        {:value=>'Pendiente Aprobacion JEFE',:order_priority=>1,:identifier =>'ReqEmpStaPenAproJef'},
        {:value=>'Cancelado JEFE',:order_priority=>2,:identifier =>'ReqEmpStaCanJef'},
        {:value=>'Pendiente RH',:order_priority=>3,:identifier =>'ReqEmpStaPenRH'},
        {:value=>'Cancelado RH',:order_priority=>4,:identifier =>'ReqEmpStaCanRH'},
        {:value=>'Aprobado RH',:order_priority=>5,:identifier =>'ReqEmpStaAprRH'}
    ],

}


companies = Company.select(:id)

companies.each do |company|

  #OPTIONS INSERT
  articlesCustomList = {
      object_name: "Article",
      field: "categories",
      enabled: TRUE,
      company_id: company.id
  }
  if optionsArticles["#{company.id}"].present?
    options_to_create = optionsArticles["#{company.id}"]
  else
    options_to_create = optionsArticles["default"]
  end

  options_to_create.each do|option|
    articlesCustomList[:value] = option[:value]
    articlesCustomList[:order_priority] = option[:order_priority]
    articlesCustomList[:identifier] = option[:identifier]
    option_custom_list = CustomList.find_by_object_name_and_field_and_value_and_company_id(articlesCustomList[:object_name],articlesCustomList[:field],articlesCustomList[:value],articlesCustomList[:company_id])
    if option_custom_list.present?
      option_custom_list.update_columns articlesCustomList
    else
      CustomList.create articlesCustomList
    end
  end

  #STATUS APPROVAL
  statusCustomList = {
      object_name: "RequestsEmployee",
      field: "status",
      enabled: TRUE,
      company_id: company.id
  }

  if statusApprovalFlow["#{company.id}"].present?
    status_to_create = statusApprovalFlow["#{company.id}"]
  else
    status_to_create = statusApprovalFlow["default"]
  end

  status_to_create.each do|status|
    statusCustomList[:value] = status[:value]
    statusCustomList[:order_priority] = status[:order_priority]
    statusCustomList[:identifier] = status[:identifier]
    status_custom_list = CustomList.find_by_object_name_and_field_and_value_and_company_id(statusCustomList[:object_name],statusCustomList[:field],statusCustomList[:value],statusCustomList[:company_id])

    if status_custom_list.present?
      status_custom_list.update_columns statusCustomList
    else
      a = CustomList.create statusCustomList
    end
  end

end



