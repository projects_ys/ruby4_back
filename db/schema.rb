# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161213155044) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "absences", force: :cascade do |t|
    t.string   "type_absence",        limit: 1
    t.string   "class_absence",       limit: 4
    t.string   "description_absence"
    t.date     "start_absence"
    t.date     "end_absence"
    t.integer  "days_payroll"
    t.integer  "days_natural"
    t.integer  "code_disease"
    t.string   "description_disease"
    t.string   "society"
    t.integer  "employee_id"
    t.integer  "company_id"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "admin_users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "articles", force: :cascade do |t|
    t.string   "titulo"
    t.string   "imagen"
    t.text     "cuerpo"
    t.text     "ini"
    t.boolean  "publicado",   default: false
    t.integer  "employee_id"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.integer  "company_id"
    t.string   "category"
  end

  add_index "articles", ["employee_id"], name: "index_articles_on_employee_id", using: :btree

  create_table "certificate_works", force: :cascade do |t|
    t.integer  "employee_id"
    t.text     "data"
    t.string   "fecha"
    t.integer  "company_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "pernr"
  end

  add_index "certificate_works", ["employee_id"], name: "index_certificate_works_on_employee_id", using: :btree

  create_table "comments_articles", force: :cascade do |t|
    t.text     "comment_text"
    t.integer  "employee_id"
    t.integer  "article_id"
    t.integer  "company_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "companies", force: :cascade do |t|
    t.string   "name",                                              null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "logo"
    t.string   "mainurl"
    t.string   "user"
    t.string   "password"
    t.string   "banner_dashboard"
    t.string   "banner_cms"
    t.string   "banner_solicitudes"
    t.boolean  "active"
    t.string   "sv_cesantias"
    t.string   "sv_coworkers"
    t.string   "sv_encabezado"
    t.string   "sv_endeudamiento"
    t.boolean  "show_articles",                 default: true
    t.boolean  "show_labor",                    default: true
    t.boolean  "show_birthday",                 default: true
    t.boolean  "show_certificates_labor",       default: true
    t.boolean  "show_certificates_vacations",   default: true
    t.boolean  "show_certificates_payroll",     default: true
    t.boolean  "show_certificates_income",      default: true
    t.boolean  "show_scesa",                    default: true
    t.boolean  "show_licenses",                 default: true
    t.boolean  "show_loans",                    default: true
    t.boolean  "show_vacations",                default: true
    t.boolean  "show_vacations_c",              default: true
    t.boolean  "show_hoex",                     default: true
    t.boolean  "show_inhabilities",             default: true
    t.boolean  "show_approvals_licenses",       default: true
    t.boolean  "show_approvals_requirements",   default: true
    t.boolean  "show_approvals_master_data",    default: true
    t.string   "image_1"
    t.string   "image_2"
    t.string   "image_3"
    t.string   "image_4"
    t.string   "image_5"
    t.string   "image_6"
    t.string   "color_1",                       default: "#123A73"
    t.string   "color_2",                       default: "#2ED63B"
    t.string   "color_3",                       default: "#00A651"
    t.string   "fuente"
    t.text     "footer_html"
    t.string   "profile_view"
    t.string   "front_domain"
    t.boolean  "show_new_employees"
    t.boolean  "show_indebtedness_levels"
    t.string   "user_sap"
    t.text     "password_sap"
    t.boolean  "see_my_fellow_subordinates",    default: true
    t.boolean  "show_shart_severance"
    t.boolean  "show_shart_severance_interest"
    t.boolean  "login_ldap",                    default: false
    t.text     "host_ldap"
    t.integer  "port_ldap"
    t.text     "username_ldap"
    t.text     "password_ldap"
    t.text     "treebase_ldap"
    t.text     "encryption_method_ldap"
    t.integer  "timeout"
    t.text     "ldap_connection"
    t.boolean  "fixed_ip"
    t.boolean  "ldap_ssl"
    t.string   "front_protocol"
    t.string   "front_subdomain"
    t.string   "widget_color"
    t.string   "topbar_color"
    t.integer  "total_users"
    t.integer  "total_employees"
    t.string   "icons_color"
    t.boolean  "show_services_management"
    t.boolean  "multiple_domains"
    t.string   "logo_dashboard"
    t.string   "logo_inside"
    t.string   "background_login"
    t.string   "background_header_menu"
    t.string   "background_lockscreen"
    t.string   "name_file_css"
    t.string   "primary_color"
    t.string   "text_primary_color"
    t.string   "body_text"
    t.string   "background_wrapper_color"
    t.string   "file_translate"
    t.text     "texts_json"
    t.boolean  "make_request",                  default: false
    t.boolean  "make_approvals",                default: false
    t.boolean  "show_organizate_chart"
    t.boolean  "notification_new_volpg",        default: false
    t.boolean  "show_document_management",      default: false
  end

  add_index "companies", ["name"], name: "index_companies_on_name", unique: true, using: :btree

  create_table "company_users", force: :cascade do |t|
    t.integer  "total_users"
    t.integer  "total_employees"
    t.integer  "company_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "compensatory_vacation_records", force: :cascade do |t|
    t.date     "begda"
    t.date     "endda"
    t.string   "subty"
    t.string   "qctxt"
    t.integer  "numbr"
    t.integer  "employee_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "company_id"
  end

  create_table "compensatory_vacations", force: :cascade do |t|
    t.integer  "at_date"
    t.string   "rule"
    t.decimal  "quantity"
    t.text     "reason"
    t.boolean  "pay"
    t.string   "status"
    t.string   "tipo"
    t.string   "attachment"
    t.integer  "employee_id"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.integer  "approver_id_posicion"
    t.integer  "approved_by_identification"
    t.integer  "company_id"
  end

  add_index "compensatory_vacations", ["employee_id"], name: "index_compensatory_vacations_on_employee_id", using: :btree

  create_table "custom_lists", force: :cascade do |t|
    t.string   "object_name"
    t.string   "field"
    t.string   "value"
    t.text     "properties"
    t.boolean  "enabled"
    t.integer  "company_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "order_priority"
    t.string   "identifier"
  end

  create_table "dates_table_updates", force: :cascade do |t|
    t.integer  "company_id"
    t.string   "table_name"
    t.integer  "records"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer  "priority",   default: 0, null: false
    t.integer  "attempts",   default: 0, null: false
    t.text     "handler",                null: false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree

  create_table "embargos", force: :cascade do |t|
    t.string   "fpper"
    t.string   "gcase"
    t.date     "edate"
    t.string   "desga"
    t.string   "gproc"
    t.integer  "value"
    t.integer  "gardv"
    t.integer  "balan"
    t.decimal  "perde"
    t.string   "gorna"
    t.string   "pnach"
    t.text     "cedor"
    t.text     "acnum"
    t.integer  "employee_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "company_id"
    t.text     "orcod"
    t.text     "desor"
    t.string   "seqnr"
  end

  create_table "employee_calendar_breaks", force: :cascade do |t|
    t.string   "grouper"
    t.string   "horary"
    t.date     "date_break"
    t.integer  "company_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "employee_infos", force: :cascade do |t|
    t.integer  "employee_id"
    t.integer  "company_id"
    t.string   "bukrs"
    t.integer  "pernr"
    t.text     "datos_personales"
    t.text     "datos_contacto"
    t.text     "datos_familiares"
    t.text     "datos_beneficiarios"
    t.text     "datos_estudios"
    t.text     "datos_empresariales"
    t.text     "datos_bancarios"
    t.text     "datos_seguridadsocial"
    t.text     "datos_impuestos"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.text     "datos_retefuente"
  end

  add_index "employee_infos", ["employee_id"], name: "index_employee_infos_on_employee_id", using: :btree

  create_table "employee_turnovers", force: :cascade do |t|
    t.string   "type_rotation"
    t.string   "denomination_class"
    t.string   "denomination_motive"
    t.date     "begda"
    t.integer  "employee_id"
    t.integer  "company_id"
    t.string   "society"
    t.integer  "position_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  create_table "employees", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "name"
    t.string   "second_name"
    t.string   "lastname"
    t.string   "second_lastname"
    t.string   "email"
    t.text     "phone"
    t.integer  "pernr"
    t.string   "image"
    t.integer  "apply_reviewer"
    t.integer  "data_reviewer"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.string   "posicion"
    t.string   "unidad_org"
    t.string   "area"
    t.string   "division_per"
    t.string   "subdivision_per"
    t.integer  "company_id"
    t.integer  "boss"
    t.date     "fecha_nac"
    t.date     "fecha_ingreso"
    t.integer  "id_posicion"
    t.integer  "id_unidad_org"
    t.string   "rel_lab"
    t.string   "see_all_dm"
    t.string   "see_rpgen"
    t.string   "see_organ"
    t.string   "is_admin"
    t.string   "cesa_approver"
    t.string   "dams_approver"
    t.string   "hoex_approver"
    t.string   "inca_approver"
    t.string   "perm_approver"
    t.string   "pres_approver"
    t.string   "vaca_approver"
    t.string   "vccp_approver"
    t.string   "new_cont"
    t.string   "sociedad"
    t.integer  "persg"
    t.string   "second_cesa_approver"
    t.string   "second_dams_approver"
    t.string   "second_hoex_approver"
    t.string   "second_inca_approver"
    t.string   "second_perm_approver"
    t.string   "second_pres_approver"
    t.string   "second_vaca_approver"
    t.string   "second_vccp_approver"
    t.string   "name_complete"
    t.string   "group_request"
    t.string   "type_document"
    t.string   "number_document"
    t.string   "grouper_calendar"
    t.string   "horary_calendar"
    t.boolean  "is_first_approver",    default: false
    t.boolean  "is_second_approver",   default: false
  end

  add_index "employees", ["user_id"], name: "index_employees_on_user_id", using: :btree

  create_table "extra_hour_records", force: :cascade do |t|
    t.date     "fecha"
    t.string   "lgart"
    t.string   "lgtxt"
    t.integer  "anzhl"
    t.integer  "employee_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "company_id"
  end

  create_table "extra_requirements", force: :cascade do |t|
    t.date     "fecha"
    t.integer  "hours"
    t.string   "motivo"
    t.string   "status"
    t.integer  "employee_id"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.string   "attachment"
    t.integer  "approver_employee_posicion_id"
    t.integer  "company_id"
    t.integer  "approver_id_posicion"
    t.integer  "approved_by_identification"
  end

  add_index "extra_requirements", ["employee_id"], name: "index_extra_requirements_on_employee_id", using: :btree

  create_table "favorite_employees", force: :cascade do |t|
    t.integer  "employee_id"
    t.integer  "favorite_employee"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.integer  "company_id"
  end

  create_table "file_employees", force: :cascade do |t|
    t.integer  "employee_id"
    t.string   "file"
    t.string   "op"
    t.string   "archivo"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.string   "name_iden"
    t.string   "pdf_type_ident"
    t.date     "begda"
    t.date     "endda"
    t.integer  "company_id"
    t.datetime "notificated_at"
  end

  create_table "group_employees_requests", force: :cascade do |t|
    t.string   "identifier_group"
    t.integer  "company_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "historical_positions", force: :cascade do |t|
    t.date     "begda"
    t.date     "endda"
    t.integer  "position"
    t.integer  "position_boss"
    t.text     "amount_accrued"
    t.string   "currency"
    t.integer  "employee_id"
    t.string   "society"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "company_id"
  end

  create_table "incapacities", force: :cascade do |t|
    t.date     "begda"
    t.date     "endda"
    t.integer  "id_incapacity"
    t.string   "text_incapacity"
    t.integer  "days_working_incapacity"
    t.integer  "days_taken_incapacity"
    t.integer  "employee_id"
    t.integer  "company_id"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "indebtedness_levels", force: :cascade do |t|
    t.date     "payroll_date"
    t.string   "payroll_concept_code"
    t.string   "payroll_concept_txt"
    t.text     "deven"
    t.text     "deduc"
    t.integer  "employee_identification"
    t.integer  "company_id"
    t.integer  "employee_id"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "infos", force: :cascade do |t|
    t.integer  "pernr"
    t.string   "subty"
    t.string   "ncamp"
    t.string   "ccamp"
    t.string   "objid"
    t.integer  "boss"
    t.boolean  "approved"
    t.integer  "employee_id"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.string   "dcamp"
    t.string   "comparador"
    t.string   "objps"
    t.string   "where"
    t.integer  "approver_id_posicion"
    t.integer  "approved_by_identification"
    t.integer  "company_id"
    t.string   "status"
  end

  add_index "infos", ["employee_id"], name: "index_infos_on_employee_id", using: :btree

  create_table "inhability_requirements", force: :cascade do |t|
    t.date     "start_date"
    t.date     "end_date"
    t.string   "motivo"
    t.string   "status"
    t.integer  "employee_id"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.string   "attachment"
    t.integer  "approver_id_posicion"
    t.integer  "approved_by_identification"
    t.integer  "company_id"
  end

  add_index "inhability_requirements", ["employee_id"], name: "index_inhability_requirements_on_employee_id", using: :btree

  create_table "labors", force: :cascade do |t|
    t.string   "mandt"
    t.string   "ansvh"
    t.string   "scesa"
    t.string   "shoex"
    t.string   "sinca"
    t.string   "sperm"
    t.string   "spres"
    t.string   "svaca"
    t.integer  "employee_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "bukrs"
    t.string   "svccp"
    t.integer  "company_id"
  end

  add_index "labors", ["employee_id"], name: "index_labors_on_employee_id", using: :btree

  create_table "last_view_articles", force: :cascade do |t|
    t.integer  "employee_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "license_requirements", force: :cascade do |t|
    t.date     "start_date"
    t.date     "end_date"
    t.string   "motivo"
    t.string   "status"
    t.integer  "employee_id"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.string   "attachment"
    t.integer  "approver_id_posicion"
    t.integer  "approved_by_identification"
    t.integer  "company_id"
  end

  add_index "license_requirements", ["employee_id"], name: "index_license_requirements_on_employee_id", using: :btree

  create_table "loan_records", force: :cascade do |t|
    t.string   "dlart"
    t.string   "stext"
    t.integer  "objps"
    t.string   "fpper"
    t.date     "fpbeg"
    t.date     "fpend"
    t.integer  "zsoll"
    t.integer  "nrdys"
    t.text     "lolci"
    t.text     "lolrp"
    t.text     "lolim"
    t.text     "totcu"
    t.text     "lollb"
    t.text     "totsa"
    t.text     "anrte"
    t.text     "darbt"
    t.integer  "employee_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "company_id"
  end

  create_table "loans", force: :cascade do |t|
    t.integer  "employee_id"
    t.integer  "company_id"
    t.string   "subty"
    t.integer  "monto"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.string   "status"
    t.integer  "approver_id_posicion"
    t.integer  "approved_by_identification"
  end

  create_table "log_error_services", force: :cascade do |t|
    t.integer  "pernr"
    t.text     "society"
    t.integer  "company_id"
    t.text     "error_inspect"
    t.integer  "service_execution_log_id"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "master_data", force: :cascade do |t|
    t.text     "countries"
    t.text     "state"
    t.text     "family"
    t.text     "institutes"
    t.text     "measurement_units"
    t.text     "marital_status"
    t.text     "formations"
    t.text     "titles"
    t.text     "specialties"
    t.text     "labor_relations"
    t.text     "banks"
    t.integer  "company_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.text     "nationality"
    t.text     "provinces"
    t.text     "gender"
    t.text     "tax_procedure"
    t.text     "account_types"
  end

  create_table "oauth_access_grants", force: :cascade do |t|
    t.integer  "resource_owner_id", null: false
    t.integer  "application_id",    null: false
    t.string   "token",             null: false
    t.integer  "expires_in",        null: false
    t.text     "redirect_uri",      null: false
    t.datetime "created_at",        null: false
    t.datetime "revoked_at"
    t.string   "scopes"
  end

  add_index "oauth_access_grants", ["token"], name: "index_oauth_access_grants_on_token", unique: true, using: :btree

  create_table "oauth_access_tokens", force: :cascade do |t|
    t.integer  "resource_owner_id"
    t.integer  "application_id"
    t.string   "token",             null: false
    t.string   "refresh_token"
    t.integer  "expires_in"
    t.datetime "revoked_at"
    t.datetime "created_at",        null: false
    t.string   "scopes"
  end

  add_index "oauth_access_tokens", ["refresh_token"], name: "index_oauth_access_tokens_on_refresh_token", unique: true, using: :btree
  add_index "oauth_access_tokens", ["resource_owner_id"], name: "index_oauth_access_tokens_on_resource_owner_id", using: :btree
  add_index "oauth_access_tokens", ["token"], name: "index_oauth_access_tokens_on_token", unique: true, using: :btree

  create_table "oauth_applications", force: :cascade do |t|
    t.string   "name",                      null: false
    t.string   "uid",                       null: false
    t.string   "secret",                    null: false
    t.text     "redirect_uri",              null: false
    t.string   "scopes",       default: "", null: false
    t.integer  "company_id",                null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "oauth_applications", ["uid"], name: "index_oauth_applications_on_uid", unique: true, using: :btree

  create_table "payments_deductions", force: :cascade do |t|
    t.integer  "year_report"
    t.string   "compression_concept",            limit: 2
    t.integer  "number_form",                    limit: 8
    t.string   "type_identification",            limit: 2
    t.string   "number_identification",          limit: 15
    t.string   "text_cumulative_concept",        limit: 70
    t.text     "amount"
    t.string   "currency",                       limit: 5
    t.integer  "employee_id"
    t.string   "society"
    t.integer  "company_id"
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
    t.text     "identification_payroll_concept"
    t.text     "description_payroll_concept"
  end

  create_table "pdf_types", force: :cascade do |t|
    t.string   "activ"
    t.string   "ident"
    t.string   "nomid"
    t.integer  "company_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "permissions", force: :cascade do |t|
    t.date     "begda"
    t.date     "endda"
    t.integer  "id_permission"
    t.string   "text_permission"
    t.integer  "days_working"
    t.integer  "days_taken"
    t.integer  "employee_id"
    t.integer  "company_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "positions", force: :cascade do |t|
    t.string   "name_position"
    t.integer  "id_position"
    t.integer  "company_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.integer  "position_boss"
  end

  create_table "requests_employees", force: :cascade do |t|
    t.datetime "date_begin"
    t.datetime "date_end"
    t.string   "status_id"
    t.integer  "pernr"
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.string   "first_approval_name"
    t.datetime "date_first_approval"
    t.string   "second_approval_name"
    t.datetime "date_second_approval"
    t.integer  "company_id"
    t.integer  "first_approval_pernr"
    t.integer  "first_approval_position"
    t.integer  "second_approval_pernr"
    t.integer  "second_approval_position"
    t.integer  "first_approver_assigned"
    t.integer  "second_approver_assigned"
    t.string   "types_requests_record_id"
    t.text     "observation_request"
    t.text     "comment_first_approver"
    t.text     "comment_second_approver"
    t.string   "synch_message_sap"
    t.float    "days_request"
    t.string   "id_activity"
    t.string   "file_support"
    t.string   "name_applicant"
    t.string   "typedoc_applicant"
    t.string   "numdoc_applicant"
    t.boolean  "is_active",                default: true
  end

  create_table "saldos", force: :cascade do |t|
    t.string   "bukrs"
    t.integer  "pernr"
    t.text     "saldo"
    t.text     "vcausado"
    t.text     "intsaldo"
    t.text     "intvcausado"
    t.text     "totdevengos"
    t.text     "totdeducciones"
    t.text     "t_cesantias"
    t.text     "t_intcesantias"
    t.text     "t_endeudamiento"
    t.integer  "employee_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "company_id"
  end

  add_index "saldos", ["employee_id"], name: "index_saldos_on_employee_id", using: :btree

  create_table "service_execution_logs", force: :cascade do |t|
    t.text     "details"
    t.text     "status_service"
    t.datetime "datetime_start_service"
    t.datetime "datetime_end_service"
    t.integer  "duration_seconds"
    t.integer  "company_id"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.integer  "web_services_sap_sync_id"
    t.integer  "deleted_records"
    t.integer  "failed_records"
    t.integer  "saved_records"
  end

  create_table "service_vacations", force: :cascade do |t|
    t.string   "attachment"
    t.string   "tipo"
    t.date     "fecha"
    t.text     "more_text"
    t.string   "status"
    t.integer  "employee_id"
    t.integer  "days"
    t.integer  "approver_id_posicion"
    t.integer  "approved_by_identification"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "service_vacations", ["employee_id"], name: "index_service_vacations_on_employee_id", using: :btree

  create_table "severances", force: :cascade do |t|
    t.integer  "employee_id"
    t.integer  "company_id"
    t.string   "numero_de_actorizacion"
    t.date     "fecha_de_corte"
    t.string   "subty"
    t.integer  "approver_id_posicion"
    t.integer  "approver_by_identification"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.integer  "monto"
    t.integer  "status"
  end

  create_table "timeline_positions", force: :cascade do |t|
    t.integer  "position_id"
    t.date     "begda"
    t.date     "endda"
    t.text     "name_position"
    t.integer  "company_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "type_inhabilities", force: :cascade do |t|
    t.string   "descd"
    t.string   "descr"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "types_request_for_groups", force: :cascade do |t|
    t.string   "identifier_group"
    t.string   "type_request"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.integer  "company_id"
  end

  create_table "types_requests_records", force: :cascade do |t|
    t.string   "id_activity"
    t.string   "subtype"
    t.string   "description"
    t.string   "moabw"
    t.string   "type_request"
    t.string   "form"
    t.integer  "company_id"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.string   "contingent"
    t.boolean  "active",       default: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",      null: false
    t.string   "encrypted_password",     default: "",      null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,       null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "provider",               default: "email", null: false
    t.string   "uid",                    default: "",      null: false
    t.string   "nickname"
    t.json     "tokens"
    t.integer  "failed_attempts",        default: 0
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.integer  "company_id"
    t.integer  "employee_id"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["unlock_token"], name: "index_users_on_unlock_token", unique: true, using: :btree

  create_table "vacation_balance_records", force: :cascade do |t|
    t.date     "begda"
    t.date     "endda"
    t.string   "subty"
    t.string   "ktext"
    t.integer  "anzhl"
    t.integer  "kverb"
    t.integer  "employee_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "company_id"
  end

  create_table "vacation_records", force: :cascade do |t|
    t.date     "begda"
    t.date     "endda"
    t.string   "awart"
    t.string   "atext"
    t.integer  "abwtg"
    t.integer  "kaltg"
    t.integer  "employee_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "company_id"
  end

  create_table "vacation_requirements", force: :cascade do |t|
    t.date     "start_date"
    t.date     "end_date"
    t.text     "more_text"
    t.text     "response_text"
    t.string   "status"
    t.integer  "employee_id"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.string   "tipo"
    t.string   "attachment"
    t.integer  "approver_id_posicion"
    t.integer  "approved_by_identification"
    t.integer  "company_id"
  end

  create_table "vacations", force: :cascade do |t|
    t.integer  "company_id"
    t.integer  "available"
    t.integer  "pernr"
    t.string   "resumen",     default: [],              array: true
    t.string   "detalle",     default: [],              array: true
    t.integer  "employee_id"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "variance_analysis", force: :cascade do |t|
    t.integer  "position"
    t.integer  "position_boss"
    t.string   "id_cc_payroll"
    t.string   "description_payroll"
    t.string   "type_accumulation",     limit: 1
    t.float    "previous_amount"
    t.float    "current_amount"
    t.float    "difference_amount"
    t.integer  "percentage_difference"
    t.string   "currency",              limit: 5
    t.string   "society"
    t.integer  "employee_id"
    t.integer  "company_id"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
  end

  create_table "view_articles", force: :cascade do |t|
    t.integer  "employee_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "article_id"
  end

  create_table "web_services_sap_syncs", force: :cascade do |t|
    t.text     "name_service"
    t.text     "description_service"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.string   "operation"
    t.string   "url"
    t.string   "name_job"
  end

  create_table "workers", force: :cascade do |t|
    t.string   "login",               default: "", null: false
    t.string   "encrypted_password",  default: "", null: false
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",       default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "workers", ["login"], name: "index_workers_on_login", unique: true, using: :btree

  add_foreign_key "compensatory_vacations", "employees"
  add_foreign_key "extra_requirements", "employees"
  add_foreign_key "infos", "employees"
  add_foreign_key "inhability_requirements", "employees"
  add_foreign_key "labors", "employees"
  add_foreign_key "license_requirements", "employees"
  add_foreign_key "service_vacations", "employees"
end
