require 'test_helper'

class EmployeesControllerTest < ActionController::TestCase
  setup do
    @employee = employees(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:employees)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create employee" do
    assert_difference('Employee.count') do
      post :create, employee: { admin: @employee.admin, apply_reviewer: @employee.apply_reviewer, company_id: @employee.company_id, data_reviewer: @employee.data_reviewer, email: @employee.email, identification: @employee.identification, image: @employee.image, lastname: @employee.lastname, name: @employee.name, phone: @employee.phone, second_lastname: @employee.second_lastname, second_name: @employee.second_name, short_name: @employee.short_name, superadmin: @employee.superadmin, user_id: @employee.user_id }
    end

    assert_redirected_to employee_path(assigns(:employee))
  end

  test "should show employee" do
    get :show, id: @employee
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @employee
    assert_response :success
  end

  test "should update employee" do
    patch :update, id: @employee, employee: { admin: @employee.admin, apply_reviewer: @employee.apply_reviewer, company_id: @employee.company_id, data_reviewer: @employee.data_reviewer, email: @employee.email, identification: @employee.identification, image: @employee.image, lastname: @employee.lastname, name: @employee.name, phone: @employee.phone, second_lastname: @employee.second_lastname, second_name: @employee.second_name, short_name: @employee.short_name, superadmin: @employee.superadmin, user_id: @employee.user_id }
    assert_redirected_to employee_path(assigns(:employee))
  end

  test "should destroy employee" do
    assert_difference('Employee.count', -1) do
      delete :destroy, id: @employee
    end

    assert_redirected_to employees_path
  end
end
