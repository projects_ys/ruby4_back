require 'test_helper'

class LicenseRequirementsControllerTest < ActionController::TestCase
  setup do
    @license_requirement = license_requirements(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:license_requirements)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create license_requirement" do
    assert_difference('LicenseRequirement.count') do
      post :create, license_requirement: { employee_id: @license_requirement.employee_id, end_date: @license_requirement.end_date, motivo: @license_requirement.motivo, start_date: @license_requirement.start_date, status: @license_requirement.status }
    end

    assert_redirected_to license_requirement_path(assigns(:license_requirement))
  end

  test "should show license_requirement" do
    get :show, id: @license_requirement
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @license_requirement
    assert_response :success
  end

  test "should update license_requirement" do
    patch :update, id: @license_requirement, license_requirement: { employee_id: @license_requirement.employee_id, end_date: @license_requirement.end_date, motivo: @license_requirement.motivo, start_date: @license_requirement.start_date, status: @license_requirement.status }
    assert_redirected_to license_requirement_path(assigns(:license_requirement))
  end

  test "should destroy license_requirement" do
    assert_difference('LicenseRequirement.count', -1) do
      delete :destroy, id: @license_requirement
    end

    assert_redirected_to license_requirements_path
  end
end
