require 'test_helper'

class ExtraRequirementsControllerTest < ActionController::TestCase
  setup do
    @extra_requirement = extra_requirements(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:extra_requirements)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create extra_requirement" do
    assert_difference('ExtraRequirement.count') do
      post :create, extra_requirement: { employee_id: @extra_requirement.employee_id, fecha: @extra_requirement.fecha, hours: @extra_requirement.hours, motivo: @extra_requirement.motivo, status: @extra_requirement.status }
    end

    assert_redirected_to extra_requirement_path(assigns(:extra_requirement))
  end

  test "should show extra_requirement" do
    get :show, id: @extra_requirement
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @extra_requirement
    assert_response :success
  end

  test "should update extra_requirement" do
    patch :update, id: @extra_requirement, extra_requirement: { employee_id: @extra_requirement.employee_id, fecha: @extra_requirement.fecha, hours: @extra_requirement.hours, motivo: @extra_requirement.motivo, status: @extra_requirement.status }
    assert_redirected_to extra_requirement_path(assigns(:extra_requirement))
  end

  test "should destroy extra_requirement" do
    assert_difference('ExtraRequirement.count', -1) do
      delete :destroy, id: @extra_requirement
    end

    assert_redirected_to extra_requirements_path
  end
end
