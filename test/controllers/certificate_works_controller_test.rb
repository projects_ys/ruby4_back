require 'test_helper'

class CertificateWorksControllerTest < ActionController::TestCase
  setup do
    @certificate_work = certificate_works(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:certificate_works)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create certificate_work" do
    assert_difference('CertificateWork.count') do
      post :create, certificate_work: { company_id: @certificate_work.company_id, data: @certificate_work.data, employee_id: @certificate_work.employee_id, fecha: @certificate_work.fecha }
    end

    assert_redirected_to certificate_work_path(assigns(:certificate_work))
  end

  test "should show certificate_work" do
    get :show, id: @certificate_work
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @certificate_work
    assert_response :success
  end

  test "should update certificate_work" do
    patch :update, id: @certificate_work, certificate_work: { company_id: @certificate_work.company_id, data: @certificate_work.data, employee_id: @certificate_work.employee_id, fecha: @certificate_work.fecha }
    assert_redirected_to certificate_work_path(assigns(:certificate_work))
  end

  test "should destroy certificate_work" do
    assert_difference('CertificateWork.count', -1) do
      delete :destroy, id: @certificate_work
    end

    assert_redirected_to certificate_works_path
  end
end
