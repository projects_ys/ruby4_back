require 'test_helper'

class CompensatoryVacationsControllerTest < ActionController::TestCase
  setup do
    @compensatory_vacation = compensatory_vacations(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:compensatory_vacations)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create compensatory_vacation" do
    assert_difference('CompensatoryVacation.count') do
      post :create, compensatory_vacation: { at_date: @compensatory_vacation.at_date, attachment: @compensatory_vacation.attachment, employee_id: @compensatory_vacation.employee_id, pay: @compensatory_vacation.pay, quantity: @compensatory_vacation.quantity, reason: @compensatory_vacation.reason, rule: @compensatory_vacation.rule, status: @compensatory_vacation.status, tipo: @compensatory_vacation.tipo }
    end

    assert_redirected_to compensatory_vacation_path(assigns(:compensatory_vacation))
  end

  test "should show compensatory_vacation" do
    get :show, id: @compensatory_vacation
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @compensatory_vacation
    assert_response :success
  end

  test "should update compensatory_vacation" do
    patch :update, id: @compensatory_vacation, compensatory_vacation: { at_date: @compensatory_vacation.at_date, attachment: @compensatory_vacation.attachment, employee_id: @compensatory_vacation.employee_id, pay: @compensatory_vacation.pay, quantity: @compensatory_vacation.quantity, reason: @compensatory_vacation.reason, rule: @compensatory_vacation.rule, status: @compensatory_vacation.status, tipo: @compensatory_vacation.tipo }
    assert_redirected_to compensatory_vacation_path(assigns(:compensatory_vacation))
  end

  test "should destroy compensatory_vacation" do
    assert_difference('CompensatoryVacation.count', -1) do
      delete :destroy, id: @compensatory_vacation
    end

    assert_redirected_to compensatory_vacations_path
  end
end
