require 'test_helper'

class ServiceVacationsControllerTest < ActionController::TestCase
  setup do
    @service_vacation = service_vacations(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:service_vacations)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create service_vacation" do
    assert_difference('ServiceVacation.count') do
      post :create, service_vacation: { approved_by_identification: @service_vacation.approved_by_identification, approver_id_posicion: @service_vacation.approver_id_posicion, attachment: @service_vacation.attachment, days: @service_vacation.days, employee_id: @service_vacation.employee_id, fecha: @service_vacation.fecha, more_text: @service_vacation.more_text, status: @service_vacation.status, tipo: @service_vacation.tipo }
    end

    assert_redirected_to service_vacation_path(assigns(:service_vacation))
  end

  test "should show service_vacation" do
    get :show, id: @service_vacation
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @service_vacation
    assert_response :success
  end

  test "should update service_vacation" do
    patch :update, id: @service_vacation, service_vacation: { approved_by_identification: @service_vacation.approved_by_identification, approver_id_posicion: @service_vacation.approver_id_posicion, attachment: @service_vacation.attachment, days: @service_vacation.days, employee_id: @service_vacation.employee_id, fecha: @service_vacation.fecha, more_text: @service_vacation.more_text, status: @service_vacation.status, tipo: @service_vacation.tipo }
    assert_redirected_to service_vacation_path(assigns(:service_vacation))
  end

  test "should destroy service_vacation" do
    assert_difference('ServiceVacation.count', -1) do
      delete :destroy, id: @service_vacation
    end

    assert_redirected_to service_vacations_path
  end
end
