require 'test_helper'

class InhabilityRequirementsControllerTest < ActionController::TestCase
  setup do
    @inhability_requirement = inhability_requirements(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:inhability_requirements)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create inhability_requirement" do
    assert_difference('InhabilityRequirement.count') do
      post :create, inhability_requirement: { employee_id: @inhability_requirement.employee_id, end_date: @inhability_requirement.end_date, motivo: @inhability_requirement.motivo, start_date: @inhability_requirement.start_date, status: @inhability_requirement.status }
    end

    assert_redirected_to inhability_requirement_path(assigns(:inhability_requirement))
  end

  test "should show inhability_requirement" do
    get :show, id: @inhability_requirement
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @inhability_requirement
    assert_response :success
  end

  test "should update inhability_requirement" do
    patch :update, id: @inhability_requirement, inhability_requirement: { employee_id: @inhability_requirement.employee_id, end_date: @inhability_requirement.end_date, motivo: @inhability_requirement.motivo, start_date: @inhability_requirement.start_date, status: @inhability_requirement.status }
    assert_redirected_to inhability_requirement_path(assigns(:inhability_requirement))
  end

  test "should destroy inhability_requirement" do
    assert_difference('InhabilityRequirement.count', -1) do
      delete :destroy, id: @inhability_requirement
    end

    assert_redirected_to inhability_requirements_path
  end
end
