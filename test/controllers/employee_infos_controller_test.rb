require 'test_helper'

class EmployeeInfosControllerTest < ActionController::TestCase
  setup do
    @employee_info = employee_infos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:employee_infos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create employee_info" do
    assert_difference('EmployeeInfo.count') do
      post :create, employee_info: { bukrs: @employee_info.bukrs, datos_bancarios: @employee_info.datos_bancarios, datos_empresariales: @employee_info.datos_empresariales, datos_personales: @employee_info.datos_personales, employee_id: @employee_info.employee_id, pernr: @employee_info.pernr }
    end

    assert_redirected_to employee_info_path(assigns(:employee_info))
  end

  test "should show employee_info" do
    get :show, id: @employee_info
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @employee_info
    assert_response :success
  end

  test "should update employee_info" do
    patch :update, id: @employee_info, employee_info: { bukrs: @employee_info.bukrs, datos_bancarios: @employee_info.datos_bancarios, datos_empresariales: @employee_info.datos_empresariales, datos_personales: @employee_info.datos_personales, employee_id: @employee_info.employee_id, pernr: @employee_info.pernr }
    assert_redirected_to employee_info_path(assigns(:employee_info))
  end

  test "should destroy employee_info" do
    assert_difference('EmployeeInfo.count', -1) do
      delete :destroy, id: @employee_info
    end

    assert_redirected_to employee_infos_path
  end
end
