require 'test_helper'

class SaldosControllerTest < ActionController::TestCase
  setup do
    @saldo = saldos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:saldos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create saldo" do
    assert_difference('Saldo.count') do
      post :create, saldo: { bukrs: @saldo.bukrs, employee_id: @saldo.employee_id, intsaldo: @saldo.intsaldo, intvcausado: @saldo.intvcausado, pernr: @saldo.pernr, saldo: @saldo.saldo, t_cesantias: @saldo.t_cesantias, t_endeudamiento: @saldo.t_endeudamiento, t_intcesantias: @saldo.t_intcesantias, totdeducciones: @saldo.totdeducciones, totdevengos: @saldo.totdevengos, vcausado: @saldo.vcausado }
    end

    assert_redirected_to saldo_path(assigns(:saldo))
  end

  test "should show saldo" do
    get :show, id: @saldo
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @saldo
    assert_response :success
  end

  test "should update saldo" do
    patch :update, id: @saldo, saldo: { bukrs: @saldo.bukrs, employee_id: @saldo.employee_id, intsaldo: @saldo.intsaldo, intvcausado: @saldo.intvcausado, pernr: @saldo.pernr, saldo: @saldo.saldo, t_cesantias: @saldo.t_cesantias, t_endeudamiento: @saldo.t_endeudamiento, t_intcesantias: @saldo.t_intcesantias, totdeducciones: @saldo.totdeducciones, totdevengos: @saldo.totdevengos, vcausado: @saldo.vcausado }
    assert_redirected_to saldo_path(assigns(:saldo))
  end

  test "should destroy saldo" do
    assert_difference('Saldo.count', -1) do
      delete :destroy, id: @saldo
    end

    assert_redirected_to saldos_path
  end
end
