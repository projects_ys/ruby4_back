require 'test_helper'

class VacationRequirementsControllerTest < ActionController::TestCase
  setup do
    @vacation_requirement = vacation_requirements(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:vacation_requirements)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create vacation_requirement" do
    assert_difference('VacationRequirement.count') do
      post :create, vacation_requirement: { employee_id: @vacation_requirement.employee_id, end_date: @vacation_requirement.end_date, more_text: @vacation_requirement.more_text, response_text: @vacation_requirement.response_text, start_date: @vacation_requirement.start_date, status: @vacation_requirement.status }
    end

    assert_redirected_to vacation_requirement_path(assigns(:vacation_requirement))
  end

  test "should show vacation_requirement" do
    get :show, id: @vacation_requirement
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @vacation_requirement
    assert_response :success
  end

  test "should update vacation_requirement" do
    patch :update, id: @vacation_requirement, vacation_requirement: { employee_id: @vacation_requirement.employee_id, end_date: @vacation_requirement.end_date, more_text: @vacation_requirement.more_text, response_text: @vacation_requirement.response_text, start_date: @vacation_requirement.start_date, status: @vacation_requirement.status }
    assert_redirected_to vacation_requirement_path(assigns(:vacation_requirement))
  end

  test "should destroy vacation_requirement" do
    assert_difference('VacationRequirement.count', -1) do
      delete :destroy, id: @vacation_requirement
    end

    assert_redirected_to vacation_requirements_path
  end
end
