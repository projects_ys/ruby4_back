require 'test_helper'

class FirstApproverNotificationsTest < ActionMailer::TestCase
  test "new_request_received" do
    mail = FirstApproverNotifications.new_request_received
    assert_equal "New request received", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
