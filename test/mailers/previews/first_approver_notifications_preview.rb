# Preview all emails at http://localhost:3000/rails/mailers/first_approver_notifications
class FirstApproverNotificationsPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/first_approver_notifications/new_request_received
  def new_request_received
    FirstApproverNotifications.new_request_received
  end

end
